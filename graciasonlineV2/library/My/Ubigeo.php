<?php

class My_Ubigeo  extends Zend_Controller_Plugin_Abstract
{
    
    function getProvincias($departamento){
        $tableUgigeo=new Application_Model_DbTable_Ubigeo();        
        $array['provincias']=$tableUgigeo->getProvincias($departamento);        
        
        $distrito=trim($array['provincias'][0]['provincia']);//la primera provincia
        $array['distritos']=$tableUgigeo->getDistritos($distrito);        
        $array['resp']=0;
        
        if(count($array['provincias'])+count($array['distritos'])>=2){
            $array['resp']=1;
        }
        
    	$json = Zend_Json::encode($array);    	
        return $json;
    }  
    
    function getDistritos($provincia){        
        $tableUgigeo=new Application_Model_DbTable_Ubigeo();
        $array['distritos']=$tableUgigeo->getDistritos(trim($provincia));        
        
        $array['resp']=0;
        
        if(count($array['distritos'])>0){
            $array['resp']=1;
        }
        
    	$json = Zend_Json::encode($array);
    	return $json;
    }
    
    function setUbigeo($form,$tableUbigeo,$ubigeo){//seteamos en modificar registrar
    	$getProvincias=$tableUbigeo->getProvincias($ubigeo['departamento']);
    	 
    	$getDepartamentos=$tableUbigeo->getDepartamentos();
        
    	foreach ($getDepartamentos as $rowDepa):
    		$form->getElement('departamento')->addMultiOption($rowDepa['departamento'], $rowDepa['departamento']);
    	endforeach;
    	 
    	foreach ($getProvincias as $rowProvincias):
    		$form->getElement('provincia')->addMultiOption($rowProvincias['provincia'], $rowProvincias['provincia']);
    	endforeach;
    	 
    	$getDistritos=$tableUbigeo->getDistritos($ubigeo['provincia']);
    	 
    	foreach ($getDistritos as $rowDistrito):
    		$form->getElement('distrito')->addMultiOption($rowDistrito['distrito'], $rowDistrito['distrito']);
    	endforeach;
    	 
    	//DEFAULT
    	$form->getElement('departamento')->setValue($ubigeo['departamento']);
    	$form->getElement('provincia')->setValue($ubigeo['provincia']);
    	$form->getElement('distrito')->setValue($ubigeo['distrito']);
    }
}

