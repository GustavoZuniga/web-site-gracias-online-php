<?php

class My_Funciones  extends Zend_Controller_Plugin_Abstract
{
    //reduce la cadena en ('es un argumento ...') 
    function newString($n,$cadena,$continua=""){ 	
        $longitud=strlen(trim($cadena));

        if($longitud<=$n){
                return trim($cadena);
        }

        $newCadena=substr (trim($cadena), 0, -($longitud-$n))." ".$continua;
        return $newCadena;    	
    }
    
    function ie($version='1-8'){
        //if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])){// if IE<=8
        if(preg_match('/(?i)msie ['.$version.']/',$_SERVER['HTTP_USER_AGENT'])){// if IE<=8            
            return 'menor';
        }else{// if IE>8
            return 'mayor';
        }
    } 
    
    function fecha1($fecha){
        $formato=explode('-',substr($fecha,0,10));
        $myCalendario=new My_Calendario();
        $mes = strtolower($myCalendario->meses($formato[1]));
        
        return "Publicado el ".$formato[2]." de ".$mes." del ".$formato[0];
    }
}

