<?php

class My_Auth  extends Zend_Controller_Plugin_Abstract
{    
    protected $_ambiente;//frontend,backend
    
    public function __construct($ambiente = array())
    {
        $this->_ambiente=$ambiente;
        
    }
    
    function zendAuth($auth){//tanto para el backend como para el frontend
            if(!Zend_Session::namespaceIsset($this->_ambiente))://cuando se logea
                $mysession = new Zend_Session_Namespace($this->_ambiente);
                $mysession->usuario=$auth->getIdentity();                
            else://cuando esta comprobando            
                $mysession = new Zend_Session_Namespace($this->_ambiente);                
                $auth->getStorage()->write($mysession->usuario);                
            endif;            
    }
    
    function authExiste($auth){
        $auth->getIdentity();//si no coloco esto no funcionara
        
        if(Zend_Session::namespaceIsset($this->_ambiente)):            
            $mysession = new Zend_Session_Namespace($this->_ambiente);            
            $auth->getStorage()->write($mysession->usuario);//aqui se carga nuevamente        
        endif;        
    }
    
    function exitSession(){
        Zend_Auth::getInstance()->clearIdentity();//sin esto no funciona
        Zend_Session::namespaceUnset($this->_ambiente);//eliminamos la session
        
    }
    
}

