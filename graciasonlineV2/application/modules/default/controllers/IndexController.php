<?php

class Default_IndexController extends Zend_Controller_Action
{

    public function indexAction(){
    	if(Zend_Auth::getInstance()->hasIdentity()){//redirecciona si ya esta logeado
    		$this->_redirect('vista');
    		exit;
    	}
		   	
    	//$formlogin=new Default_Form_Login();
        //Application_Form_Login
        $formlogin=new Application_Form_Login();
    	$this->view->frmlogin=$formlogin;    	
    }
    
    private function getAuthAdapter(){
    	$authAdapter=new Zend_Auth_Adapter_DbTable( zend_Db_Table::getDefaultAdapter());
    	$authAdapter->setTableName('das_usuarios')
			    	->setIdentityColumn('email_usuarios')
			    	->setCredentialColumn('password_usuarios');
    	return $authAdapter;
    }
    
    public function loginAction(){    	
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	
    	$formlogin=new Default_Form_Login();
    	
    	if( $this->getRequest()->isPost() ){
			if( $formlogin->isValid( $this->_getAllParams() )) {
					
					$username=$formlogin->getValue('nick');
					$password=$formlogin->getValue('password');
					$this->ZendAuth($username, $password);
			}    	
    	}else{
    		$this->_redirect('index');//en caso de que coloquen el url con parametros (/index/login?param1=valor1 ...)
    	}    
    }
    
    /**
     * 
     * @param string $username
     * @param string $password
     * @todo he creado function "ZendAuth" para ser utilizado en el logeo y en el registro (tipo facebook->logica mia)->recurso reutilizable, utilizan funciones (loginAction y registrarAction) 
     */
    function ZendAuth($username,$password){
    	$authAdapter=$this->getAuthAdapter();
    	$authAdapter->setIdentity($username)
    	->setCredential($password);
    		
    	$auth=Zend_Auth::getInstance();
    	$result=$auth->authenticate($authAdapter);
    		
    	if($result->isValid()){
    		$identity=$authAdapter->getResultRowObject();
    		$authStorage=$auth->getStorage();
    		$authStorage->write($identity);
    		//------------------------------------------------------
    		$mysession = new Zend_Session_Namespace('mysession');    		
    		$mysession->idusuario= $identity->idusuarios;
    		//------------------------------------------------------
    		$array["resp"]=1;
    		$array["url"]="http://localhost/das/public/perfil";
    		$json = Zend_Json::encode($array);
    		echo $json;
    	}else{
    		$array["resp"]=0;
    		$json = Zend_Json::encode($array);
    		echo $json;
    	}
    }
    
    public function logoutAction(){//con este codigo me deslogeo
    	//consultamos si el usuario esta logeado 
    	$auth = Zend_Auth::getInstance();
    	if ($auth->hasIdentity()) {    		
    		Zend_Auth::getInstance()->clearIdentity();
    		$mysession = new Zend_Session_Namespace('mysession');//Para remover variables de la sesion:
    		unset($mysession->idusuarios);
    		unset($mysession->usuarioactual);
    			
    		//Y para destruir la sesi n:
    		//destruyo todos los datos del namespace
    		Zend_Session::namespaceUnset('mysession');
    		Zend_Session::destroy();
    			
    		$this->_redirect('index');
    	} else {
    		echo "No estas logueado";
    		$this->_redirect('index');
    	}    		    
    }


}

