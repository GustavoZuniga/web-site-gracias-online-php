<?php

class Usuarios_LoginController extends Zend_Controller_Action
{

    public function init()
    {	
    	//$this->dbmoodle = Zend_Registry::get('dbmoodle');//para la conexion a la bbdd moodle
        //calendario aside
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
    	
    	$myCalendario=new My_Calendario();
    	
    	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));
    	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
    	
    	$this->view->diasMes=$diasMes;
    	$this->view->diaSemana=$diaSemana;
    	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
    	
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
    	
    	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        /*
         * configuracion para obtener las imagenes de noticias
         */
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);        
        $this->view->urlgetimagen = $config->ruta->fotos->mostrar.'/noticia/';
        
        //wiget noticias
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $this->view->wgtnoticia = $tableNoticias->getWigetNoticias();
    }

    
    public function indexAction(){//redirect
            
            $tableUsuario = new Application_Model_DbTable_Usuario();            
            $formLogin=new Application_Form_Login();
            
             if($this->getRequest()->isPost()):
                     if( $formLogin->isValid($this->_getAllParams())) :
                                    
                                    $authAdapter = new Zend_Auth_Adapter_DbTable($tableUsuario->getAdapter(),'cms_usuario');
                                    $authAdapter->setIdentityColumn('username')
                                                ->setCredentialColumn('password')
                                                ->setIdentity($formLogin->getValue('txtUsuario'))
                                                ->setCredential(md5($formLogin->getValue('txtPassword')));
                                    
                                    
                                    // Recogemos Zend_Auth
                                    $auth = Zend_Auth::getInstance();
                                    
                                    // Realiza la comprobación con el adaptador que hemos creado
                                    $result = $auth->authenticate($authAdapter);

                                    // Si la autentificación es válida
                                    if ($result->isValid()) {
                                                   $data = $authAdapter->getResultRowObject(array('id','username','password','email'));                                                   
                                                   $dataLogin['frontend']=$data;
                                                   $auth->getStorage()->write((object)$dataLogin);                                                   
                                                   //----------------------------------------------------------                                                   
                                                   $myAuth=new My_Auth('frontend');//evaluamos si existe la session
                                                   $myAuth->zendAuth($auth);
                                                   //----------------------------------------------------------
                                                   $this->_redirect('calendario/listaeventos');
                                            
                                    } else {
                                        $this->_redirect('usuarios/inicio');
                                        //$this->view->loginError = 'Usuario o contraseña incorrectas';//$result->getMessages();
                                    }
                     endif;
             else:    
                     $this->_redirect('usuarios/inicio');//en caso de que coloquen el url con parametros (/index/login?param1=valor1 ...)
             endif; 
    }
        
    public function logoutAction()
    {
        $myAuth=new My_Auth('frontend');
        $myAuth->exitSession();
        $this->_redirect('/usuarios/login');
    }
    
    public function crearCuentaAction()
    {
        //$this->dbmoodle = Zend_Registry::get('dbmoodle');//para la conexion a la bbdd moodle
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        $this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/crear-cuenta-portal.js');//cambiar esta ruta esta harcodeada
        $frmRegistrar=new Application_Form_CrearCuenta();        
        
        if( $this->getRequest()->isPost() ){
        	if($frmRegistrar->isValid($this->_request->getPost())){
        	       $data['username'] = $frmRegistrar->getValue('txtNombreUsuarioRu');
                        $data['password'] = $frmRegistrar->getValue('txtPasswordRu');
                       $data['password'] = md5($data['password']);
                       $data['fecharegistro'] = date('Y-m-d H:i:s');
                       $data['firstname'] = $frmRegistrar->getValue('txtNombresRu');
                       $data['lastname'] = $frmRegistrar->getValue('txtApellidosRu');
                       $data['email'] = $frmRegistrar->getValue('txtEmailRu');
                       $data['city'] = $frmRegistrar->getValue('txtCiudadRu');
                       $data['country'] = $frmRegistrar->getValue('pais');
                        $data['key'] = $frmRegistrar->getValue('txtPasswordRu');
                       
                       $tableUser=new Application_Model_DbTable_Usuario();
                       
                       $rptaUser = $tableUser->addUsuario($data);
                       
                        
                        
                    /*
                     * insertamos data de ususario nuevo 
                     * en la tabla mdl_user de moodle
                     */
                     /*   $dataMoodle['username'] = $data['username'];
                        $dataMoodle['password'] =  $data['password'];
                        $dataMoodle['firstname'] = $data['firstname'];
                        $dataMoodle['lastname'] = $data['lastname'];
                        $dataMoodle['email'] = $data['email'];
                        $dataMoodle['city'] = $data['city'];
                        $dataMoodle['country'] = $data['country'];
                        $dataMoodle['confirmed']= 1;
                        $dataMoodle['mnethostid']= 1;
                        $dataMoodle['descriptionformat']= 1;
                        
                	$this->dbmoodle->insert('mdl_user', $dataMoodle);*/
                        
                       
                       if((int)$rptaUser > 0){
                           $this->view->respRegistro=1;
                       }else{
                           $this->view->respRegistro=0;
                       }
        	}        
        }
        
        $this->view->frmRegistrar=$frmRegistrar;
    }

}

