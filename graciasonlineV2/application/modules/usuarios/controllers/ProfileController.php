<?php

class Usuarios_ProfileController extends Zend_Controller_Action
{

    public function init()
    {	
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
        /*
         * configuracion para obtener las imagenes de noticias
         */
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);        
        $this->view->urlgetimagen = $config->ruta->fotos->mostrar.'/noticia/';
		
		$myAuth=new My_Auth('frontend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->frontend)):
            $this->_redirect('login');
        endif;
        
        //$this->_auth= $auth->getIdentity()->frontend;
        
    }

    
    public function indexAction(){//redirect
                  
    }
     
	public function profileStudentAction()
    {
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        //$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/crear-cuenta-portal.js');//cambiar esta ruta esta harcodeada
        
		$authIni = Zend_Auth::getInstance();
		$datoLogin = $authIni->getIdentity()->frontend;
		
		var_dump($datoLogin);exit();
		$tableUsuario = new Application_Model_DbTable_Usuario();
		
		$frmRegistrar=new Application_Form_CrearCuenta();        
        
        if( $this->getRequest()->isPost() ){
		
        	if($frmRegistrar->isValid($this->_request->getPost())){
                       $data['password'] = $frmRegistrar->getValue('txtPasswordRu');
                       $data['password'] = md5($data['password']);
                       $data['fecharegistro'] = date('Y-m-d H:i:s');
                       $data['firstname'] = $frmRegistrar->getValue('txtNombresRu');
                       $data['lastname'] = $frmRegistrar->getValue('txtApellidosRu');
                       $data['email'] = $frmRegistrar->getValue('txtEmailRu');
                       $data['rol'] = $frmRegistrar->getValue('cmbRol');
                       
                       $tableUser=new Application_Model_DbTable_Usuario();
                       
                       $rptaUser = $tableUser->addUsuario($data);
                       
                       if((int)$rptaUser > 0){
                           $this->view->respRegistro=1;
                       }else{
                           $this->view->respRegistro=0;
                       }
        	}  			
        }
        
        $this->view->frmRegistrar=$frmRegistrar;
    }
	
	public function profileTeacherAction()
    {
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        //$this->view->headScript()->appendFile($urlWeb.'/assets/js/vendor.bundle.js');
		
		$authIni = Zend_Auth::getInstance();
		$datoLogin = $authIni->getIdentity()->frontend;

		$tableUsuario = new Application_Model_DbTable_Usuario();
		$row = $tableUsuario->getInfoUsuario($datoLogin->id);
		

        $frmRegistrar=new Application_Form_CrearCuenta();        
        
        if( $this->getRequest()->isPost() ){
		
        	if($frmRegistrar->isValid($this->_request->getPost())){
                       $data['password'] = $frmRegistrar->getValue('txtPasswordRu');
                       $data['password'] = md5($data['password']);
                       $data['fecharegistro'] = date('Y-m-d H:i:s');
                       $data['firstname'] = $frmRegistrar->getValue('txtNombresRu');
                       $data['lastname'] = $frmRegistrar->getValue('txtApellidosRu');
                       $data['email'] = $frmRegistrar->getValue('txtEmailRu');
                       $data['rol'] = $frmRegistrar->getValue('cmbRol');
                       
                       $tableUser=new Application_Model_DbTable_Usuario();
                       
                       $rptaUser = $tableUser->addUsuario($data);
                       
                       if((int)$rptaUser > 0){
                           $this->view->respRegistro=1;
                       }else{
                           $this->view->respRegistro=0;
                       }
        	}  			
        }
        $this->view->row = $row;
        $this->view->frmRegistrar=$frmRegistrar;
    }
}

