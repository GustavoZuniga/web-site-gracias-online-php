<?php

class Usuarios_EquiposController extends Zend_Controller_Action
{
    protected $_auth;
    
    public function init()
    {
    	Zend_Layout::getMvcInstance()->assign('menu', 'equipos');
    	 
    	//calendario aside
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
    	 
    	$myCalendario=new My_Calendario();
    	 
    	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));
    	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
    	 
    	$this->view->diasMes=$diasMes;
    	$this->view->diaSemana=$diaSemana;
    	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
    	 
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
    	 
    	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        /*
         * configuracion para obtener las imagenes de noticias
         */
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
        $this->view->urlgetimagen = $config->ruta->fotos->mostrar.'/noticia/';
        
        //wiget noticias
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $this->view->wgtnoticia = $tableNoticias->getWigetNoticias();
        
        $myAuth=new My_Auth('frontend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
        
        if(isset($auth->getIdentity()->frontend)):            
            $this->_auth= $auth->getIdentity()->frontend;
        endif;
    }

    public function indexAction(){
        Zend_Layout::getMvcInstance()->assign('title', 'Equipos');//seo
        $page = $this->_getParam('page', 0);
        $buscar=trim($this->_getParam('buscar',0));
                
    	$this->view->page=$page;
        $this->view->parametros="";
        
        if((int)$page<=0):    		
                $page=1;
        endif;
        
    	$frmEquiposBuscar=new Application_Form_EquiposBuscar();
        $tableEquipos=new Application_Model_DbTable_Equipos();
        
        if($this->getRequest()->isPost()):
            if($frmEquiposBuscar->isValid($this->_request->getPost())):
                $idEquipos = $frmEquiposBuscar->getValue('disciplina');
                $frmEquiposBuscar->getElement('disciplina')->setValue($idEquipos);
                $getEquipos=$tableEquipos->getBuscarEquipos($idEquipos);
                
                $buscar=$idEquipos;
            endif;
        else:
            if((int)$buscar>0):
                $frmEquiposBuscar->getElement('disciplina')->setValue($buscar);
                $getEquipos=$tableEquipos->getBuscarEquipos((int)$buscar);
            else:                
                $getEquipos=$tableEquipos->getListaEquipos();
            endif;
        endif;
        
        $this->view->parametros=$buscar;
        
        $numeroRegistros=5;
        $rangoPaginas=10;

        $paginador = Zend_Paginator::factory($getEquipos);
        $paginador->setItemCountPerPage($numeroRegistros)
                                ->setCurrentPageNumber((int)$page)
                                ->setPageRange($rangoPaginas);
         
        $this->view->equipos=$paginador;
        $this->view->frmEquiposBuscar=$frmEquiposBuscar;
        
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
        $this->view->rutaFotos = $config->ruta->fotos->mostrar.'/equipos/';
    }

    public function detalleAction(){
        $id=$this->_request->getParam('id',0);
    	
    	if((int)$id==0):
    		$this->_redirect('/inicio/index');
    	endif;
        
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/galeria.js');
    	
    	Zend_Layout::getMvcInstance()->assign('plugin', 'galeria');
        
        $tableEquipos = new Application_Model_DbTable_Equipos();                
        $getEquipo=$tableEquipos->getEquipo($id);
        
        $this->view->equipo=$getEquipo;
        Zend_Layout::getMvcInstance()->assign('title', $getEquipo['nombre']);//seo
        
        $tableEquiposFotos=new Application_Model_DbTable_EquiposFotos();
        
        $this->view->fotos=$tableEquiposFotos->getFotos($id);
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
	$this->view->rutaFotos=$config->ruta->fotos->mostrar.'/equipos/';
        $this->view->rutaBase = $config->ruta->redes->urlredes;
        
                
        $tableEquiposVideos=new Application_Model_DbTable_EquiposVideos();
        $getVideos=$tableEquiposVideos->getVideos($id);
        $this->view->videos=$getVideos;
        
        $tableDeportistas=new Application_Model_DbTable_Deportista();
        $this->view->deportistas=$tableDeportistas->getDeportistasEquipo($id);
        
        Zend_Layout::getMvcInstance()->assign('videos', $getVideos);
    }
    
    public function fotoAction(){
        $id=$this->_request->getParam('id',0);
    	
    	if((int)$id==0):
    		$this->_redirect('/inicio/index');
    	endif;
        
        $urlWeb = $this->getFrontController()->getBaseUrl();
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/galeria-sola.js');
        
        $tableEquipos = new Application_Model_DbTable_Equipos();                
        $getEquipo=$tableEquipos->getEquipo($id);
        $this->view->equipo=$getEquipo;
        Zend_Layout::getMvcInstance()->assign('title', $getEquipo['nombre'].' (Fotos)');//seo
        
        $tableEquiposFotos=new Application_Model_DbTable_EquiposFotos();
        $getFotos=$tableEquiposFotos->getFotos($id);
        $this->view->equiposFotos=$getFotos;
        
        
        $this->view->countFotos=count($getFotos);
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
	$this->view->rutaFotos=$config->ruta->fotos->mostrar.'/equipos/';
        $this->view->rutaBase = $config->ruta->redes->urlredes;
        
    }
    
    public function videoAction(){
        Zend_Layout::getMvcInstance()->assign('plugin', 'galeria');
        
        $id=$this->_getParam('id',0);
        $idEquipo=$this->_getParam('ide',0);
        
        if((int)$id>0):
            $tableEquipos=new Application_Model_DbTable_Equipos();
            $getEquipo=$tableEquipos->getEquipo($idEquipo);
            $this->view->nombre=$getEquipo['nombre'];
            $this->view->id=$getEquipo['id'];
            Zend_Layout::getMvcInstance()->assign('title', $getEquipo['nombre'].' (Video)');//seo
            
            $tableEquiposVideos=new Application_Model_DbTable_EquiposVideos();
            
            $this->view->video=$tableEquiposVideos->getVideo($id);
            $urlWeb = $this->getFrontController()->getBaseUrl();
            $this->view->headScript()->appendFile($urlWeb.'/static/js/youtube.js');
        
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);            
            $this->view->rutaBase = $config->ruta->redes->urlredes;
        else:
            $this->_redirect('usuarios/inicio/index');                
        endif;
    }
    
    //------------------------------------------------------------------------------------------------------
    //TRAIDO LO DE BACKEND
    //------------------------------------------------------------------------------------------------------
    
    function agregarFotosVideos($form,$resp){            
            //------------------------------------------------------------------
            //FOTOS
            //------------------------------------------------------------------
           $imagenes=array();

           $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);                           
           $ruta=$config->ruta->fotos->subir.'/equipos/';

            $dataFotos['id_equipos']=$resp;
            $dataFotos['ruta']=$ruta;
            $dataFotos['estado']=1;            
            $dataFotos['usuarioregistro']=(int)$this->_auth->id;
            $dataFotos['fecharegistro']=date('Y-m-d');
            //------------------------------------------------------------------
                            $upload = new Zend_File_Transfer_Adapter_Http();

                            foreach($upload->getFileInfo() as $file => $info): 
                                //-------------------------------------------------------------------
                                $upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$file);
                                $upload->addValidator('Size',true,array('min'=>'2KB','max'=>'1024KB'),$file);
                                //-------------------------------------------------------------------
                                if ($upload->isValid($file)): 
                                        $file_array = explode('.', $info['name']);
                                        $new_filename = rand().'.'. $file_array[1];

                                        $dataRename['source']=$info['tmp_name'];
                                        $dataRename['target']=$ruta.$new_filename;
                                        $dataRename['overwrite']=true;

                                        $upload->addFilter('Rename',$dataRename);
                                        $upload->receive($file);

                                        $imagenes[]=$new_filename;
                                endif;
                            endforeach;        				                            
            //------------------------------------------------------------------
            $countImagenes=count($imagenes);
            if($countImagenes >0):
                    $tableEquiposFotos=new Application_Model_DbTable_EquiposFotos();

                    for($i=0;$i<$countImagenes;$i++):
                        $dataFotos['nombre']=$imagenes[$i];
                        $tableEquiposFotos->addFotos($dataFotos);                        
                    endfor;
                    
                    //grabar foto de inicio para que aparesca en el frontend
                    //-------------------------------------------------------------------------
                    $data['foto_inicio']=$imagenes[((int)$countImagenes-1)];//ultima imagen
                    
                    $tableEquipos=new Application_Model_DbTable_Equipos();
                    $respFotoInicio=$tableEquipos->updateEquipos($data,$resp);
            endif;
            
            //------------------------------------------------------------------
            //VIDEOS(solo se colocaran videos de youtube)
            //------------------------------------------------------------------
            $videos=trim($form->getValue('getVideos'));
            if(!empty($videos)):
                   $getVideos=explode('/////',$videos);
                   $countVideos=count($getVideos);

                   if($countVideos>0):
                          $dataVideos['id_equipos']=$resp;                                                
                          $dataVideos['estado']=1;                          
                          $dataVideos['usuarioregistro']=(int)$this->_auth->id;
                          $dataVideos['fecharegistro']=date('Y-m-d');
                         $tableEquiposVideos=new Application_Model_DbTable_EquiposVideos();

                         for($i=0;$i<$countVideos;$i++):
                             $dataVideos['nombre']=$getVideos[$i];
                             $tableEquiposVideos->addVideos($dataVideos);                             
                         endfor;
                   endif;
            endif;
                        
    }
    
    function eliminarFotosVideos($form){
        
        $myAuth=new My_Auth('frontend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->frontend)):
            $this->_redirect('inicio/index');
        endif;
        
        $fotosEliminados=trim($form->getValue('getFotosEliminados'));
        if(strlen($fotosEliminados)>0){
            $arrayIdsFotos=explode("/////",$fotosEliminados);
            $tableEquiposFotos=new Application_Model_DbTable_EquiposFotos();            
            $nombresFotos=$tableEquiposFotos->getGrupoFotos($arrayIdsFotos);
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
            $ruta=$config->ruta->fotos->subir.'/equipos/';
            
            //eliminamos fisicamente
            foreach ($nombresFotos as $row):
            		unlink($ruta.$row['nombre']);
            endforeach;
            
            $tableEquiposFotos->deleteFotos($arrayIdsFotos);            
        }
        
        $getVideosEliminados=trim($form->getValue('getVideosEliminados'));
        if(strlen($getVideosEliminados)>0){            
            $arrayIdsVideos=explode("/////",$getVideosEliminados);
            $tableEquiposVideos=new Application_Model_DbTable_EquiposVideos();
            $tableEquiposVideos->deleteVideos($arrayIdsVideos);            
        }
    }
    
    function registrarEquipos($form,$estado='add'){                           
                            $data['nombre']=$form->getValue('txtNombre');
                            $data['deporte']=$form->getValue('txtDeporte');
                            $data['id_disciplinas']=$form->getValue('sltDisciplina');
                            $data['direccion_postal']=$form->getValue('txtDireccionpostal');

                            $data['telefono']=$form->getValue('txtTelefono');                            
                            $data['email']=$form->getValue('txtEmail');

                            $departamento=trim($form->getValue('departamento'));
                            $provincia=trim($form->getValue('provincia'));
                            $distrito=trim($form->getValue('distrito'));
                                                       
                            $data['estado']=1;
                            
                           try{
                                        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
                                        $getData=$tableUbigeo->getUbicasion($departamento, $provincia, $distrito);
                                        $data['ubigeo']=$getData['id'];

                                        $tableEquipos=new Application_Model_DbTable_Equipos();
                                        
                                        //------------------------------------------------------------------
                                        if($estado=='add'){
                                            $data['fecharegistro']= date('Y-m-d H:i:s');
                                            $data['usuarioregistro']=(int)$this->_auth->id;
                                            $resp=$tableEquipos->addEquipo($data);
                                            
                                            //Agregamos equipos a un evento
                                            /*
                                            if((int)$form->getValue('eventos')>0):                                                
                                                $dataEventosEquipo['id_eventos']=trim($form->getValue('eventos'));
                                                $dataEventosEquipo['id_equipos']=$resp;
                                                $dataEventosEquipo['estado']=1;
                                                $dataEventosEquipo['usuarioregistro']=(int)$this->_idUsuario;
                                                $dataEventosEquipo['fecharegistro']=date('Y-m-d H:i:s');
                                                
                                                $getEventosEquipos=new Application_Model_DbTable_EventosEquipos();
                                                $getEventosEquipos->addEquipos($dataEventosEquipo);
                                            endif;                                             
                                             */
                                            
                                        }else{//update
                                            $data['fechamodificacion']=date('Y-m-d H:i:s');
                                            $data['usuariomodificacion']=(int)$this->_auth->id;
                                            $id=$form->getValue('id');
                                            $resp=$tableEquipos->updateEquipos($data, $id);
                                            
                                            if((int)$resp>0):
                                            	$resp=$id;
                                            else:
                                            	$resp=0;
                                            endif;
                                            
                                            if($estado=='update'){
                                            	$this->eliminarFotosVideos($form);
                                            	$resp=$id;
                                                
                                                //Editamos equipos a un evento
                                                /*
                                                if((int)$form->getValue('eventos')>0):                                                
                                                    $dataEventosEquipo['id_eventos']=trim($form->getValue('eventos'));
                                                    $dataEventosEquipo['id_equipos']=$resp;
                                                    $dataEventosEquipo['estado']=1;
                                                    $dataEventosEquipo['usuariomodificacion']=$this->_idUsuario;
                                                    $dataEventosEquipo['fechamodificacion']=date('Y-m-d H:i:s');
                                                    
                                                    $idEveEqui=trim($form->getValue('idEveEqui'));
                                                    $getEventosEquipos=new Application_Model_DbTable_EventosEquipos();
                                                    $respUpdate=$getEventosEquipos->updateEquipos($dataEventosEquipo,$idEveEqui);
                                                endif;
                                                 * 
                                                 */
                                            }
                                        }
                                        //------------------------------------------------------------------
                                        
                                        //
                                        if((int)$resp > 0):                                                        
                                                        $this->agregarFotosVideos($form,$resp);
                                                        $this->_redirect('usuarios/equipos/listaequipos');                                                         
                                        endif;                 
                                         
                           } catch (Zend_Db_Exception $e) {
                                    $this->getMessenger()->error('Error al Actulizar.');
                                     echo $e->getMessage();
                           } catch (Zend_Exception $e) {
                                    $this->getMessenger()->error('error desconocido');
                                     echo $e->getMessage();
                           }
    }
    
    
    public function listaequiposAction()
    {           
        $myAuth=new My_Auth('frontend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->frontend)):
            $this->_redirect('inicio/index');
        endif;
        
        Zend_Layout::getMvcInstance()->assign('title','Lista de Equipos');//seo
        
        $page = $this->_getParam('page', 0);                
    	$this->view->page=$page;
        
        $existe = $this->_getParam('existe', 0);                
    	$this->view->existe=$existe;
        
        if((int)$page<=0):    		
                $page=1;
        endif;
        
        $tableEquipos=new Application_Model_DbTable_Equipos();
        $idUsuario=(int)$this->_auth->id;
        
        $getEquipos = $tableEquipos->getListaEquipos($idUsuario);
        
        $numeroRegistros=25;
        $rangoPaginas=10;

        $paginador = Zend_Paginator::factory($getEquipos);
        $paginador->setItemCountPerPage($numeroRegistros)
                                ->setCurrentPageNumber((int)$page)
                                ->setPageRange($rangoPaginas);

        $this->view->equipos=$paginador;        
    }
    
    public function nuevoAction(){
            $myAuth=new My_Auth('frontend');
            $auth = Zend_Auth::getInstance();        
            $myAuth->authExiste($auth);

            if(!isset($auth->getIdentity()->frontend)):
                $this->_redirect('inicio/index');
            endif;
            
            Zend_Layout::getMvcInstance()->assign('title','Panel de mantenimineto');//seo
            $urlWeb = $this->getFrontController()->getBaseUrl();
            $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/videos-fotos.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/registro-equipo.js');

            $tableUbigeo=new Application_Model_DbTable_Ubigeo();
            $myUbigeo=new My_Ubigeo();

            $frmEquipo = new Application_Form_Equipos();

            if($this->getRequest()->isPost()):
                    $request=$this->_request->getPost();

                    $ubigeo['departamento']=$request['departamento'];
                    $ubigeo['provincia']=$request['provincia'];
                    $ubigeo['distrito']=$request['distrito'];

                    $myUbigeo->setUbigeo($frmEquipo,$tableUbigeo,$ubigeo);

                    if($frmEquipo->isValid($this->_request->getPost())):
                            $this->registrarEquipos($frmEquipo,'add');                                                      
                    endif;

            else:
                    $ubigeo['departamento']=$tableUbigeo::DEPARTAMENTO_INICIAL;
                    $ubigeo['provincia']=$tableUbigeo::PROVINCIA_INICIAL;
                    $ubigeo['distrito']=$tableUbigeo::DISTRITO_INICIAL;

                    $myUbigeo->setUbigeo($frmEquipo,$tableUbigeo,$ubigeo);
            endif;
            $this->view->frmEquipoDeportivo = $frmEquipo;
    }
    
    public function updateAction(){
            $myAuth=new My_Auth('frontend');
            $auth = Zend_Auth::getInstance();        
            $myAuth->authExiste($auth);

            if(!isset($auth->getIdentity()->frontend)):
                $this->_redirect('inicio/index');
            endif;
            
            Zend_Layout::getMvcInstance()->assign('title','Panel de mantenimineto');//seo
            $urlWeb = $this->getFrontController()->getBaseUrl();
            $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/videos-fotos.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/registro-equipo.js');
            
            $tableEquipos=new Application_Model_DbTable_Equipos();
            $tableUbigeo=new Application_Model_DbTable_Ubigeo();
            $myUbigeo=new My_Ubigeo();
            
            $id=$this->getParam('id',0);
            $frmEquipo = new Application_Form_Equipos($id);
            
            if($this->getRequest()->isPost()):
                    $request=$this->_request->getPost();

                    $ubigeo['departamento']=$request['departamento'];
                    $ubigeo['provincia']=$request['provincia'];
                    $ubigeo['distrito']=$request['distrito'];

                    $myUbigeo->setUbigeo($frmEquipo,$tableUbigeo,$ubigeo);

                    if($frmEquipo->isValid($this->_request->getPost())):                           
                            $this->registrarEquipos($frmEquipo,'update');                                                      
                    endif;

            else:
                
                    if((int)$id==0){
                            $this->redirect('admin/login/index');
                    }
                    
                    $getEquipo=$tableEquipos->getEquipo($id);
                    
                    $frmEquipo->getElement('txtNombre')->setValue($getEquipo['nombre']);
                    $frmEquipo->getElement('txtDeporte')->setValue($getEquipo['deporte']);
                    $frmEquipo->getElement('sltDisciplina')->setValue($getEquipo['id_disciplina']);
                    $frmEquipo->getElement('txtDireccionpostal')->setValue($getEquipo['direccion_postal']);
                    $frmEquipo->getElement('txtTelefono')->setValue($getEquipo['telefono']);
                    $frmEquipo->getElement('txtEmail')->setValue($getEquipo['email']);
                    
                    $ubigeo['departamento']=$getEquipo['departamento'];
                    $ubigeo['provincia']=$getEquipo['provincia'];
                    $ubigeo['distrito']=$getEquipo['distrito'];
                    
                    $myUbigeo->setUbigeo($frmEquipo,$tableUbigeo,$ubigeo);
                    
                    $tableEquiposFotos=new Application_Model_DbTable_EquiposFotos();            
                    $this->view->fotos=$tableEquiposFotos->getFotos($id);

                    $tableEquiposVideos=new Application_Model_DbTable_EquiposVideos();
                    $this->view->videos=$tableEquiposVideos->getVideos($id);
                    
                    //-------------------------------------------------------------------
                    $tableEventosEquipos=new Application_Model_DbTable_EventosEquipos();
                    $getEventosEquipos=$tableEventosEquipos->getParticipa($id);
                                        
                    if(count($getEventosEquipos)==1):                        
                        $frmEquipo->getElement('idEveEqui')->setValue($getEventosEquipos[0]['id']);
                    endif;
                    //------------------------------------------------------------------- 
                    
                    $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/equipos/update';
            endif;
            
            $this->view->frmEquipoDeportivo = $frmEquipo;
    }
    
    
    public function borrarAction(){//redirect
		$myAuth=new My_Auth('frontend');
                $auth = Zend_Auth::getInstance();        
                $myAuth->authExiste($auth);

                if(!isset($auth->getIdentity()->frontend)):
                    $this->_redirect('inicio/index');
                endif;
                
                Zend_Layout::getMvcInstance()->assign('title','Panel de mantenimineto');//seo
                
                $id = $this->_getParam('id', 0);
                $tableEquipo = new Application_Model_DbTable_Equipos();
                $data['estado']=$tableEquipo::ELIMINADO;
                $data['usuarioregistro']=(int)$this->_auth->id;
		$tableEquipo->deleteEquipo($data,$id);
                
                $existe = $this->_getParam('existe', 0);
                
                $this->_redirect('usuarios/equipos/listaequipos/existe/'.$existe);
	}
        
    /*
    public function destacarAction(){
            if($this->_auth->usuario <> 'frontend'):        
                $this->_redirect('inicio/index');
            endif;
            
            Zend_Layout::getMvcInstance()->assign('title','Panel de mantenimineto');//seo
            
            $id=$this->_request->getParam('id');
            $act=$this->_request->getParam('act');
            $tableUsuarioAdmin = new Application_Model_DbTable_Equipos();
            $tableUsuarioAdmin->destacarEquipo($id,$act);
            $this->_redirect($_SERVER['HTTP_REFERER']);
    }
   */
        
    public function asignarequipoAction(){
        
        $myAuth=new My_Auth('frontend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->frontend)):
            $this->_redirect('inicio/index');
        endif;
        
        Zend_Layout::getMvcInstance()->assign('title','Panel de mantenimineto');//seo
        
        $id=$this->_request->getParam('id',0);
        $this->view->idEquipo=$id;
        $existe=$this->_request->getParam('existe',0);
        
        if((int)$id==0):
            $this->_redirect('inicio/index');
        endif;
        
        $tableEquipos=new Application_Model_DbTable_Equipos();        
        $this->view->equipo=$tableEquipos->getEquipoSolo($id);
        
        $frmAsignarEquipo=new Application_Form_AsignarEquipo($id);
        $this->view->frmAsignarEquipo=$frmAsignarEquipo;
        
        $tableEventosEquipos=new Application_Model_DbTable_EventosEquipos();
        $this->view->participa=$tableEventosEquipos->getParticipa($id);
        
        $this->view->existe=$existe;
        
        /*
        $numeroRegistros=25;
        $rangoPaginas=10;

        $paginador = Zend_Paginator::factory($getEquipos);
        $paginador->setItemCountPerPage($numeroRegistros)
                                ->setCurrentPageNumber((int)$page)
                                ->setPageRange($rangoPaginas);

        $this->view->participa=$paginador;
        */
        
        
        
    }
    
    public function addasignarequipoAction(){//redirect
        
        $myAuth=new My_Auth('frontend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->frontend)):
            $this->_redirect('inicio/index');
        endif;
        
        Zend_Layout::getMvcInstance()->assign('title','Panel de mantenimineto');//seo
        
        $frmAsignarEquipo=new Application_Form_AsignarEquipo();
        
         if( $this->getRequest()->isPost()):
                
                if($frmAsignarEquipo->isValid($this->_request->getPost())):
                    
                    $data['id_eventos'] = $frmAsignarEquipo->getValue('eventos');
                    $data['id_equipos'] = $frmAsignarEquipo->getValue('equipos');
                    $data['usuarioregistro'] = (int)$this->_auth->id;
                    $data['fecharegistro'] = date('Y-m-d H:i:s');
                    
                    $tableEventosEquipos=new Application_Model_DbTable_EventosEquipos();
                    $data['estado'] = $tableEventosEquipos::ACTIVO;
                    
                    $respExiste=$tableEventosEquipos->existe($data['id_equipos'],$data['id_eventos']);
                    
                    $existe=$tableEventosEquipos::INACTIVO;
                    
                    if((int)$respExiste['count'] > 0):
                           $existe=$tableEventosEquipos::ACTIVO;
                           $this->_redirect('/usuarios/equipos/asignarequipo/id/'.$data['id_equipos'].'/existe/'.$existe);                    
                    else:   
                        $resp=$tableEventosEquipos->addEquipos($data);
                            
                        if((int)$resp>0):
                            $this->_redirect('/usuarios/equipos/asignarequipo/id/'.$data['id_equipos'].'/existe/2');
                        endif;
                    endif;
                    
                endif;
                
         endif;
    }
    
    public function deleteasignarequiposAction(){//redirect
        $myAuth=new My_Auth('frontend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->frontend)):
            $this->_redirect('inicio/index');
        endif;
        
        Zend_Layout::getMvcInstance()->assign('title','Panel de mantenimineto');//seo
        
         $id=$this->_request->getParam('id',0);
         $idEquipo=$this->_request->getParam('equipo',0);
         
         if((int)$id==0):
            $this->_redirect('admin/login');
        endif;
        
        $tableEventosEquipos=new Application_Model_DbTable_EventosEquipos();
        $tableEventosEquipos->deleteEquipos($id);
        
        $this->_redirect('/usuarios/equipos/asignarequipo/id/'.(int)$idEquipo.'/existe/3');
        
    }

}

