<?php

class Usuarios_FederacionesController extends Zend_Controller_Action
{

    public function init()
    {
        
        
    	Zend_Layout::getMvcInstance()->assign('menu', 'federaciones');
    	
    	//calendario aside
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
    	
    	$myCalendario=new My_Calendario();
    	
    	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));
    	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
    	
    	$this->view->diasMes=$diasMes;
    	$this->view->diaSemana=$diaSemana;
    	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
    	
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
    	
    	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        
        // configuracion para obtener las imagenes de noticias
         
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
        $this->view->urlgetimagen = $config->ruta->fotos->mostrar.'/noticia/';
        
        //wiget noticias
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $this->view->wgtnoticia = $tableNoticias->getWigetNoticias();            
         
    }
	
    public function indexAction(){
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
                $this->view->rutaFotos=$config->ruta->fotos->mostrar.'/federaciones/';
        
                Zend_Layout::getMvcInstance()->assign('title', 'Federaciones');//seo
	    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
	    	$this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/federaciones.js');
	    	
			$id=0;
			$frmComboFederaciones=new Application_Form_ComboFederaciones($id);
			$this->view->formComboFederaciones = $frmComboFederaciones;
			
			if($this->getRequest()->isPost()){
			
				$request=$this->_request->getPost();
				
				$id=$request['federaciones'];
    	
				if((int)$id==0):
					$this->_redirect('/federaciones/');
				else:
					$this->_redirect('/federaciones/detalle/id/'.$id);
				endif;
			}
    		
               
    }
    
    public function detalleAction(){
        
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        $this->view->rutaFotos=$config->ruta->fotos->mostrar.'/federaciones/';
        
        
    	$id=$this->_request->getParam('id',0);
    	
    	if((int)$id==0):
    		$this->_redirect('/inicio/index');
    	endif;
    	
        $tableFederaciones = new Application_Model_DbTable_Federaciones();         
        
        $getFederacion=$tableFederaciones->getFederacion($id);
       
        $this->view->federacion=$getFederacion;
        Zend_Layout::getMvcInstance()->assign('title', $getFederacion['nombre']);//seo
              
        $this->view->rutaBase = $config->ruta->redes->urlredes;
               
                
        //NOTICAS
        //---------------------------------------------------------------------------------------------        
        $this->view->rutaFotosNoticias=$config->ruta->fotos->mostrar.'/noticia/';
        $page = $this->_getParam('page', 0);
        
        $this->view->page=$page;
        $this->view->parametros=$id;
        
        if((int)$page<=0):    		
                $page=1;
        endif;
                
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $getNoticias=$tableNoticias->getNoticasDeFederacion($id);
        
        $numeroRegistros=3;
        $rangoPaginas=10;

        $paginador = Zend_Paginator::factory($getNoticias);
        $paginador->setItemCountPerPage($numeroRegistros)
                                ->setCurrentPageNumber((int)$page)
                                ->setPageRange($rangoPaginas);
         
        $this->view->noticias=$paginador;
        //---------------------------------------------------------------------------------------------
    }
    
    public function noticiadetalleAction(){
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

        $this->view->rutaFotos = $config->ruta->fotos->mostrar."/noticia/";        
        $this->view->rutaBase = $config->ruta->redes->urlredes;        
    	$id=$this->_request->getParam('id');
        $this->view->idNoticia=$this->_request->getParam('idfed',0);
        
        $tableNoticia = new Application_Model_DbTable_Noticia();
        
        $row=$tableNoticia->getNoticia($id);

        $row['titulo'] = $row['titulo'];
        $row['resumen'] = $row['resumen'];
        $row['descripcion'] = $row['descripcion'];
        $row['id'] = $row['id'];
        $row['nombre'] = $row['nombre'];
        
        
        $this->view->noticia=$row;
        
        Zend_Layout::getMvcInstance()->assign('title',$row['titulo']);//seo
        Zend_Layout::getMvcInstance()->assign('description',$row['resumen']);//seo
    }

}

