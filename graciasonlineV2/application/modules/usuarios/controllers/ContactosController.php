<?php

class Usuarios_ContactosController extends Zend_Controller_Action
{

    public function init()
    {
        //calendario aside
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
    	 
    	$myCalendario=new My_Calendario();
    	 
    	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));
    	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
    	 
    	$this->view->diasMes=$diasMes;
    	$this->view->diaSemana=$diaSemana;
    	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
    	 
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
    	 
    	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        
        /*
         * configuracion para obtener las imagenes de noticias
         */
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
        $this->view->urlgetimagen = $config->ruta->fotos->mostrar.'/noticia/';
        
        //wiget noticias
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $this->view->wgtnoticia = $tableNoticias->getWigetNoticias();
    }
    
    public function indexAction()
    {
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/contactos.js');
      
        $frmContactos=new Application_Form_Contactos();
        $this->view->frmContactos=$frmContactos;
        
        Zend_Layout::getMvcInstance()->assign('title', 'Contáctenos');//seo
        
    }

    public function enviaremailAction(){//ajax
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $frmContactos=new Application_Form_Contactos();
            
            $data['resp']=0;    
            if( $this->getRequest()->isPost()):                    
                    if($frmContactos->isValid($this->_request->getPost())):
                        $email = $frmContactos->getValue('txtEmail');
                        $descripcion=$frmContactos->getValue('textDescripcion');
                        
                        $mensaje="Email: ".$email."<br/><br/>";
                        $mensaje.=$email." nos está enviando un mensaje del Portal IPD:<br/><br/>";
                        $mensaje.=$descripcion;
                        try {
                                $mails = new Zend_Mail();
                                $mails->setBodyText('Email de contactos');

                                $mails->setBodyHtml($mensaje);
                                //$mails->setFrom('campusvirtual@ipd.gpb.pe', 'Genera un nuevo ingreso');
                                $mails->setFrom('campusvirtual@ipd.gpb.pe', 'Mensaje de contactos');

                                $mails->addTo($email, 'Portal IPD' );
                                $mails->setSubject('Portal IPD Contactos');

                                if($mails->send()){
                                    $data['resp']=1;
                                }else{
                                    $data['resp']=0;
                                }
                            } catch (Exception $e){                                    
                                    $data['resp']=0;
                            }
                         
                    endif;
            endif;
            
            $json = Zend_Json::encode($data);
            echo $json;
    }

}

