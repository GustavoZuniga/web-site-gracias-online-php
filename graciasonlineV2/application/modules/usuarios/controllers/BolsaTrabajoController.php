<?php

class Usuarios_BolsaTrabajoController extends Zend_Controller_Action
{

    public function init()
    {
    	Zend_Layout::getMvcInstance()->assign('menu', 'bolsa-trabajo');
    	
    	//calendario aside
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
    	
    	$myCalendario=new My_Calendario();
    	
    	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));
    	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
    	
    	$this->view->diasMes=$diasMes;
    	$this->view->diaSemana=$diaSemana;
    	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
    	
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
    	
    	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        
        // configuracion para obtener las imagenes de noticias         
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        $this->view->urlgetimagen = $config->ruta->fotos->mostrar.'/noticia/';
        
        //wiget noticias
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $this->view->wgtnoticia = $tableNoticias->getWigetNoticias();
    }

    public function indexAction(){
        Zend_Layout::getMvcInstance()->assign('title','Bolsa de Trabajo' );//seo        
        
        $page = $this->_getParam('page', 0);
        $this->view->page=$page;
        $this->view->parametros="";
        
        if((int)$page<=0):    		
                $page=1;
        endif;
        
        $frmBolsaTrabajoBusqueda=new Application_Form_AvisosBuscar();
        $tableAvisos = new Application_Model_DbTable_Avisos();
        
        $avisos = $tableAvisos->getListadoAvisos();
       
        $numeroRegistros=5;
        $rangoPaginas=10;
       
        $paginador = Zend_Paginator::factory($avisos);
        $paginador->setItemCountPerPage($numeroRegistros)
                                ->setCurrentPageNumber((int)$page)
                                ->setPageRange($rangoPaginas);
         
        $this->view->avisos=$paginador;
        //------------------------------------------------------------
        
        $this->view->frmBolsaTrabajoBusqueda=$frmBolsaTrabajoBusqueda;
    }
    
    public function detalleAction(){
        
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
        $this->view->rutaBase = $config->ruta->redes->urlredes;
    	
        $id=$this->_request->getParam('id');
        
        $tableAviso = new Application_Model_DbTable_Avisos();
       
        $row=$tableAviso->getAvisos($id);

        $row['titulo'] = $row['titulo'];
        $row['descripcion'] = $row['descripcion'];
        $row['id'] = $id;
        $this->view->aviso=$row;
        
        Zend_Layout::getMvcInstance()->assign('title',$row['titulo'] );//seo
       
        $length=$config->seo->description->length;
        $myFunciones=new My_Funciones();
        $description=$myFunciones->newString($length,$row['descripcion'],'...');
        Zend_Layout::getMvcInstance()->assign('description',$description );//seo

    }
    
    /*filtro de busqueda*/
    public function buscarAction(){
        
        Zend_Layout::getMvcInstance()->assign('title','Búsqueda de bolsa de trabajo' );//seo
        
        $page = $this->_getParam('page', 0);
        $buscarDisciplina=trim($this->_getParam('disciplina',0));
        $buscarDepartamento=trim($this->_getParam('departamento',0));
        $buscarFecha=trim($this->_getParam('fecha',0));
               
    	$this->view->page=$page;
        $this->view->param1="";
        $this->view->param2="";
        $this->view->param3="";
        
        if((int)$page<=0):    		
                $page=1;
        endif;
        
    	 $frmBolsaTrabajoBusqueda = new Application_Form_AvisosBuscar();
         $tableAvisos = new Application_Model_DbTable_Avisos();
        
        if($this->getRequest()->isPost()):
            if($frmBolsaTrabajoBusqueda->isValid($this->_request->getPost())):
                $disciplina = $frmBolsaTrabajoBusqueda->getValue('sltDisciplina');
                $departamento = $frmBolsaTrabajoBusqueda->getValue('sltDepartamento');
                $fecha = $frmBolsaTrabajoBusqueda->getValue('sltFecha');
                
                $frmBolsaTrabajoBusqueda->getElement('sltDisciplina')->setValue($disciplina);
                $frmBolsaTrabajoBusqueda->getElement('sltDepartamento')->setValue($departamento);
                $frmBolsaTrabajoBusqueda->getElement('sltFecha')->setValue($fecha);
                
                $data['sltDisciplina'] = $disciplina;
                $data['sltDepartamento'] = $departamento;
                $data['sltFecha'] = $fecha;
               
                $avisos = $tableAvisos->buscar($data);
               
                $buscarDisciplina = $data['sltDisciplina'];
                $buscarDepartamento = $data['sltDepartamento'];
                $buscarFecha = $data['sltFecha'];
                
            endif;
        else:            
                $data['sltDisciplina'] = $buscarDisciplina;
                $data['sltDepartamento'] = $buscarDepartamento;
                $data['sltFecha'] = $buscarFecha;
                $frmBolsaTrabajoBusqueda->getElement('sltDisciplina')->setValue($buscarDisciplina);
                $frmBolsaTrabajoBusqueda->getElement('sltDepartamento')->setValue($buscarDepartamento);
                $frmBolsaTrabajoBusqueda->getElement('sltFecha')->setValue($buscarFecha);
                $avisos = $tableAvisos->buscar($data);            
        endif;
        
        $this->view->param1=$buscarDisciplina;
        $this->view->param2=$buscarDepartamento;
        $this->view->param3=$buscarFecha;
        
        $numeroRegistros=5;
        $rangoPaginas=10;
       
        $paginador = Zend_Paginator::factory($avisos);
        $paginador->setItemCountPerPage($numeroRegistros)
                                ->setCurrentPageNumber((int)$page)
                                ->setPageRange($rangoPaginas);
         
        $this->view->avisos=$paginador;
        $this->view->frmBolsaTrabajoBusqueda=$frmBolsaTrabajoBusqueda;
    }
    public function buscar2Action(){
        
        $data = $this->getRequest()->getParams();
        
        

        $tableAvisos = new Application_Model_DbTable_Avisos();
        
        
        $avisos = $tableAvisos->buscar($data);
       
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        
   
        $paginator = Zend_Paginator::factory($avisos);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        
        $this->view->paginator = $paginator;
        
        
    }
    
    /*    */
    public function postularAction()
    {
        $id=$this->_request->getParam('id');
        
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        $this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/postular.js');//cambiar esta ruta esta harcodeada
        $frmPostular=new Application_Form_Postular($id); 
        
     
        
        if( $this->getRequest()->isPost() ){     
            
        	if($frmPostular->isValid($this->_request->getPost())){
                       
        	       $data['nombrecompleto'] = $frmPostular->getValue('txtNombresCompletoPo');
                       $data['email'] = $frmPostular->getValue('txtEmailPo');
                       $data['ciudad'] = $frmPostular->getValue('txtCiudadPo');
                       $data['pais'] = $frmPostular->getValue('pais');
                       $data['comentario'] = $frmPostular->getValue('textComentarioPo');
                       $data['fecharegistro'] = date('Y-m-d H:i:s');
                       $data['aviso_id'] = $frmPostular->getValue('id');
                       
                       $tableUser=new Application_Model_DbTable_Postulante();
                       
                       $rptaUser = $tableUser->addPostulante($data);
                       
                       if((int)$rptaUser > 0){
                           $this->view->respRegistro=1;
                       }else{
                           $this->view->respRegistro=0;
                       }
        	}        
        }
        
        $this->view->frmPostular=$frmPostular;
    }


}

