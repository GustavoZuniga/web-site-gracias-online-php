<?php

class Usuarios_SedeDeportivaController extends Zend_Controller_Action
{

    public function init()
    {
    	Zend_Layout::getMvcInstance()->assign('menu', 'sede-deportiva');
    	
    	//calendario aside
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
    	
    	$myCalendario=new My_Calendario();
    	
    	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));
    	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
    	
    	$this->view->diasMes=$diasMes;
    	$this->view->diaSemana=$diaSemana;
    	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
    	
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
    	
    	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        /*
         * configuracion para obtener las imagenes de noticias
         */
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
        $this->view->urlgetimagen = $config->ruta->fotos->mostrar.'/noticia/';
        
        //wiget noticias
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $this->view->wgtnoticia = $tableNoticias->getWigetNoticias();
    }
	
    public function indexAction(){
                Zend_Layout::getMvcInstance()->assign('title', 'Sedes deportivas');//seo
	    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
	    	$this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
	    	
    		$page = $this->_getParam('page', 0);                
                $buscar=$this->_getParam('ubigeo', 0);
                
    		$this->view->page=$page;
                $this->view->parametros="";
                
    		if((int)$page<=0):    		
    			$page=1;
    		endif;
                
    		$tableSedeDeportiva=new Application_Model_DbTable_SedeDeportiva();
    		$frmBuscarSedes=new Application_Form_BuscarSedesDeportivas();
    		$tableUbigeo=new Application_Model_DbTable_Ubigeo();    		
    		$myUbigeo=new My_Ubigeo();    		
    		
    		if($this->getRequest()->isPost() ):    			
	    		$request=$this->_request->getPost();
	    			    		
	    		$ubigeo['departamento']=$request['departamento'];
	    		$ubigeo['provincia']=$request['provincia'];
	    		$ubigeo['distrito']=$request['distrito'];
	    		 
	    		$myUbigeo->setUbigeo($frmBuscarSedes, $tableUbigeo, $ubigeo);//seteamos
    			$getUbigeo=$tableUbigeo->getUbicasion($ubigeo['departamento'], $ubigeo['provincia'], $ubigeo['distrito']);
    			
	    		$getListar=$tableSedeDeportiva->getBuscarSedes($getUbigeo['id']);	    		
                        
	    		$buscar=$getUbigeo['id'];
    		else:                           
                        if((int)$buscar>0):
                            $getUbigeoNombres=$tableUbigeo->getUbigeo((int)$buscar);
                            $ubigeo['departamento']=$getUbigeoNombres['departamento'];
                            $ubigeo['provincia']=$getUbigeoNombres['provincia'];
                            $ubigeo['distrito']=$getUbigeoNombres['distrito'];
                            
                            $getListar=$tableSedeDeportiva->getBuscarSedes($buscar);
                        else:
                            $ubigeo['departamento']=$tableUbigeo::DEPARTAMENTO_INICIAL;
                            $ubigeo['provincia']=$tableUbigeo::PROVINCIA_INICIAL;
                            $ubigeo['distrito']=$tableUbigeo::DISTRITO_INICIAL;
                            
                            $getListar=$tableSedeDeportiva->getListaSedeDeportiva();    
                        endif;
                        
	    		$myUbigeo->setUbigeo($frmBuscarSedes, $tableUbigeo, $ubigeo);//seteamos	    		
    		endif;
    		                
    		$this->view->parametros=$buscar;
                
    		$numeroRegistros=5;
    		$rangoPaginas=10;
    		
    		$paginador = Zend_Paginator::factory($getListar);
    		$paginador->setItemCountPerPage($numeroRegistros)
			    		->setCurrentPageNumber((int)$page)
			    		->setPageRange($rangoPaginas);
    		
    		$this->view->sedesDeportivas=$paginador;
                $this->view->frmBuscarSedes=$frmBuscarSedes;
                
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
                $this->view->rutaFotos=$config->ruta->fotos->mostrar.'/sedes-deportivas/';
    }
    
    public function detalleAction(){
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        $this->view->rutaFotos=$config->ruta->fotos->mostrar.'/sedes-deportivas/';
        
    	$id=$this->_request->getParam('id',0);
    	
    	if((int)$id==0):
    		$this->_redirect('/inicio/index');
    	endif;
    	
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/galeria.js');
    	
    	Zend_Layout::getMvcInstance()->assign('plugin', 'galeria');
        
        $tableSedes = new Application_Model_DbTable_SedeDeportiva();                
        $getSede=$tableSedes->getSedeDeportiva($id);
        
        $this->view->sede=$getSede;
        Zend_Layout::getMvcInstance()->assign('title', $getSede['nombre']);//seo
        
        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
        $getUbigeo=$tableUbigeo->getUbigeo($getSede['ubigeo']);
        $this->view->ubigeo=$getUbigeo;
        
        $tableSedeDeportivaFotos=new Application_Model_DbTable_SedeDeportivaFotos();
        $this->view->fotos=$tableSedeDeportivaFotos->getFotos($id);        
        $this->view->rutaBase = $config->ruta->redes->urlredes;
       
    }
    
    public function fotoAction(){
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/galeria-sola.js');
    	
    	$id=$this->_getParam('id',0);
    	if((int)$id>0):	    	
	    	$tableSedeDeportiva=new Application_Model_DbTable_SedeDeportiva();
                $getSede=$tableSedeDeportiva->getSedeDeportiva($id);
    		$this->view->sedes=$getSede;
                Zend_Layout::getMvcInstance()->assign('title', $getSede['nombre'].' (Fotos)');//seo
    		
	    	$tableSedeDeportivaFotos=new Application_Model_DbTable_SedeDeportivaFotos();
	    	$getFotos=$tableSedeDeportivaFotos->getFotos($id);
	    	
	    	$this->view->sedesFotos=$getFotos;
	    	$this->view->countFotos=count($getFotos);
	    	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
	    	$this->view->rutaFotos=$config->ruta->fotos->mostrar.'/sedes-deportivas/';
                $this->view->rutaBase = $config->ruta->redes->urlredes;
	    	
	    	$this->view->id=$id;
    	else:
    		$this->_redirect('usuarios/inicio/index');
    	endif;
    }


}

