<?php

class Usuarios_InicioController extends Zend_Controller_Action
{

    public function init()
    {
        $options=array('layout'=>'layout');//cambiamos de layout
        Zend_Layout::startMvc($options);
        
        Zend_Layout::getMvcInstance()->assign('menu', 'inicio');
        
    }

    public function indexAction()
    {	
		Zend_Layout::getMvcInstance()->assign('title','Learn Spanish Online From Your Home, Work or on the Move with a Professional Native Spanish Teacher on Skype| Gracias Online Spanish Language School');//seo
		Zend_Layout::getMvcInstance()->assign('description','Learn Spanish 1 on 1 with a professional tutor on Skype.  Take lessons with a private Spanish speaking teacher. Take online Spanish classes when & where you want. Lessons available 24x7. Study Spanish via Skype with your own online language school. All native Spanish tutors have college degrees and years of experience teaching Spanish as a second language to foreigners');//seo
		Zend_Layout::getMvcInstance()->assign('keywords','Live Spanish Language Courses, Online Spanish Language School, Live Native Spanish Teachers, Online Courses, Online Spanish School, One-on-one Spanish Language course, United States, US, Brazil, United Kingdom, Europe,');//seo
		
        $myCalendario=new My_Calendario();
        
     	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));        
     	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
     	
     	$this->view->diasMes=$diasMes;
     	$this->view->diaSemana=$diaSemana;
     	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
     	
     	$tableEventos=New Application_Model_DbTable_Eventos();
		/*echo "Usuarios_InicioController";exit();
     	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
     	echo "11111111111";exit();
     	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        //Zend_Debug::dump($this->view->dataEventos);exit;
        echo "11111111111";exit();*/
        /*
         * configuracion para obtener las imagenes de noticias
         */
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	        
        $this->view->rutaFotosNoticias = $config->ruta->fotos->mostrar.'/noticia/';
        $this->view->rutaFotosEquipos = $config->ruta->fotos->mostrar.'/equipos/';
        
        $tableNoticias = new Application_Model_DbTable_Noticia();
       
        $row = $tableNoticias->getWigetNoticias();
        
        $this->view->wgtnoticia = $row;
        
        //Consulta para mostrar equipos destacados
        $tableEquipo = new Application_Model_DbTable_Equipos();
        $rowEquipos = $tableEquipo->getEquiposDestacados();
        $this->view->wgtequipos = $rowEquipos;
    }
    
    public function getcalendarioAction(){//ajax
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	
    	$year=$this->_getParam('year');
    	$month=$this->_getParam('month');
    	
        $myCalendario=new My_Calendario();    	
    	$diaSemana=$myCalendario->diaSemana($year, $month);
    	$diasMes=$myCalendario->getMonthDays($year, $month);
    	
    	$array["diasMes"]=$diasMes;
    	$array["diaSemana"]=$diaSemana;
    	$array["nombreMes"]=$myCalendario->meses((int)$month);
    	
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes($month.'/'.$year);
    	
    	$array['eventos']=$myCalendario->calendario($getEventos,$year,$month,$diasMes);
    	$array['year']=$year;
    	$array['month']=$month;
    	
    	$json = Zend_Json::encode($array);
    	echo $json;
    	    	
    }
    
}

