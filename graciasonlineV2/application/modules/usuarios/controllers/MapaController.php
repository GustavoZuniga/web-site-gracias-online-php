<?php

class Usuarios_MapaController extends Zend_Controller_Action
{

    public function init()
    {
        //calendario aside
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
    	
    	$myCalendario=new My_Calendario();
    	
    	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));
    	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
    	
    	$this->view->diasMes=$diasMes;
    	$this->view->diaSemana=$diaSemana;
    	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
    	
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
    	
    	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        /*
         * configuracion para obtener las imagenes de noticias
         */
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
        $this->view->urlgetimagen = $config->ruta->fotos->mostrar.'/noticia/';
        
        //wiget noticias
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $this->view->wgtnoticia = $tableNoticias->getWigetNoticias();
    }

    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->assign('title', 'Mapa del sitio');//seo
    }


}

