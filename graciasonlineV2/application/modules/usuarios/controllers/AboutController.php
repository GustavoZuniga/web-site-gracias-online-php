<?php

class Usuarios_AboutController extends Zend_Controller_Action
{
    
    public function init()
    {
    	//Zend_Layout::getMvcInstance()->assign('menu', 'about');
    	
    	//calendario aside
    	//$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	//$this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/calendario.js');
    	/*
    	$myCalendario=new My_Calendario();
    	
    	$diaSemana=$myCalendario->diaSemana(date("Y"), date("m"));
    	$diasMes=$myCalendario->getMonthDays(date("Y"), date("m"));
    	
    	$this->view->diasMes=$diasMes;
    	$this->view->diaSemana=$diaSemana;
    	$this->view->nombreMes=$myCalendario->meses((int)date("m"));
    	
    	$tableEventos=New Application_Model_DbTable_Eventos();
    	$getEventos=$tableEventos->getEventosMes(date("m").'/'.date("Y"));
    	
    	$this->view->dataEventos=$myCalendario->calendario($getEventos,date('Y'),date('m'),$diasMes);
        
		//configuracion para obtener las imagenes de noticias
         
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);        
        $this->view->urlgetimagen=$config->ruta->fotos->mostrar.'/noticia/';
        
        //wiget noticias
        $tableNoticias = new Application_Model_DbTable_Noticia();
        $this->view->wgtnoticia = $tableNoticias->getWigetNoticias();
        */
    }

    public function indexAction(){
	
		/*
			Zend_Layout::getMvcInstance()->assign('title','');//seo
			Zend_Layout::getMvcInstance()->assign('description','');//seo
			Zend_Layout::getMvcInstance()->assign('keywords','');//seo
		*/
        Zend_Layout::getMvcInstance()->assign('title','Study Spanish Online from Your Home, Work or On the Move with a Live Spanish Speaking Tutor |Gracias Online Spanish Language School');//seo
		Zend_Layout::getMvcInstance()->assign('description','Live online Spanish courses with native teachers. Study Spanish in private one-on-one lessons and improve your Spanish skills today! Click here and learn more!');//seo
		Zend_Layout::getMvcInstance()->assign('keywords','Best online Spanish language course, best Native Spanish teachers and tutors, Best online Spanish school, Spanish Online, United States, US, Brazil, United Kingdom, Europe, China, Japan ');//seo
        
        //$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
                
        /*$this->view->rutaFotos=$config->ruta->fotos->mostrar.'/noticia/';        
        $this->view->rutaBase = $config->ruta->redes->urlredes;
        
        $page = $this->_getParam('page', 0);
        $buscar=trim($this->_getParam('buscar',''));
        
        $this->view->page=$page;
        $this->view->parametros="";
        
        if((int)$page<=0):    		
                $page=1;
        endif;
                
        $tableNoticia = new Application_Model_DbTable_Noticia();
        $frmNoticias=new Application_Form_BuscarNoticias();
        
        if($this->getRequest()->isPost()):
            if($frmNoticias->isValid($this->_request->getPost())):
                $search = trim($frmNoticias->getValue('txtBusqueda'));            
                $getNoticias=$tableNoticia->buscar($search);
                $buscar=$search;
            else:
                $this->_redirect('noticias/index');
            endif;
        else:
            if(strlen($buscar)>0):
                $getNoticias=$tableNoticia->buscar($buscar);
            else:                
                $getNoticias=$tableNoticia->getListaNoticias();
            endif;
        endif;
        
        $this->view->parametros=$buscar;
        
        $numeroRegistros=3;
        $rangoPaginas=10;

        $paginador = Zend_Paginator::factory($getNoticias);
        $paginador->setItemCountPerPage($numeroRegistros)
                                ->setCurrentPageNumber((int)$page)
                                ->setPageRange($rangoPaginas);
         
        $this->view->noticias=$paginador;
        $this->view->frmNoticias=$frmNoticias;*/
    }
    
    public function detalleAction(){
        
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

        $this->view->rutaFotos = $config->ruta->fotos->mostrar."/noticia/";        
        $this->view->rutaBase = $config->ruta->redes->urlredes;        
    	$id=$this->_request->getParam('id');
        
        $tableNoticia = new Application_Model_DbTable_Noticia();
        
        $row=$tableNoticia->getNoticia($id);

        $row['titulo'] = $row['titulo'];
        $row['resumen'] = $row['resumen'];
        $row['descripcion'] = $row['descripcion'];
        $row['id'] = $row['id'];
        $row['nombre'] = $row['nombre'];
        
        
        $this->view->noticia=$row;
        
        Zend_Layout::getMvcInstance()->assign('title',$row['titulo']);//seo
        Zend_Layout::getMvcInstance()->assign('description',$row['resumen']);//seo
        
    }
    

}

