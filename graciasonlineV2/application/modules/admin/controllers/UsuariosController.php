<?php

class Admin_UsuariosController extends Zend_Controller_Action
{
    public function init()
    {        
       $options=array('layout'=>'layout-admin');
       Zend_Layout::startMvc($options);
       Zend_Layout::getMvcInstance()->assign('menuSelect', 1);
       $urlWeb = $this->getFrontController()->getBaseUrl();
       $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-usuario.js');
              
       $myAuth=new My_Auth('backend');
       $auth = Zend_Auth::getInstance();        
       $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
    }
    
    public function indexAction() {
       $urlWeb = $this->getFrontController()->getBaseUrl();
       $this->view->headScript()->appendFile($urlWeb.'/static/admin/js/jquery.reveal.js');
       
        $tableUsuario = new Application_Model_DbTable_UsuarioAdmin();
        $usuarios = $tableUsuario->getListaUsuariosAdmin();
     
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        $paginator = Zend_Paginator::factory($usuarios);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->paginator = $paginator;
    }
    
    public function nuevoAction(){
        
        $urlWeb = $this->getFrontController()->getBaseUrl();
        $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-usuario.js');
        
        $id=0;
        $frmRegistrarUsuario = new Application_Form_RegistroUsuario($id);
        $this->view->formUsuario = $frmRegistrarUsuario;
        if($this->getRequest()->isPost()){
            if($frmRegistrarUsuario->isValid($this->_request->getPost())){

                $data['username'] = $frmRegistrarUsuario->getValue('txtUsername');
                $data['password'] = md5($frmRegistrarUsuario->getValue('txtPassword'));
                $data['nombres'] = $frmRegistrarUsuario->getValue('txtNombres');
                $data['apellidos'] = $frmRegistrarUsuario->getValue('txtApellidos');
                $data['email'] = $frmRegistrarUsuario->getValue('txtEmail');
                $data['fecharegistro'] = date('Y-m-d H:i:s');

                $tableUsuario=new Application_Model_DbTable_UsuarioAdmin();
                   
                if($tableUsuario->addUsuario($data)){
                    $this->view->respRegistro=1;
                    $this->_redirect('admin/usuarios');
                }else{
                    $this->view->respRegistro=0;
                }

            }
        }
    }
	
    public function editarAction(){
           
        
        $id=$this->_request->getParam('id');
        
        $tableUsuario = new Application_Model_DbTable_UsuarioAdmin();
        
        $frmUpdUsuario = new Application_Form_RegistroUsuario($id);
                
		if ($this->getRequest()->isPost()) {
			if ($frmUpdUsuario->isValid($this->getRequest()->getPost())) {
				
				$data['id'] = $frmUpdUsuario->getValue('id');
				$data['nombres'] = $frmUpdUsuario->getValue('txtNombres');
				$data['apellidos'] = $frmUpdUsuario->getValue('txtApellidos');
                                $data['username'] = $frmUpdUsuario->getValue('txtUsername');
                                $data['password'] = md5($frmUpdUsuario->getValue('txtPassword'));
                                $data['email'] = $frmUpdUsuario->getValue('txtEmail');
				
				
                                if($tableUsuario->updateUsuario($data)){
                                    $this->view->respRegistro=1;
                                    return $this->_redirect('admin/usuarios');
                                }else{
                                    $this->view->respRegistro=0;
                                }
				//Vamos a la página principal de la aplicación
				//$this->_redirect('/');
			}else{//Si los datos del formulario, no son válidos, se muestra el formulario con los datos de nuevo.
				$id = $this->_getParam('id', 0);
                                if ($id > 0) {

                                        $getUsuario=$tableUsuario->getUsuarios($id);
                                        $frmUpdUsuario->getElement('id')->setValue($getUsuario['id']);
                                        $frmUpdUsuario->getElement('txtNombres')->setValue($getUsuario['nombres']);
                                        $frmUpdUsuario->getElement('txtApellidos')->setValue($getUsuario['apellidos']);
                                        $frmUpdUsuario->getElement('txtUsername')->setValue($getUsuario['username']);
                                        $frmUpdUsuario->getElement('txtPassword')->setValue($getUsuario['password']);
                                        $frmUpdUsuario->getElement('txtEmail')->setValue($getUsuario['email']);
                                        $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/usuarios/editar';

                                }
			}
		}else{//Mostramos los datos en caso de no haber enviado los datos al servidor para actualizar 
			
                        $id = $this->_getParam('id', 0);
			if ($id > 0) { 
                                $tableUsuario = new Application_Model_DbTable_UsuarioAdmin();
                               
                                $getUsuario=$tableUsuario->getUsuariosAdmin($id);
                                $frmUpdUsuario->getElement('id')->setValue($getUsuario['id']);
                                $frmUpdUsuario->getElement('txtNombres')->setValue($getUsuario['nombres']);
                                $frmUpdUsuario->getElement('txtApellidos')->setValue($getUsuario['apellidos']);
                                $frmUpdUsuario->getElement('txtUsername')->setValue($getUsuario['username']);
                                $frmUpdUsuario->getElement('txtPassword')->setValue($getUsuario['password']);
                                $frmUpdUsuario->getElement('txtEmail')->setValue($getUsuario['email']);
                                $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/usuarios/editar';
                                
                        }
		}
                
                $this->view->formUsuario = $frmUpdUsuario;
    }
	
	public function borrarAction(){
           
		if ($this->getRequest()->isPost()) {
			$del = $this->getRequest()->getPost('del');
			if ($del == 'Si') {
				$id = $this->getRequest()->getPost('id');
                                $tableUsuarioAdmin = new Application_Model_DbTable_UsuarioAdmin();
				$tableUsuarioAdmin->deleteUsuarioAdmin($id);
			}
                        return $this->_redirect('admin/usuarios');
			//$this->_redirect($_SERVER['HTTP_REFERER']);
		}else{
			$id = $this->_getParam('id', 0);
			$tableUsuarioAdmin = new Application_Model_DbTable_UsuarioAdmin();
			$this->view->usuario = $tableUsuarioAdmin->getUsuariosAdmin($id);
		}		
	}
        
        public function activarAction(){
            $id=$this->_request->getParam('id');
            $act=$this->_request->getParam('act');
            $tableUsuarioAdmin = new Application_Model_DbTable_UsuarioAdmin();
            $tableUsuarioAdmin->activarUsuarioAdmin($id,$act);
            $this->_redirect($_SERVER['HTTP_REFERER']);
        }
  

}

