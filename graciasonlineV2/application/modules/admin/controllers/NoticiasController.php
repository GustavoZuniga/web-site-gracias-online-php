<?php

class Admin_NoticiasController extends Zend_Controller_Action
{
    protected $_auth;
    
    public function init()
    {        
       $options=array('layout'=>'layout-admin');//cambiamos de layout
       Zend_Layout::startMvc($options);
       Zend_Layout::getMvcInstance()->assign('menuSelect', 5);
       $urlWeb = $this->getFrontController()->getBaseUrl();
       $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-noticia.js');
       
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $this->_auth= $auth->getIdentity()->backend;
                
    }
    
    public function indexAction() {

        $urlWeb = $this->getFrontController()->getBaseUrl();
       $this->view->headScript()->appendFile($urlWeb.'/static/admin/js/jquery.reveal.js');
       
        $tableNoticia = new Application_Model_DbTable_Noticia();
        
        $noticias = $tableNoticia->getListaNoticias();
        
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        //Zend_Debug::dump($post); exit();
        $paginator = Zend_Paginator::factory($noticias);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        
        $this->view->paginator = $paginator;
    }
    
    public function listadoAction(){
        $tableNoticia = new Application_Model_DbTable_Noticia();
        $noticias = $tableNoticia->fetchAll();
        
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        //Zend_Debug::dump($post); exit();
        $paginator = Zend_Paginator::factory($noticias);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        
        $this->view->paginator = $paginator;
    }

    public function nuevoAction(){
	
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);			
        $ruta=$config->ruta->fotos->subir.'/noticia/';
    
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-noticia.js');
        $this->view->headScript()->appendFile($urlWeb.'/static/admin/js/nicEdit.js');
        //Instanciamos el formulario
        $id=0;
        $frmRegistrarNoticia = new Application_Form_RegistroNoticia($id);
        $this->view->formNoticia = $frmRegistrarNoticia;
                
        if($this->getRequest()->isPost()){
            if($frmRegistrarNoticia->isValid($this->_request->getPost())){
                $data['id_federaciones'] = $frmRegistrarNoticia->getValue('federaciones');
                $data['titulo'] = $frmRegistrarNoticia->getValue('txtTitulo');
                $data['resumen'] = $frmRegistrarNoticia->getValue('txtResumen');
                $data['descripcion'] = $frmRegistrarNoticia->getValue('txtDescripcion');
                $data['lugar'] = $frmRegistrarNoticia->getValue('txtLugar');
                $data['fecha_registro'] = date('Y-m-d H:i:s');                
                $data['usuario_registro'] = (int)$this->_auth->id;
                

               $upload = new Zend_File_Transfer_Adapter_Http();
               $filename = $upload->getFileName('imagen',false);//noombre del archivo de texto
               
               $tableNoticia=new Application_Model_DbTable_Noticia();
               $respNoticia=$tableNoticia->addRegistro($data);
               
               if(!empty($filename)){                        
                	$upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$filename);
                	$upload->addValidator('Size',true,array('min'=>'2KB','max'=>'2048KB'),$filename);
                	$upload->setDestination($ruta);
                	
                        if ($upload->isValid()) {
                                 $file_array = explode('.', $filename);
                                 $new_filename = 'noticia_'.$respNoticia.'.'. $file_array[1];
                                 $upload->addFilter('Rename', $new_filename);
                                 $upload->receive();
                                 
                                 $tableNoticiaImagen = new Application_Model_DbTable_NoticiaImagen();
                                 $dataImagen['noticia_id'] = $respNoticia;
                                 $dataImagen['nombre'] = $new_filename;
                                 $dataImagen['ruta'] = $ruta;

                                 $tableNoticiaImagen->addRegistro($dataImagen);
                        }                
                }
                                
                if((int)$respNoticia>0){
                    $this->view->respRegistro=1;
                    $this->_redirect('admin/noticias');
                }else{
                    $this->view->respRegistro=0;
                }
                
            }
        }
    }
	
	public function editarAction(){
            
            $urlWeb = $this->getFrontController()->getBaseUrl();
            $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-noticia.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/admin/js/nicEdit2.js');
            
           $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);			
           $ruta=$config->ruta->fotos->subir.'/noticia/';
           
		$this->view->title = "Editar Noticia";
		$this->view->headTitle($this->view->title, 'PREPEND');
		
                
                $id=$this->_request->getParam('id');
				
		$frmRegistrarNoticia = new Application_Form_RegistroNoticia($id);
               
		if ($this->getRequest()->isPost()) {
			if ($frmRegistrarNoticia->isValid($this->getRequest()->getPost())) {
                                $data['id'] = $frmRegistrarNoticia->getValue('id');
				$data['id_federaciones'] = $frmRegistrarNoticia->getValue('federaciones');
                                $data['titulo'] = $frmRegistrarNoticia->getValue('txtTitulo');
				$data['descripcion'] = $frmRegistrarNoticia->getValue('txtDescripcion');
                                $data['resumen'] = $frmRegistrarNoticia->getValue('txtResumen');				
                                $data['lugar'] = $frmRegistrarNoticia->getValue('txtLugar');
                                $data['usuario_modificacion'] = (int)$this->_auth->id;
				$data['fecha_modificacion'] =date('Y-m-d H:i:s');
                                  
                                $upload = new Zend_File_Transfer_Adapter_Http();
                                $filename = $upload->getFileName('imagen',false);
                                 
                                if(!empty($filename)){ 
                                        
                                        $upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$filename);
                                        $upload->addValidator('Size',true,array('min'=>'2KB','max'=>'2048KB'),$filename);
                                        $upload->setDestination($ruta);

                                        if ($upload->isValid()) {
                                                $file_array = explode('.', $filename);
                                                 $new_filename = 'noticia_'.$id.'.'. $file_array[1];
                                                 $upload->addFilter('Rename', $new_filename);
                                                 $upload->receive();

                                                 $tableNoticiaImagen = new Application_Model_DbTable_NoticiaImagen();
                                                 $dataImagen['noticia_id'] = $id;
                                                 $dataImagen['nombre'] = $new_filename;
                                                 $dataImagen['ruta'] = $ruta;
                                                 
                                                 $tableNoticiaImagen->updateNoticia($dataImagen);
                                        }                                
                                }
                                                                
                                $tableNoticia = new Application_Model_DbTable_Noticia();
                                
                                if($tableNoticia->updateNoticia($data)){
                                    //$this->view->respRegistro=1
                                    return $this->_redirect('admin/noticias');
                                }else{
                                    $this->view->respRegistro=0;
                                }
                        
                        }
		}else{
			 
                        $id = $this->_getParam('id', 0);
						if ($id > 0) {
							 
                                $tableNoticia = new Application_Model_DbTable_Noticia();
                                $getNoticia=$tableNoticia->getNoticia($id);
                                $frmRegistrarNoticia->getElement('txtTitulo')->setValue($getNoticia['titulo']);
                                $frmRegistrarNoticia->getElement('txtDescripcion')->setValue($getNoticia['descripcion']);
                                $frmRegistrarNoticia->getElement('txtResumen')->setValue($getNoticia['resumen']);
                                $frmRegistrarNoticia->getElement('txtLugar')->setValue($getNoticia['lugar']);
                                
                                $this->view->imagen = $getNoticia['nombre'];
                                $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/noticias/editar';
                                $this->view->formNoticia = $frmRegistrarNoticia;
                        }
		}
                
                
	}
	
	public function borrarAction(){
		
		if ($this->getRequest()->isPost()) {
			$del = $this->getRequest()->getPost('del');
			if ($del == 'Si') {
				$id = $this->getRequest()->getPost('id');
                                $tableNoticia = new Application_Model_DbTable_Noticia();
				$tableNoticia->deleteNoticia($id);
			}
			$this->_redirect('admin/noticias'); 
		}else{
			$id = $this->_getParam('id', 0);
			$tableNoticia = new Application_Model_DbTable_Noticia();
			$this->view->noticia = $tableNoticia->getNoticia($id);
		}		
	}
        
        public function activarAction(){
            $id=$this->_request->getParam('id');
            $act=$this->_request->getParam('act');
            $tableNoticia = new Application_Model_DbTable_Noticia();
            $tableNoticia->activarNoticia($id,$act);
            $this->_redirect($_SERVER['HTTP_REFERER']);
        }


}

