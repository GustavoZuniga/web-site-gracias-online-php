<?php

class Admin_LoginController extends Zend_Controller_Action
{

    public function init()
    {        
       $options=array('layout'=>'layout-admin-login');//cambiamos de layout
       Zend_Layout::startMvc($options);
       
    }

    public function indexAction()
    {   
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/login.js');

        $usuario = new Application_Model_DbTable_Usuario();
        $frmLoginAdmin = new Application_Form_LoginAdmin();
 
        if ($frmLoginAdmin->isValid($_POST)) {            
            $authAdapter = new Zend_Auth_Adapter_DbTable($usuario->getAdapter(),'cms_admin');
            $authAdapter->setIdentityColumn('username')
                        ->setCredentialColumn('password')
                        ->setIdentity($frmLoginAdmin->getValue('username'))
                        ->setCredential(md5($frmLoginAdmin->getValue('password')));
           
            // Recogemos Zend_Auth
            $auth = Zend_Auth::getInstance();
            // Realiza la comprobación con el adaptador que hemos creado
            $result = $auth->authenticate($authAdapter);
           
            // Si la autentificación es válida
            if ($result->isValid()) {                    
                    $data = $authAdapter->getResultRowObject(array('id','username','email'));                    
                    $dataLogin['backend']=$data;
                    $auth->getStorage()->write((object)$dataLogin);
                    //----------------------------------------------------------                                                   
                    $myAuth=new My_Auth('backend');//evaluamos si existe la session
                    $myAuth->zendAuth($auth);
                    //----------------------------------------------------------
                    $this->_redirect('admin/usuarios');
 
            } else {
                
                $this->view->loginError = 'Usuario o contraseña incorrectas';
            }
 
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        $this->view->rutaBase = $config->ruta->redes->urlredes;
        $this->view->frmLoginAdmin = $frmLoginAdmin;
    }
    
    public function loginAction(){
        
        $this->_redirect('admin/noticias');
    }
    
    public function logoutAction()
    {   
        $myAuth=new My_Auth('backend');
        $myAuth->exitSession();
        $this->_redirect('/admin/login');
       
    }

}

