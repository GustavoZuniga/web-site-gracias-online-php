<?php

/**
 * @autor=Dimas Gustavo
 * @todo hace mension a los eventos del backend
 *	 
 */

class Admin_CalendarioController extends Zend_Controller_Action
{
    protected $_auth;
    
    public function init()
    {
    	$options=array('layout'=>'layout-admin');//cambiamos de layout
        Zend_Layout::startMvc($options);
        Zend_Layout::getMvcInstance()->assign('menuSelect', 3);
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $this->_auth= $auth->getIdentity()->backend;        
        
    }
    
    function agregarFotosVideos($frmRegistrar,$resp){            
            //------------------------------------------------------------------
            //FOTOS
            //------------------------------------------------------------------
           $imagenes=array();

           $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);                           
           
           $ruta=$config->ruta->fotos->subir.'/eventos/';           

            $dataFotos['id_eventos']=$resp;
            $dataFotos['ruta']=$ruta;
            $dataFotos['estado']=1;
            $dataFotos['usuarioregistro']=(int)$this->_auth->id;
            $dataFotos['fecharegistro']=date('Y-m-d');
            //------------------------------------------------------------------
                            $upload = new Zend_File_Transfer_Adapter_Http();

                            foreach($upload->getFileInfo() as $file => $info): 
                                //-------------------------------------------------------------------
                                $upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$file);
                                $upload->addValidator('Size',true,array('min'=>'2KB','max'=>'1024KB'),$file);
                                //-------------------------------------------------------------------
                                if ($upload->isValid($file)): 
                                        $file_array = explode('.', $info['name']);
                                        $new_filename = rand().'.'. $file_array[1];

                                        $dataRename['source']=$info['tmp_name'];
                                        $dataRename['target']=$ruta.$new_filename;
                                        $dataRename['overwrite']=true;

                                        $upload->addFilter('Rename',$dataRename);
                                        $upload->receive($file);

                                        $imagenes[]=$new_filename;
                                endif;
                            endforeach;        				                            
            //------------------------------------------------------------------
            $countImagenes=count($imagenes);
            if($countImagenes >0):
                    $tableEventosFotos=new Application_Model_DbTable_EventosFotos();

                    for($i=0;$i<$countImagenes;$i++):
                        $dataFotos['nombre']=$imagenes[$i];
                        $tableEventosFotos->addFotos($dataFotos);                        
                    endfor;
            endif;

            //------------------------------------------------------------------
            //VIDEOS(solo se colocaran videos de youtube)
            //------------------------------------------------------------------
            $videos=trim($frmRegistrar->getValue('getVideos'));
            if(!empty($videos)):
                   $getVideos=explode('/////',$videos);
                   $countVideos=count($getVideos);

                   if($countVideos>0):
                          $dataVideos['id_eventos']=$resp;                                                
                          $dataVideos['estado']=1;
                          $dataVideos['usuarioregistro']=(int)$this->_auth->id;
                          $dataVideos['fecharegistro']=date('Y-m-d');
                         $tableEventosVideos=new Application_Model_DbTable_EventosVideos();

                         for($i=0;$i<$countVideos;$i++):
                             $dataVideos['nombre']=$getVideos[$i];
                             $tableEventosVideos->addVideos($dataVideos);                             
                         endfor;
                   endif;
            endif;
                        
    }
    
    function eliminarFotosVideos($frmRegistrar){
        
        $fotosEliminados=trim($frmRegistrar->getValue('getFotosEliminados'));
        if(strlen($fotosEliminados)>0){            
            $arrayIdsFotos=explode("/////",$fotosEliminados);
            $tableEventosFotos=new Application_Model_DbTable_EventosFotos();            
            $nombresFotos=$tableEventosFotos->getGrupoFotos($arrayIdsFotos);
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
            $ruta=$config->ruta->fotos->subir.'/eventos/';
            
            //eliminamos fisicamente
            foreach ($nombresFotos as $row):
            		unlink($ruta.$row['nombre']);
            endforeach;
            
            $tableEventosFotos->deleteFotos($arrayIdsFotos);            
        }
        
        $getVideosEliminados=trim($frmRegistrar->getValue('getVideosEliminados'));
        if(strlen($getVideosEliminados)>0){            
            $arrayIdsVideos=explode("/////",$getVideosEliminados);
            $tableEventosVideos=new Application_Model_DbTable_EventosVideos();
            $tableEventosVideos->deleteVideos($arrayIdsVideos);            
        }
        
    }
    
    function registrarEvento($frmRegistrar,$estado='add'){//auxiliar de registrar update y modificar evento
                           $data['nombre_evento']=$frmRegistrar->getValue('txtNombreEvento');
                           $data['descripcion']=$frmRegistrar->getValue('textDescripcion');                           
                           $data['lugar']=$frmRegistrar->getValue('txtLugar');
                           
                           $departamento=trim($frmRegistrar->getValue('departamento'));
                           $provincia=trim($frmRegistrar->getValue('provincia'));
                           $distrito=trim($frmRegistrar->getValue('distrito'));
                           
                           $fechaInicio=explode('/',$frmRegistrar->getValue('txtFechaInicio'));
                           $newFechaInicio=$fechaInicio[2].'-'.$fechaInicio[1].'-'.$fechaInicio[0];
                           
                           $data['fecha_inicio']=$newFechaInicio;
                           
                           $fechaFin=explode('/',$frmRegistrar->getValue('txtFechaFin'));
                           $newFechaFin=$fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
                           
                           $data['fecha_fin']=$newFechaFin;
                           $data['hora']=date('Y-m-d').' '.$frmRegistrar->getValue('txtHora').':00';
                                                      
                           $data['estado']=1;
                           
                           $data['mapa_latitud']=$frmRegistrar->getValue('mapaLatitud');
                           $data['mapa_longitud']=$frmRegistrar->getValue('mapaLongitud');
                          
                           $data['email']=$frmRegistrar->getValue('txtEmail');
                           $data['telefono']=$frmRegistrar->getValue('txtTelefono');
                           $data['twitter']=$frmRegistrar->getValue('txtTwitter');
                           $data['facebook']=$frmRegistrar->getValue('txtFacebook');
                           
                           $tableEventos=new Application_Model_DbTable_Eventos();
                           
                           $data['quien_creo']=$tableEventos::CREO_ADMINISTRADOR;
                           
                           try{
                                        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
                                        $getData=$tableUbigeo->getUbicasion($departamento, $provincia, $distrito);
                                        $data['ubigeo']=$getData['id'];
                                        //------------------------------------------------------------------
                                        
                                        if($estado=='add'){
                                            $data['fecharegistro']=date('Y-m-d');
                                            $data['usuarioregistro']=(int)$this->_auth->id;
                                            $resp=$tableEventos->addEventos($data);
                                        }else{//update
                                            $data['fechamodificasion']=date('Y-m-d');
                                            $data['usuariomodificasion']=(int)$this->_auth->id;
                                            $id=$frmRegistrar->getValue('id');
                                            $resp=$tableEventos->updateEventos($data, $id);
                                            
                                            
                                            
                                            if((int)$resp>0):
                                            	$resp=$id;
                                            else:
                                            	$resp=0;
                                            endif;
                                            
                                            if($estado=='update'){
                                            	$this->eliminarFotosVideos($frmRegistrar);
                                            	$resp=$id;
                                            }
                                        }
                                        //------------------------------------------------------------------

                                        if((int)$resp > 0):                                                        
                                                        $this->agregarFotosVideos($frmRegistrar,$resp);                                                        
                                                        $this->_redirect('admin/calendario/listaeventos');                                                         
                                        endif;                 
                                         
                           } catch (Zend_Db_Exception $e) {
                                    $this->getMessenger()->error('Error al Actulizar.');
                                     echo $e->getMessage();
                           } catch (Zend_Exception $e) {
                                    $this->getMessenger()->error('error desconocido');
                                     echo $e->getMessage();
                           }
    }
    
    function setUbigeo($frmRegistrar,$tableUbigeo,$ubigeo){//seteamos en modificar registrar
	    	$getProvincias=$tableUbigeo->getProvincias($ubigeo['departamento']);
	    
	    	$getDepartamentos=$tableUbigeo->getDepartamentos();
	    
	    	foreach ($getDepartamentos as $rowDepa):
	    	$frmRegistrar->getElement('departamento')->addMultiOption($rowDepa['departamento'], $rowDepa['departamento']);
	    	endforeach;
	    
	    	foreach ($getProvincias as $rowProvincias):
	    	$frmRegistrar->getElement('provincia')->addMultiOption($rowProvincias['provincia'], $rowProvincias['provincia']);
	    	endforeach;
	    
	    	$getDistritos=$tableUbigeo->getDistritos($ubigeo['provincia']);
	    
	    	foreach ($getDistritos as $rowDistrito):
	    	$frmRegistrar->getElement('distrito')->addMultiOption($rowDistrito['distrito'], $rowDistrito['distrito']);
	    	endforeach;
	    
	    	//DEFAULT
	    	$frmRegistrar->getElement('departamento')->setValue($ubigeo['departamento']);
	    	$frmRegistrar->getElement('provincia')->setValue($ubigeo['provincia']);
	    	$frmRegistrar->getElement('distrito')->setValue($ubigeo['distrito']);
    }
    
    public function indexAction()
    {
        $this->_redirect('admin/calendario/listaeventos');
    }

    public function listadoAction(){
        Zend_Layout::getMvcInstance()->assign('title', 'Eventos del día');//seo
        
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube    	
        $this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/pag.js');
        
        $page = $this->_getParam('page', 0);
    	$this->view->page=$page;
        if((int)$page<=0):    		
                $page=1;
        endif;
        
        $fecha=$this->_getParam('fecha','00-00-1900');
        
        
        $fechaArray=explode('-',$fecha);
        
        if(checkdate($fechaArray[1],$fechaArray[0],$fechaArray[2])){//el formato para validar es (mes dia año)
                $tableEvento=new Application_Model_DbTable_Eventos();                
                $getEventos=$tableEvento->getEventoDia($fechaArray[1].'/'.$fechaArray[2]);
                
                $myCalendario = new My_Calendario();                
                $getEventosDia=$myCalendario->newDataListaDia($getEventos,$fechaArray);
                
                //----------------------------------------------------------------------------------------
                $this->view->parametros=$fecha;
                
                $numeroRegistros=15;
                $rangoPaginas=10;
                
                $paginador = Zend_Paginator::factory($getEventosDia);
                $paginador->setItemCountPerPage($numeroRegistros)
                                        ->setCurrentPageNumber((int)$page)
                                        ->setPageRange($rangoPaginas);

                $this->view->eventos=$paginador;
                //----------------------------------------------------------------------------------------
                
                $this->view->titulo=$fechaArray[0]." DE ".$myCalendario->meses($fechaArray[1])." DE ".$fechaArray[2];
                
        }else{//quiere jackear
                $this->redirect('usuarios/inicio/index');
        }
    }
    
    
    
    public function getprovinciasAction(){//ajax
        $this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
        $departamento=$this->_getParam('departamento','AREQUIPA');
        
        $myUbigeo=new My_Ubigeo();
        echo $myUbigeo->getProvincias($departamento);
    }
    
    public function getdistritosAction(){//ajax
        $this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
        $provincia=$this->_getParam('provincia','AREQUIPA');
        
        $myUbigeo=new My_Ubigeo();
        echo $myUbigeo->getDistritos($provincia);
    }
    
    public function listaeventosAction(){
        Zend_Layout::getMvcInstance()->assign('title','Lista de Eventos');//seo
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
                
        $id=(int)$this->_auth->id;  
        
        $page = $this->_getParam('page', 0);
                
    	$this->view->page=$page;        
        
        if((int)$page<=0):    		
                $page=1;
        endif;
        
        $tableEventos=new Application_Model_DbTable_Eventos();
        $getEventos=$tableEventos->listadoEventos($id);
        //----------------------------------------------------------------------------------------

        $numeroRegistros=10;
        $rangoPaginas=10;
        
        $paginador = Zend_Paginator::factory($getEventos);
        $paginador->setItemCountPerPage($numeroRegistros)
                                ->setCurrentPageNumber((int)$page)
                                ->setPageRange($rangoPaginas);

        $this->view->eventos=$paginador;
        //----------------------------------------------------------------------------------------
        
    }
            
    //MANTENIMIENTO DE EVENTOS    
    public function addAction(){
            Zend_Layout::getMvcInstance()->assign('title','Registro de eventos');//seo
            
            $myAuth=new My_Auth('backend');
            $auth = Zend_Auth::getInstance();        
            $myAuth->authExiste($auth);

            if(!isset($auth->getIdentity()->backend)):
                $this->_redirect('admin/login');
            endif;
            
            $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube 
            $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/videos-fotos.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/registro-eventos.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/jquery.maskedinput.js');
            Zend_Layout::getMvcInstance()->assign('fechas', 1);
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);                           
            
            $mapa['latitud']=$config->mapa->latitud;
            $mapa['longitud']=$config->mapa->longitud;

            Zend_Layout::getMvcInstance()->assign('googleMap', $mapa);//datos para el google maps
            
            $this->view->respRegistro=0;
            $tableUbigeo=new Application_Model_DbTable_Ubigeo();
            $frmRegistrar=new Application_Form_Eventos();
            
            if( $this->getRequest()->isPost()):
		            $request=$this->_request->getPost();
		             
		            $ubigeo['departamento']=$request['departamento'];
		            $ubigeo['provincia']=$request['provincia'];
		            $ubigeo['distrito']=$request['distrito'];
		             
		            $this->setUbigeo($frmRegistrar,$tableUbigeo,$ubigeo);
		            
                            if($frmRegistrar->isValid($this->_request->getPost())):
                                $this->registrarEvento($frmRegistrar,'add');                            
                            endif;
                    
            else:           
		            
		            $ubigeo['departamento']=$tableUbigeo::DEPARTAMENTO_INICIAL;
		            $ubigeo['provincia']=$tableUbigeo::PROVINCIA_INICIAL;
		            $ubigeo['distrito']=$tableUbigeo::DISTRITO_INICIAL;
		            
		            $this->setUbigeo($frmRegistrar,$tableUbigeo,$ubigeo);//Seteo ubigeo
            endif;
            
            $this->view->frmRegistrarEventos=$frmRegistrar;            
    }
    
    public function deleteAction(){
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $id=$this->_getParam('id',0);
        
        $tableEventos=new Application_Model_DbTable_Eventos();
        $data['estado']=$tableEventos::ESTADO_ELIMINADO;        
        $data['usuariomodificasion']=(int)$this->_auth->id;
        
        if($tableEventos->deleteEvento($data, $id)):
            $this->redirect('admin/calendario/listaeventos');
        endif;
    } 
    
    public function updateAction(){
        Zend_Layout::getMvcInstance()->assign('title','Edición de Eventos');//seo
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
        $this->view->headScript()->appendFile($urlWeb.'/static/js/videos-fotos.js');
        $this->view->headScript()->appendFile($urlWeb.'/static/js/frontend/registro-eventos.js');
        
        $this->view->headScript()->appendFile($urlWeb.'/static/js/jquery.maskedinput.js');
        Zend_Layout::getMvcInstance()->assign('fechas', 1);        
        
        
        $tableEventos=new Application_Model_DbTable_Eventos();
        
        $id=$this->getParam('id',0);
        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
        $frmRegistrar=new Application_Form_Eventos($id);
        
        if( $this->getRequest()->isPost()):        			
                            $request=$this->_request->getPost();

                            $ubigeo['departamento']=$request['departamento'];
                            $ubigeo['provincia']=$request['provincia'];
                            $ubigeo['distrito']=$request['distrito'];

                            $this->setUbigeo($frmRegistrar,$tableUbigeo,$ubigeo);//Seteo ubigeo por el cambio de select por el ajax, sin este seteo nunca validara el request

                            if($frmRegistrar->isValid($this->_request->getPost())):	                        
                                    $this->registrarEvento($frmRegistrar,'update');
                            endif;
	                
        else:
            
		            if((int)$id==0){
		                $this->redirect('admin/login');
		            }
		            
		            $getEvento=$tableEventos->getEvento($id);
		            
		            $frmRegistrar->getElement('txtNombreEvento')->setValue($getEvento['nombre_evento']);
		            $frmRegistrar->getElement('textDescripcion')->setValue($getEvento['descripcion']);
		            $frmRegistrar->getElement('txtLugar')->setValue($getEvento['lugar']);
		            $frmRegistrar->getElement('txtFechaInicio')->setValue($getEvento['fecha_inicio']);
                            $frmRegistrar->getElement('txtFechaFin')->setValue($getEvento['fecha_fin']);
		            $frmRegistrar->getElement('txtHora')->setValue($getEvento['hora']);
		            $frmRegistrar->getElement('mapaLatitud')->setValue($getEvento['mapa_latitud']);
		            $frmRegistrar->getElement('mapaLongitud')->setValue($getEvento['mapa_longitud']);
                            $frmRegistrar->getElement('txtEmail')->setValue($getEvento['email']);
                            $frmRegistrar->getElement('txtTelefono')->setValue($getEvento['telefono']);
                            $frmRegistrar->getElement('txtTwitter')->setValue($getEvento['twitter']);
                            $frmRegistrar->getElement('txtFacebook')->setValue($getEvento['facebook']);
                            
                            
                            if(strlen(trim($getEvento['mapa_latitud']))>0):
                                $mapa['latitud']=$getEvento['mapa_latitud'];
                                $mapa['longitud']=$getEvento['mapa_longitud'];   
                            else:
                                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);            
                                $mapa['latitud']=$config->mapa->latitud;
                                $mapa['longitud']=$config->mapa->longitud;
                            endif;
                            
                            
                            Zend_Layout::getMvcInstance()->assign('googleMap', $mapa);//datos para el google maps
                            
		            $getUbigeo=$tableUbigeo->getUbigeo($getEvento['ubigeo']);
		            
		            $ubigeo['departamento']=$getUbigeo['departamento'];
		            $ubigeo['provincia']=$getUbigeo['provincia'];
		            $ubigeo['distrito']=$getUbigeo['distrito'];
		            
		            $this->setUbigeo($frmRegistrar,$tableUbigeo,$ubigeo);//Seteo ubigeo
		            
		            $tableEventosFotos=new Application_Model_DbTable_EventosFotos();            
		            $this->view->fotos=$tableEventosFotos->getFotos($id);
		            
		            $tableEventosVideos=new Application_Model_DbTable_EventosVideos();
		            $this->view->videos=$tableEventosVideos->getVideos($id);
		            
		            $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/usuarios/calendario/update';
        endif;
        
        $this->view->frmRegistrarEventos=$frmRegistrar;
        
    }    
   
}

