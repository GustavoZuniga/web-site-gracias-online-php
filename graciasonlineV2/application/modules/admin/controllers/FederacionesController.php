<?php

class Admin_FederacionesController extends Zend_Controller_Action
{
    protected $_auth;
    
    public function init()
    {       
        $options=array('layout'=>'layout-admin');//cambiamos de layout
        Zend_Layout::startMvc($options);
        Zend_Layout::getMvcInstance()->assign('menuSelect', 3);
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $this->_auth= $auth->getIdentity()->backend;
    }
    
    function agregarLogo($frmRegistrar,$resp){            
            //------------------------------------------------------------------
            //FOTOS
            //------------------------------------------------------------------           
           $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
           $ruta=$config->ruta->fotos->subir.'/federaciones/';
            //------------------------------------------------------------------
                            
                            $upload = new Zend_File_Transfer_Adapter_Http();
                            foreach($upload->getFileInfo() as $file => $info): 
                                //-------------------------------------------------------------------
                                $upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$file);
                                $upload->addValidator('Size',true,array('min'=>'2KB','max'=>'1024KB'),$file);
                                //-------------------------------------------------------------------
                                if ($upload->isValid($file)): 
                                        $new_filename = $info['name'];

                                        $dataRename['source']=$info['tmp_name'];
                                        $dataRename['target']=$ruta.$new_filename;
                                        $dataRename['overwrite']=true;

                                        $upload->addFilter('Rename',$dataRename);
                                        $upload->receive($file);
                                        
                                        $data['logo']=trim($new_filename);
                                        
                                        $tableFederaciones=new Application_Model_DbTable_Federaciones();
                                        $tableFederaciones->updateFederacion($data, $resp);
                                endif;
                            endforeach;           
            //------------------------------------------------------------------           
    }
    
    function registrarEvento($frmRegistrar,$estado='add'){//auxiliar de registrar update y modificar evento
                           $departamento=trim($frmRegistrar->getValue('departamento'));
                           $provincia=trim($frmRegistrar->getValue('provincia'));
                           $distrito=trim($frmRegistrar->getValue('distrito'));
                           
                           $data['nombre']=$frmRegistrar->getValue('txtNombre');
                           $data['direccion']=$frmRegistrar->getValue('txtDireccion');                           
                           $data['telefono']=$frmRegistrar->getValue('txtTelefono');
                           $data['web']=$frmRegistrar->getValue('txtWeb');
                           $data['email']=$frmRegistrar->getValue('txtEmail');
                           $data['facebook']=$frmRegistrar->getValue('txtFacebook');
                           $data['twitter']=$frmRegistrar->getValue('txtTwitter');
                           $data['resolucion']=$frmRegistrar->getValue('txtResolucion');
                           $data['presidente']=$frmRegistrar->getValue('txtPresidente');
                           $data['vicepresidente']=$frmRegistrar->getValue('txtVisepresidente');
                           $data['secretario']=$frmRegistrar->getValue('txtSecretario');
                           $data['tesorero']=$frmRegistrar->getValue('txtTesorero');
                           $data['vocal']=$frmRegistrar->getValue('txtVocal');
                          
                           $tableFederaciones=new Application_Model_DbTable_Federaciones();
                           
                           $data['estado']=$tableFederaciones::ACTIVO;
                           
                           try{
                                        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
                                        $getData=$tableUbigeo->getUbicasion($departamento, $provincia, $distrito);
                                        $data['ubigeo']=$getData['id'];
                                        //------------------------------------------------------------------
                                        
                                        if($estado=='add'){
                                            $data['fecharegistro']=date('Y-m-d');
                                            $data['usuarioregistro']=(int)$this->_auth->id;
                                            $resp=$tableFederaciones->addFederaciones($data);
                                        }else{//update
                                            $data['fechamodificacion']=date('Y-m-d');
                                            $data['usuariomodificacion']=(int)$this->_auth->id;
                                            $id=$frmRegistrar->getValue('id');
                                            $resp=$tableFederaciones->updateFederacion($data, $id);
                                            
                                            if((int)$resp>0):
                                            	$resp=$id;
                                            else:
                                            	$resp=0;
                                            endif;
                                            
                                            if($estado=='update'){
                                            	$this->eliminarLogo($frmRegistrar);
                                            	$resp=$id;
                                            }
                                        }
                                        //------------------------------------------------------------------

                                        if((int)$resp > 0):                                                        
                                                        $this->agregarLogo($frmRegistrar,$resp);                                                        
                                                        $this->_redirect('admin/federaciones/index');                                                         
                                        endif;                 
                                         
                           } catch (Zend_Db_Exception $e) {
                                    $this->getMessenger()->error('Error al Actulizar.');
                                     echo $e->getMessage();
                           } catch (Zend_Exception $e) {
                                    $this->getMessenger()->error('error desconocido');
                                     echo $e->getMessage();
                           }
    }
    
    function eliminarLogo($frmRegistrar){
        $id=trim($frmRegistrar->getValue('id'));

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        $ruta=$config->ruta->fotos->subir.'/federaciones/';
        
        $upload = new Zend_File_Transfer_Adapter_Http();
        foreach($upload->getFileInfo() as $file => $info):
            $tableFederaciones=new Application_Model_DbTable_Federaciones();            
            $getFederacion=$tableFederaciones->getFederacion($id);
        
            unlink($ruta.$getFederacion['logo']);//eliminamos fisicamente
        
            $data['logo']='';
            $tableFederaciones->updateFederacion($data,$id);
        endforeach;                           
    }
    
    public function indexAction(){
                Zend_Layout::getMvcInstance()->assign('title','Lista de Federaciones');//seo
        
                $myAuth=new My_Auth('backend');
                $auth = Zend_Auth::getInstance();        
                $myAuth->authExiste($auth);

                if(!isset($auth->getIdentity()->backend)):
                    $this->_redirect('admin/login');
                endif;

                $id=(int)$this->_auth->id;  

                $page = $this->_getParam('page', 0);

                $this->view->page=$page;        

                if((int)$page<=0):    		
                        $page=1;
                endif;

                $tableFederaciones=new Application_Model_DbTable_Federaciones();
                $getFederaciones=$tableFederaciones->getListaFederaciones($id);//por el momento no usamos el id
                //----------------------------------------------------------------------------------------
                
                $numeroRegistros=10;
                $rangoPaginas=10;

                $paginador = Zend_Paginator::factory($getFederaciones);
                $paginador->setItemCountPerPage($numeroRegistros)
                                        ->setCurrentPageNumber((int)$page)
                                        ->setPageRange($rangoPaginas);

                $this->view->federaciones=$paginador;
                //----------------------------------------------------------------------------------------
               
    }
    
    //MANTENIMIENTO DE EVENTOS    
    public function addAction(){
            Zend_Layout::getMvcInstance()->assign('title','Registro de federaciones');//seo
            
            $myAuth=new My_Auth('backend');
            $auth = Zend_Auth::getInstance();
            $myAuth->authExiste($auth);

            if(!isset($auth->getIdentity()->backend)):
                $this->_redirect('admin/login');
            endif;
            
            $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube 
            $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/federaciones.js');
                        
            $this->view->respRegistro=0;
            $tableUbigeo=new Application_Model_DbTable_Ubigeo();
            $frmFederaciones=new Application_Form_Federaciones();
            $myUbigeo=new My_Ubigeo();
            
            if( $this->getRequest()->isPost()):
		            $request=$this->_request->getPost();
		             
		            $ubigeo['departamento']=$request['departamento'];
		            $ubigeo['provincia']=$request['provincia'];
		            $ubigeo['distrito']=$request['distrito'];
		             
		            $myUbigeo->setUbigeo($frmFederaciones,$tableUbigeo,$ubigeo);
		            
                            if($frmFederaciones->isValid($this->_request->getPost())):
                                $this->registrarEvento($frmFederaciones,'add');                            
                            endif;
                    
            else:           
		            
		            $ubigeo['departamento']=$tableUbigeo::DEPARTAMENTO_INICIAL;
		            $ubigeo['provincia']=$tableUbigeo::PROVINCIA_INICIAL;
		            $ubigeo['distrito']=$tableUbigeo::DISTRITO_INICIAL;
		            
		            $myUbigeo->setUbigeo($frmFederaciones,$tableUbigeo,$ubigeo);//Seteo ubigeo
                            
            endif;
            
            $this->view->frmFederaciones=$frmFederaciones;
            
    }
    
    public function updateAction(){
        Zend_Layout::getMvcInstance()->assign('title','Edición de Federaciones');//seo
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');        
        $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/federaciones.js');
                
        $tableFederaciones=new Application_Model_DbTable_Federaciones();
        
        $id=$this->getParam('id',0);
        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
        $frmRegistrar=new Application_Form_Federaciones($id);
        $myUbigeo=new My_Ubigeo();
        
        if( $this->getRequest()->isPost()):        			
                            $request=$this->_request->getPost();

                            $ubigeo['departamento']=$request['departamento'];
                            $ubigeo['provincia']=$request['provincia'];
                            $ubigeo['distrito']=$request['distrito'];

                            $myUbigeo->setUbigeo($frmRegistrar,$tableUbigeo,$ubigeo);//Seteo ubigeo por el cambio de select por el ajax, sin este seteo nunca validara el request

                            if($frmRegistrar->isValid($this->_request->getPost())):	                        
                                    $this->registrarEvento($frmRegistrar,'update');
                            endif;
	                
        else:
            
		            if((int)$id==0){
		                $this->redirect('admin/login');
		            }
		            
		            $getFederacion=$tableFederaciones->getFederacion($id);
		            
		            $frmRegistrar->getElement('txtNombre')->setValue($getFederacion['nombre']);
		            $frmRegistrar->getElement('txtDireccion')->setValue($getFederacion['direccion']);
		            $frmRegistrar->getElement('txtTelefono')->setValue($getFederacion['telefono']);
		            $frmRegistrar->getElement('txtWeb')->setValue($getFederacion['web']);
                            $frmRegistrar->getElement('txtEmail')->setValue($getFederacion['email']);
                            $frmRegistrar->getElement('txtResolucion')->setValue($getFederacion['resolucion']);
                            $frmRegistrar->getElement('txtPresidente')->setValue($getFederacion['presidente']);
                            $frmRegistrar->getElement('txtVisepresidente')->setValue($getFederacion['vicepresidente']);
                            $frmRegistrar->getElement('txtSecretario')->setValue($getFederacion['secretario']);
                            $frmRegistrar->getElement('txtTesorero')->setValue($getFederacion['tesorero']);
                            $frmRegistrar->getElement('txtVocal')->setValue($getFederacion['vocal']);
                            $frmRegistrar->getElement('txtFacebook')->setValue($getFederacion['facebook']);
		            $frmRegistrar->getElement('txtTwitter')->setValue($getFederacion['twitter']);
                            
		            $getUbigeo=$tableUbigeo->getUbigeo($getFederacion['ubigeo']);
		            
		            $ubigeo['departamento']=$getUbigeo['departamento'];
		            $ubigeo['provincia']=$getUbigeo['provincia'];
		            $ubigeo['distrito']=$getUbigeo['distrito'];
		            
		            $myUbigeo->setUbigeo($frmRegistrar,$tableUbigeo,$ubigeo);//Seteo ubigeo
		            		            
		            $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/federaciones/update';
        endif;
        
        $this->view->frmFederaciones=$frmRegistrar;        
    }
    
    public function deleteAction(){
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
       
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $id=$this->_getParam('id',0);
        
        $tableFederaciones=new Application_Model_DbTable_Federaciones();
        $data['estado']=$tableFederaciones::ELIMINADO;        
        $data['usuariomodificacion']=(int)$this->_auth->id;
        
        $tableFederaciones->deleteFederacion($data, $id);
        $this->redirect('admin/federaciones/index');
    } 

}

