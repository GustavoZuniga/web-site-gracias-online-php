<?php

class Admin_SedeDeportivaController extends Zend_Controller_Action
{
    protected $_auth;

    public function init()
    {      
        $options=array('layout'=>'layout-admin');//cambiamos de layout
        Zend_Layout::startMvc($options);
        Zend_Layout::getMvcInstance()->assign('menuSelect', 4);
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $this->_auth= $auth->getIdentity()->backend;
    }
    
    /**
     * @autor=Dimas Gustavo
     */
    function agregarFotos($resp){            
            //------------------------------------------------------------------
            //FOTOS
            //------------------------------------------------------------------
           $imagenes=array();

           $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
           $ruta=$config->ruta->fotos->subir.'/sedes-deportivas/';

            $dataFotos['id_sededeportiva']=$resp;
            $dataFotos['ruta']=$ruta;
            $dataFotos['estado']=1;
            $dataFotos['usuarioregistro']=1;
            $dataFotos['fecharegistro']=date('Y-m-d');
            //------------------------------------------------------------------
                            $upload = new Zend_File_Transfer_Adapter_Http();

                            foreach($upload->getFileInfo() as $file => $info): 
                                //-------------------------------------------------------------------
                                $upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$file);
                                $upload->addValidator('Size',true,array('min'=>'2KB','max'=>'1024KB'),$file);
                                //-------------------------------------------------------------------
                                if ($upload->isValid($file)): 
                                        $file_array = explode('.', $info['name']);
                                        $new_filename = rand().'.'. $file_array[1];

                                        $dataRename['source']=$info['tmp_name'];
                                        $dataRename['target']=$ruta.$new_filename;
                                        $dataRename['overwrite']=true;

                                        $upload->addFilter('Rename',$dataRename);
                                        $upload->receive($file);

                                        $imagenes[]=$new_filename;
                                endif;
                            endforeach;        				                            
            //------------------------------------------------------------------
            $countImagenes=count($imagenes);
            if($countImagenes >0):
                    $tableSedeDeportivaFotos=new Application_Model_DbTable_SedeDeportivaFotos();

                    for($i=0;$i<$countImagenes;$i++):
                        $dataFotos['nombre']=$imagenes[$i];
                        $tableSedeDeportivaFotos->addFotos($dataFotos);                        
                    endfor;
                    
                    //grabar foto de inicio para que aparesca en el frontend
                    //-------------------------------------------------------------------------
                    $data['foto_inicio']=$imagenes[((int)$countImagenes-1)];//ultima imagen
                    
                    $tableSedeDeportiva=new Application_Model_DbTable_SedeDeportiva();
                    $respFotoInicio=$tableSedeDeportiva->updateSedeDeportiva($data,$resp);                    
                    //-------------------------------------------------------------------------
            endif;

            
    }
    
    /**
     * @autor=Dimas Gustavo
     */
    function eliminarFotos($frmSedeDeportiva){
        
        $fotosEliminados=trim($frmSedeDeportiva->getValue('getFotosEliminados'));
        if(strlen($fotosEliminados)>0){
        
            $arrayIdsFotos=explode("/////",$fotosEliminados);
            $tableSedeDeportivaFotos=new Application_Model_DbTable_SedeDeportivaFotos();            
            $nombresFotos=$tableSedeDeportivaFotos->getGrupoFotos($arrayIdsFotos);
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
            $ruta=$config->ruta->fotos->sedes;
        
            //eliminamos fisicamente
            foreach ($nombresFotos as $row):
            		unlink($ruta.$row['nombre']);
            endforeach;
            
            $tableSedeDeportivaFotos->deleteFotos($arrayIdsFotos);
            
            //si todabia existe la foto inicio
            $name=trim($frmSedeDeportiva->getValue('fotoInicio'));
            
            $id=trim($frmSedeDeportiva->getValue('id'));
            $getResp=$tableSedeDeportivaFotos->existeFoto($name, $id);
            
            if((int)$getResp['count']==0):            	
            	$data['foto_inicio']="";
            	$tableSedeDeportiva=new Application_Model_DbTable_SedeDeportiva();
                $respFotoInicio=$tableSedeDeportiva->updateSedeDeportiva($data,$id);
            endif;
        }       
    }
    
    /**
     * @autor=Dimas Gustavo
     */
    function registrarSedeDeportiva($frmSedeDeportiva,$estado='add'){//auxiliar de registrar update y modificar evento
                            $data['nombre']=$frmSedeDeportiva->getValue('txtNombre');
                            $data['ubicacion']=$frmSedeDeportiva->getValue('txtUbicacion');                            
                            $data['telefono']=$frmSedeDeportiva->getValue('txtTelefono');
                            $data['email']=$frmSedeDeportiva->getValue('txtEmail');
                            $data['equipamiento']=$frmSedeDeportiva->getValue('txtEquipamiento');
                            $data['horario_atencion']=$frmSedeDeportiva->getValue('txtHoraatencion');
                            $data['costo_hora']=$frmSedeDeportiva->getValue('txtCostohora');
                            $data['responsable']=$frmSedeDeportiva->getValue('txtResponsable');
                            
                            $data['estado']=1;
                                                        
                           $departamento=trim($frmSedeDeportiva->getValue('sltDepartamento'));
                           $provincia=trim($frmSedeDeportiva->getValue('sltProvincia'));
                           $distrito=trim($frmSedeDeportiva->getValue('sltDistrito'));                           
                          
                           try{
                                        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
                                        $getData=$tableUbigeo->getUbicasion($departamento, $provincia, $distrito);
                                        $data['ubigeo']=$getData['id'];
                                        
                                        $tableSede=new Application_Model_DbTable_SedeDeportiva();
                                        
                                        //------------------------------------------------------------------
                                        if($estado=='add'){
                                            $data['fecharegistro']=date('Y-m-d');
                                            $data['usuarioregistro']=(int)$this->_auth->id;
                                            $resp=$tableSede->addSedeDeportiva($data);
                                        }else{//update
                                        	                                            
                                            $data['fechamodificacion']=date('Y-m-d');
                                            $data['usuariomodificacion']=(int)$this->_auth->id;
                                            $id=$frmSedeDeportiva->getValue('id'); 
                                                                                     
                                            $resp=$tableSede->updateSedeDeportiva($data,$id);
                                            
                                            if((int)$resp>0):
                                            	$resp=$id;
                                            else:
                                            	$resp=0;
                                            endif;
                                            
                                            if($estado=='update'){
                                            	$this->eliminarFotos($frmSedeDeportiva);
                                            	$resp=$id;
                                            }
                                        }
                                        //------------------------------------------------------------------

                                        if((int)$resp > 0):
                                                        
                                                        $this->agregarFotos($resp);
                                                        
                                                        $this->_redirect('admin/sede-deportiva/index');                                                         
                                        endif;                 
                                         
                           } catch (Zend_Db_Exception $e) {
                                    $this->getMessenger()->error('Error al Actulizar.');
                                     echo $e->getMessage();
                           } catch (Zend_Exception $e) {
                                    $this->getMessenger()->error('error desconocido');
                                     echo $e->getMessage();
                           }
    }
    
    /**
     * @autor=Dimas Gustavo
     */    
    function setUbigeo($frmSedeDeportiva,$tableUbigeo,$ubigeo){//seteamos en modificar registrar
               
	    	$getProvincias=$tableUbigeo->getProvincias($ubigeo['departamento']);
	    	$getDepartamentos=$tableUbigeo->getDepartamentos();
                
	    	foreach ($getDepartamentos as $rowDepa):
	    	$frmSedeDeportiva->getElement('sltDepartamento')->addMultiOption($rowDepa['departamento'], $rowDepa['departamento']);
	    	endforeach;
                
	    	foreach ($getProvincias as $rowProvincias):
	    	$frmSedeDeportiva->getElement('sltProvincia')->addMultiOption($rowProvincias['provincia'], $rowProvincias['provincia']);
	    	endforeach;
	    
	    	$getDistritos=$tableUbigeo->getDistritos($ubigeo['provincia']);
	    
	    	foreach ($getDistritos as $rowDistrito):
	    	$frmSedeDeportiva->getElement('sltDistrito')->addMultiOption($rowDistrito['distrito'], $rowDistrito['distrito']);
	    	endforeach;
	    
	    	//DEFAULT
	    	$frmSedeDeportiva->getElement('sltDepartamento')->setValue($ubigeo['departamento']);
	    	$frmSedeDeportiva->getElement('sltProvincia')->setValue($ubigeo['provincia']);
	    	$frmSedeDeportiva->getElement('sltDistrito')->setValue($ubigeo['distrito']);
                
                
    }
    
    
    public function indexAction()
    {
        $tableSedes=new Application_Model_DbTable_SedeDeportiva();  
        
        $sedes = $tableSedes->getListaSedeDeportiva();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        
        $paginator = Zend_Paginator::factory($sedes);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        
        $this->view->paginator = $paginator;
        
    }
    
    /** 
     * @autor=Dimas Gustavo
     */
    public function nuevoAction(){
        $urlWeb = $this->getFrontController()->getBaseUrl();        
        $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
        $this->view->headScript()->appendFile($urlWeb.'/static/js/videos-fotos.js');
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/backend/sede-deportiva.js');
        
        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
        $frmSedeDeportiva = new Application_Form_SedeDeportiva();        
        
        if( $this->getRequest()->isPost() ){
                
                $request=$this->_request->getPost();
		
                $ubigeo['departamento']=$request['sltDepartamento'];
                $ubigeo['provincia']=$request['sltProvincia'];
                $ubigeo['distrito']=$request['sltDistrito'];
                
                $this->setUbigeo($frmSedeDeportiva,$tableUbigeo,$ubigeo);
                
                if($frmSedeDeportiva->isValid($this->_request->getPost())){
                    $this->registrarSedeDeportiva($frmSedeDeportiva,'add');                    
                }
                
        }else{
                $ubigeo['departamento']=$tableUbigeo::DEPARTAMENTO_INICIAL;
                $ubigeo['provincia']=$tableUbigeo::PROVINCIA_INICIAL;
                $ubigeo['distrito']=$tableUbigeo::DISTRITO_INICIAL;

                $this->setUbigeo($frmSedeDeportiva,$tableUbigeo,$ubigeo);//Seteo ubigeo
        }
        
        $this->view->frmSedeDeportiva = $frmSedeDeportiva;
    }

    
    /**
     * @autor=Dimas Gustavo
     */
    public function updateAction(){
    	    	        
    	$urlWeb = $this->getFrontController()->getBaseUrl();
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/videos-fotos.js');
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/backend/sede-deportiva.js');
    	
    	$tableSedeDeportiva=new Application_Model_DbTable_SedeDeportiva();
    	$tableUbigeo=new Application_Model_DbTable_Ubigeo();
    	
    	
    	$id=$this->getParam('id',0);
    	$frmSedeDeportiva = new Application_Form_SedeDeportiva($id);
    	
    	if( $this->getRequest()->isPost() ){
    		
    		$request=$this->_request->getPost();
    	
    		$ubigeo['departamento']=$request['sltDepartamento'];
    		$ubigeo['provincia']=$request['sltProvincia'];
    		$ubigeo['distrito']=$request['sltDistrito'];
    		
    		$this->setUbigeo($frmSedeDeportiva,$tableUbigeo,$ubigeo);
    	
    		if($frmSedeDeportiva->isValid($this->_request->getPost())){
    			$this->registrarSedeDeportiva($frmSedeDeportiva,'update');
    		}
    	
    	}else{
    		
    		if((int)$id==0){
    			$this->redirect('admin/login/index');
    		}
    		
    		$getSedeDeportiva=$tableSedeDeportiva->getSedeDeportiva($id);

    		$frmSedeDeportiva->getElement('txtNombre')->setValue($getSedeDeportiva['nombre']);
    		$frmSedeDeportiva->getElement('txtUbicacion')->setValue($getSedeDeportiva['ubicacion']);    		
    		$frmSedeDeportiva->getElement('txtEquipamiento')->setValue($getSedeDeportiva['equipamiento']);
    		$frmSedeDeportiva->getElement('txtTelefono')->setValue($getSedeDeportiva['telefono']);
    		$frmSedeDeportiva->getElement('txtEmail')->setValue($getSedeDeportiva['email']);
    		$frmSedeDeportiva->getElement('txtHoraatencion')->setValue($getSedeDeportiva['horario_atencion']);
    		$frmSedeDeportiva->getElement('txtCostohora')->setValue($getSedeDeportiva['costo_hora']);    		
    		$frmSedeDeportiva->getElement('txtResponsable')->setValue($getSedeDeportiva['responsable']);
    		$frmSedeDeportiva->getElement('txtResponsable')->setValue($getSedeDeportiva['responsable']);
    		$frmSedeDeportiva->getElement('fotoInicio')->setValue($getSedeDeportiva['foto_inicio']);
    		
    		$getUbigeo=$tableUbigeo->getUbigeo($getSedeDeportiva['ubigeo']);
    		
    		$ubigeo['departamento']=$getUbigeo['departamento'];
    		$ubigeo['provincia']=$getUbigeo['provincia'];
    		$ubigeo['distrito']=$getUbigeo['distrito'];
    		
    		$this->setUbigeo($frmSedeDeportiva,$tableUbigeo,$ubigeo);//Seteo ubigeo
    		
    		$tableSedesDeportivasFotos=new Application_Model_DbTable_SedeDeportivaFotos();
    		$this->view->fotos=$tableSedesDeportivasFotos->getFotos($id);
    		
    		$this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/sede-deportiva/update';
    		
    	}
    	
    	$this->view->frmSedeDeportiva = $frmSedeDeportiva;
    }
    

    public function detalleAction(){//ESTO NO ESTA CONTEMPLADO POR ESO FALTA HACER
        
    	$urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/backend/bolsa-trabajo.js');
    
    	$id=$this->_request->getParam('id');
    
    	if ($id > 0) {
                
    		$tableSedes=new Application_Model_DbTable_SedeDeportiva();
    		$rowSede=$tableSedes->getDetalleSedeDeportiva($id);                    
    		$this->view->sede=$rowSede;                
    	}
    }
     
   public function borrarAction(){
		
        if ($this->getRequest()->isPost()) {
                $del = $this->getRequest()->getPost('del');
                if ($del == 'Si') {
                        $id = $this->getRequest()->getPost('id');
                        $tableNoticia = new Application_Model_DbTable_SedeDeportiva();
                        $tableNoticia->deleteSedeDeportiva($id);
                }
                $this->_redirect('admin/sede-deportiva'); 
        }else{
                $id = $this->_getParam('id', 0);
                $tableSede = new Application_Model_DbTable_SedeDeportiva();
                $this->view->sede = $tableSede->getSedeDeportiva($id);
        }		
    }
    
}

