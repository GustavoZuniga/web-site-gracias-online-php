<?php

class Admin_BolsaTrabajoController extends Zend_Controller_Action
{
    protected $_auth;
    
    public function init()
    {        
        $options=array('layout'=>'layout-admin');//cambiamos de layout
        Zend_Layout::startMvc($options);
        Zend_Layout::getMvcInstance()->assign('menuSelect', 3);
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $this->_auth= $auth->getIdentity()->backend;
    }

    public function indexAction()
    {
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/backend/bolsa-trabajo.js');
        
        $frmBolsaTrabajo=new Application_Form_BolsaTrabajo();        
        
        if( $this->getRequest()->isPost() ){    		
                if($frmBolsaTrabajo->isValid($this->_request->getPost())){
                    $data['titulo']=$frmBolsaTrabajo->getValue('txtTitulo');
                    $data['descripcion']=$frmBolsaTrabajo->getValue('txtDescripcion');
                    $data['institucion']=$frmBolsaTrabajo->getValue('txtInstitucion');
                    $data['id_disciplinas']=$frmBolsaTrabajo->getValue('sltDisciplina');
                    $checkOcultar = $frmBolsaTrabajo->getValue('checkOcultar');
                   
                    if($checkOcultar==0)
                        $data['ocultar']=0;
                    else
                        $data['ocultar']=1;
                    $data['departamento']=$frmBolsaTrabajo->getValue('sltDepartamento');
                    $data['provincia']='';
                    $data['distrito']='';                    
                    $data['estado']=1;
                    $data['email']=$frmBolsaTrabajo->getValue('txtEmail');
                    $data['usuarioregistro']=(int)$this->_auth->id;
                    $data['fecharegistro']= date('Y-m-d H:i:s');                   
                    
                    $tableAvisos=new Application_Model_DbTable_Avisos();
                    
                    if($tableAvisos->addAvisos($data)){
                        $this->view->msgRegistro=1;
                         $this->_redirect('admin/bolsa-trabajo/listado');
                    }else{
                        $this->view->msgRegistro=0;
                    }
                }
        }
        
        $this->view->frmBolsaTrabajo=$frmBolsaTrabajo;
    }
    
    public function updateAction(){
        
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/backend/bolsa-trabajo.js');
        
        $id=$this->_request->getParam('id');
        
        $frmBolsaTrabajo=new Application_Form_BolsaTrabajo($id);              
              
		if ($this->getRequest()->isPost()) {                    
			if ($frmBolsaTrabajo->isValid($this->getRequest()->getPost())) {				
				$data['titulo']=$frmBolsaTrabajo->getValue('txtTitulo');
                                $data['descripcion']=$frmBolsaTrabajo->getValue('txtDescripcion');
                                $data['institucion']=$frmBolsaTrabajo->getValue('txtInstitucion');
                                $data['id_disciplinas']=$frmBolsaTrabajo->getValue('sltDisciplina');
                                $checkOcultar = $frmBolsaTrabajo->getValue('checkOcultar');
                                if($checkOcultar==0)
                                    $data['ocultar']=0;
                                else
                                    $data['ocultar']=1;
                                $data['departamento']=$frmBolsaTrabajo->getValue('sltDepartamento');                    
                                $data['estado']=1;
                                $data['email']=$frmBolsaTrabajo->getValue('txtEmail');
                                $data['usuariomodificasion']=(int)$this->_auth->id;
                                $data['fechamodificasion']= date('Y-m-d H:i:s');                    
                                $id=$frmBolsaTrabajo->getValue('id');
                                $tableAvisos=new Application_Model_DbTable_Avisos();
                                if($tableAvisos->updateAvisos($data,$id)){                        
                                    $this->redirect('admin/bolsa-trabajo/listado');
                                }else{
                                    $this->view->msgRegistro="No se pudo registrar, pruebe más tarde.";
                                }
			}else{//Si los datos del formulario, no son válidos, se muestra el formulario con los datos de nuevo.
			$this->redirect('admin/bolsa-trabajo/listado');	
                           // $frmBolsaTrabajo->populate($formData);
			}
		}else{//Mostramos los datos en caso de no haber enviado los datos al servidor para actualizar 
                    
                        $id = $this->_getParam('id', 0);
			if ($id > 0) {
                                $tableAvisos=new Application_Model_DbTable_Avisos();
                                $getAvisos=$tableAvisos->getAvisos($id);
                                $frmBolsaTrabajo->getElement('txtTitulo')->setValue($getAvisos['titulo']);
                                $frmBolsaTrabajo->getElement('txtInstitucion')->setValue($getAvisos['institucion']);
                                
                                if($getAvisos['ocultar']==1){
                                   $frmBolsaTrabajo->getElement('checkOcultar')->setValue(true); 
                                }else{
                                    $frmBolsaTrabajo->getElement('checkOcultar')->setValue(false);
                                }
                                
                                $frmBolsaTrabajo->getElement('sltDisciplina')->setValue($getAvisos['id_disciplinas']);
                                $frmBolsaTrabajo->getElement('txtDescripcion')->setValue($getAvisos['descripcion']);
                                $frmBolsaTrabajo->getElement('sltDepartamento')->setValue($getAvisos['departamento']);
                                
                                $frmBolsaTrabajo->getElement('txtEmail')->setValue($getAvisos['email']);          
                                $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/bolsa-trabajo/update';
                                
                        }
		}
                
                $this->view->frmBolsaTrabajo=$frmBolsaTrabajo;        
    }

    public function listadoAction(){
        $tableAvisos=new Application_Model_DbTable_Avisos();  
        
        $avisos = $tableAvisos->getListadoAvisos();
        //$this->view->listadoAvisos=$tableAvisos->getListadoAvisos();
        
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        //Zend_Debug::dump($post); exit();
        $paginator = Zend_Paginator::factory($avisos);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        
        $this->view->paginator = $paginator;
    }
    
     public function activarAction()
    {
        $id=$this->_request->getParam('id');
        $act=$this->_request->getParam('act');
        $tableAviso = new Application_Model_DbTable_Avisos();
        $tableAviso->activarAviso($id,$act);
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function borrarAction(){
        if ($this->getRequest()->isPost()) {
                $del = $this->getRequest()->getPost('del');
                if ($del == 'Si') {
                        $id = $this->getRequest()->getPost('id');
                        $tableAviso = new Application_Model_DbTable_Avisos();
                        $tableAviso->borrarAvisos($id);
                }
                $this->_redirect('admin/bolsa-trabajo/listado'); 
        }else{
                $id = $this->_getParam('id', 0);
                $tableAviso = new Application_Model_DbTable_Avisos();
                $this->view->aviso = $tableAviso->getAvisos($id);
        }	      
    }
    
}

