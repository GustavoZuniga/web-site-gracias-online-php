<?php

class Admin_EstudiantesController extends Zend_Controller_Action
{
    protected $_auth;
    
    public function init()
    {        
       $options=array('layout'=>'layout-admin');//cambiamos de layout
       Zend_Layout::startMvc($options);
       Zend_Layout::getMvcInstance()->assign('menuSelect', 2);
       $urlWeb = $this->getFrontController()->getBaseUrl();
       $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-noticia.js');
       
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $this->_auth= $auth->getIdentity()->backend;
                
    }
    
    public function indexAction() {

       $urlWeb = $this->getFrontController()->getBaseUrl();
       $this->view->headScript()->appendFile($urlWeb.'/static/admin/js/jquery.reveal.js');
       
	   $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);			
	   $ruta=$config->ruta->fotos->subir.'/profesores/';
		
        $table = new Application_Model_DbTable_Estudiante();
        
        $rows = $table->getAll();
        //var_dump($rows);exit();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        
        $paginator = Zend_Paginator::factory($rows);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->rutaFotos=$config->ruta->fotos->mostrar.'/profesores/';
        $this->view->paginator = $paginator;
    }

    public function nuevoAction(){
	
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
		
        $ruta=$config->ruta->fotos->subir.'/estudiantes/';
  
        $urlWeb = $this->getFrontController()->getBaseUrl();//para que reconosca en la nube
        $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-noticia.js');
        $this->view->headScript()->appendFile($urlWeb.'/static/admin/js/nicEdit.js');
        //Instanciamos el formulario
        $id=0;
        $frm = new Application_Form_Estudiante($id);
		
        $this->view->form = $frm;
                
        if($this->getRequest()->isPost()){
		
            if($frm->isValid($this->_request->getPost())){
                $data['pro_nombre'] = $frm->getValue('txtNombre');
                $data['pro_precio'] = $frm->getValue('txtPrecio');
                //$data['fecha_registro'] = date('Y-m-d H:i:s');                
                //$data['usuario_registro'] = (int)$this->_auth->id;
                
               $upload = new Zend_File_Transfer_Adapter_Http();
               $filename = $upload->getFileName('imagen',false);//noombre del archivo de texto
               
               $tableProducto=new Application_Model_DbTable_Producto();
			  
               $resp=$tableProducto->addRegistro($data);
               
               if(!empty($filename)){                        
                	$upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$filename);
                	$upload->addValidator('Size',true,array('min'=>'1KB','max'=>'5120KB'),$filename);
                	$upload->setDestination($ruta);
                	
                        if ($upload->isValid()) {
                                 $file_array = explode('.', $filename);
                                 $new_filename = 'pro_'.$resp.'.'. $file_array[1];
                                 $upload->addFilter('Rename', $new_filename);
                                 $upload->receive();
                                 
                                 $tableImg = new Application_Model_DbTable_ImagenEntidad();
								 
								 $dataImagen['img_entidad'] = 'PRODUCTO';
								 $dataImagen['img_entidad_id'] = $resp;
								 $dataImagen['img_extension'] = $file_array[1];
								 //$dataImagen['img_estado'] = //POR DEFAULT LA BBD DATOS LO CREA EN 0
								 $dataImagen['img_nombre'] = $new_filename;
								 $dataImagen['img_ruta'] = $ruta;

                                 $tableImg->insertImagen($dataImagen);
                        }                
                }
                                
                if((int)$resp>0){
                    $this->view->respRegistro=1;
                    $this->_redirect('admin/productos');
                }else{
                    $this->view->respRegistro=0;
                }
                
            }
        }
    }
	
	public function activarAction(){
		$data['id']=$this->_request->getParam('id');// id de producto
		$data['estado'] = $this->_request->getParam('act');// 1 o 0
		$table = new Application_Model_DbTable_Estudiante();
		$table->cambiarEstado($data);
		$this->_redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function editarAction(){
            
		$urlWeb = $this->getFrontController()->getBaseUrl();
		//$this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-noticia.js');
		//$this->view->headScript()->appendFile($urlWeb.'/static/admin/js/nicEdit2.js');
            
	    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);			
	    $ruta=$config->ruta->fotos->subir.'/productos/';
           
		$this->view->title = "Editar";
		$this->view->headTitle($this->view->title, 'PREPEND');
				   
        $id=$this->_request->getParam('id');
				
		$frmProducto = new Application_Form_Producto($id);
               
		if ($this->getRequest()->isPost()) {
			if ($frmProducto->isValid($this->getRequest()->getPost())) {
				$data['pro_id'] = $this->getRequest()->getPost('hdnId');
				$data['pro_nombre'] = $frmProducto->getValue('txtNombre');
				$data['pro_precio'] = $frmProducto->getValue('txtPrecio');
                //$data['usuario_modificacion'] = (int)$this->_auth->id;
				//$data['fecha_modificacion'] =date('Y-m-d H:i:s');
                                 
                                $upload = new Zend_File_Transfer_Adapter_Http();
                                $filename = $upload->getFileName('imagen',false);
                                 
                                if(!empty($filename)){ 
                                         
                                        $upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$filename);
                                        $upload->addValidator('Size',true,array('min'=>'1KB','max'=>'5120KB'),$filename);
                                        $upload->setDestination($ruta);
										
                                        if ($upload->isValid()) {
										
                                                $file_array = explode('.', $filename);
                                                 $new_filename = 'pro_'.$data['pro_id'].'.'. $file_array[1];
                                                 $upload->addFilter('Rename', $new_filename);
                                                 $upload->receive();

                                                 $tableImagen = new Application_Model_DbTable_ImagenEntidad();
                                                 $dataImagen['img_entidad'] = 'PRODUCTO';
												 $dataImagen['img_entidad_id'] = $data['pro_id'];
												 $dataImagen['img_extension'] = $file_array[1];
												 //$dataImagen['img_estado'] = //POR DEFAULT LA BBD DATOS LO CREA EN 0
												 $dataImagen['img_nombre'] = $new_filename;
												 $dataImagen['img_ruta'] = $ruta;
                                                 
                                                 $tableImagen->updateImagen($dataImagen);
												 
                                        }                                
                                }
                                                                
                                $tableProducto = new Application_Model_DbTable_Producto();
                                
                                if($tableProducto->updateRegistro($data)){
                                    //$this->view->respRegistro=1
                                    return $this->_redirect('admin/productos');
                                }else{
                                    $this->view->respRegistro=0;
                                }
                        
                        }
		}else{
			 
			$id = $this->_getParam('id', 0);
			
			if ($id > 0) {
				 
					$table = new Application_Model_DbTable_Producto();
					$getDatos = $table->getProductoById($id);
					$frmProducto->getElement('hdnId')->setValue($getDatos['pro_id']);
					$frmProducto->getElement('txtNombre')->setValue($getDatos['pro_nombre']);
					$frmProducto->getElement('txtPrecio')->setValue(number_format($getDatos['pro_precio'],2));
					if(empty($getDatos['img_nombre'])){
						$this->view->rutaFotos=$config->ruta->fotos->mostrar.'/productos/no-disponible.png'; 
					}else{
						$this->view->rutaFotos=$config->ruta->fotos->mostrar.'/productos/'.$getDatos['img_nombre'];
					}
					 
					//$this->view->imagen = $getDatos['nombre'];
					$this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/productos/editar/'.$getDatos['pro_id'];
					
					$this->view->formNoticia = $frmProducto;
			}
		}
                
                
	}
	
	public function borrarAction(){
		echo "en implementacion"; exit();
		/*if ($this->getRequest()->isPost()) {
			$del = $this->getRequest()->getPost('del');
			if ($del == 'Si') {
				$id = $this->getRequest()->getPost('id');
                                $tableNoticia = new Application_Model_DbTable_Noticia();
				$tableNoticia->deleteNoticia($id);
			}
			$this->_redirect('admin/noticias'); 
		}else{
			$id = $this->_getParam('id', 0);
			$tableNoticia = new Application_Model_DbTable_Noticia();
			$this->view->noticia = $tableNoticia->getNoticia($id);
		}*/		
	}
        
        
		
		
	public function listadoAction(){
        /*$tableNoticia = new Application_Model_DbTable_Noticia();
        $noticias = $tableNoticia->fetchAll();
        
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        //Zend_Debug::dump($post); exit();
        $paginator = Zend_Paginator::factory($noticias);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        
        $this->view->paginator = $paginator;*/
    }


}

