<?php

class Admin_EquiposController extends Zend_Controller_Action
{
    PROTECTED $_idUsuario;
    PROTECTED $_Auth;
    
    public function init()
    {      
        $options=array('layout'=>'layout-admin');//cambiamos de layout
        Zend_Layout::startMvc($options);
        Zend_Layout::getMvcInstance()->assign('menuSelect', 5);        
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $this->_auth= $auth->getIdentity()->backend;        
        $this->_idUsuario=$this->_auth->id;        
    }

    function agregarFotosVideos($form,$resp){            
            //------------------------------------------------------------------
            //FOTOS
            //------------------------------------------------------------------
           $imagenes=array();

           $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);                           
           $ruta=$config->ruta->fotos->subir.'/equipos/';

            $dataFotos['id_equipos']=$resp;
            $dataFotos['ruta']=$ruta;
            $dataFotos['estado']=1;
            $dataFotos['usuarioregistro']=1;
            $dataFotos['fecharegistro']=date('Y-m-d');
            //------------------------------------------------------------------
                            $upload = new Zend_File_Transfer_Adapter_Http();

                            foreach($upload->getFileInfo() as $file => $info): 
                                //-------------------------------------------------------------------
                                $upload->addValidator('Extension',true,array('jpeg','jpg','gif','png'),$file);
                                $upload->addValidator('Size',true,array('min'=>'2KB','max'=>'1024KB'),$file);
                                //-------------------------------------------------------------------
                                if ($upload->isValid($file)): 
                                        $file_array = explode('.', $info['name']);
                                        $new_filename = rand().'.'. $file_array[1];

                                        $dataRename['source']=$info['tmp_name'];
                                        $dataRename['target']=$ruta.$new_filename;
                                        $dataRename['overwrite']=true;

                                        $upload->addFilter('Rename',$dataRename);
                                        $upload->receive($file);

                                        $imagenes[]=$new_filename;
                                endif;
                            endforeach;        				                            
            //------------------------------------------------------------------
            $countImagenes=count($imagenes);
            if($countImagenes >0):
                    $tableEquiposFotos=new Application_Model_DbTable_EquiposFotos();

                    for($i=0;$i<$countImagenes;$i++):
                        $dataFotos['nombre']=$imagenes[$i];
                        $tableEquiposFotos->addFotos($dataFotos);                        
                    endfor;
                    
                    //grabar foto de inicio para que aparesca en el frontend
                    //-------------------------------------------------------------------------
                    $data['foto_inicio']=$imagenes[((int)$countImagenes-1)];//ultima imagen
                    
                    $tableEquipos=new Application_Model_DbTable_Equipos();
                    $respFotoInicio=$tableEquipos->updateEquipos($data,$resp);
            endif;
            
            //------------------------------------------------------------------
            //VIDEOS(solo se colocaran videos de youtube)
            //------------------------------------------------------------------
            $videos=trim($form->getValue('getVideos'));
            if(!empty($videos)):
                   $getVideos=explode('/////',$videos);
                   $countVideos=count($getVideos);

                   if($countVideos>0):
                          $dataVideos['id_equipos']=$resp;                                                
                          $dataVideos['estado']=1;
                          $dataVideos['usuarioregistro']=1;
                          $dataVideos['fecharegistro']=date('Y-m-d');
                         $tableEquiposVideos=new Application_Model_DbTable_EquiposVideos();

                         for($i=0;$i<$countVideos;$i++):
                             $dataVideos['nombre']=$getVideos[$i];
                             $tableEquiposVideos->addVideos($dataVideos);                             
                         endfor;
                   endif;
            endif;
                        
    }
    
    function eliminarFotosVideos($form){
        
        $fotosEliminados=trim($form->getValue('getFotosEliminados'));
        if(strlen($fotosEliminados)>0){
            $arrayIdsFotos=explode("/////",$fotosEliminados);
            $tableEquiposFotos=new Application_Model_DbTable_EquiposFotos();            
            $nombresFotos=$tableEquiposFotos->getGrupoFotos($arrayIdsFotos);
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
            $ruta=$config->ruta->fotos->subir.'/equipos/';
            
            //eliminamos fisicamente
            foreach ($nombresFotos as $row):
            		unlink($ruta.$row['nombre']);
            endforeach;
            
            $tableEquiposFotos->deleteFotos($arrayIdsFotos);            
        }
        
        $getVideosEliminados=trim($form->getValue('getVideosEliminados'));
        if(strlen($getVideosEliminados)>0){            
            $arrayIdsVideos=explode("/////",$getVideosEliminados);
            $tableEquiposVideos=new Application_Model_DbTable_EquiposVideos();
            $tableEquiposVideos->deleteVideos($arrayIdsVideos);            
        }
    }
    
    function registrarEquipos($form,$estado='add'){                           
                            $data['nombre']=$form->getValue('txtNombre');
                            $data['deporte']=$form->getValue('txtDeporte');
                            $data['id_disciplinas']=$form->getValue('sltDisciplina');
                            $data['direccion_postal']=$form->getValue('txtDireccionpostal');

                            $data['telefono']=$form->getValue('txtTelefono');                            
                            $data['email']=$form->getValue('txtEmail');

                            $departamento=trim($form->getValue('departamento'));
                            $provincia=trim($form->getValue('provincia'));
                            $distrito=trim($form->getValue('distrito'));
                                                       
                            $data['estado']=1;
                            
                           try{
                                        $tableUbigeo=new Application_Model_DbTable_Ubigeo();
                                        $getData=$tableUbigeo->getUbicasion($departamento, $provincia, $distrito);
                                        $data['ubigeo']=$getData['id'];

                                        $tableEquipos=new Application_Model_DbTable_Equipos();
                                        
                                        //------------------------------------------------------------------
                                        if($estado=='add'){
                                            $data['fecharegistro']= date('Y-m-d H:i:s');
                                            $data['usuarioregistro']=1;
                                            $resp=$tableEquipos->addEquipo($data);
                                            
                                            //Agregamos equipos a un evento
                                            if((int)$form->getValue('eventos')>0):                                                
                                                $dataEventosEquipo['id_eventos']=trim($form->getValue('eventos'));
                                                $dataEventosEquipo['id_equipos']=$resp;
                                                $dataEventosEquipo['estado']=1;
                                                $dataEventosEquipo['usuarioregistro']=(int)$this->_idUsuario;
                                                $dataEventosEquipo['fecharegistro']=date('Y-m-d H:i:s');
                                                
                                                $getEventosEquipos=new Application_Model_DbTable_EventosEquipos();
                                                $getEventosEquipos->addEquipos($dataEventosEquipo);
                                            endif;
                                        }else{//update
                                            $data['fechamodificacion']=date('Y-m-d H:i:s');
                                            $data['usuariomodificacion']=(int)$this->_idUsuario;
                                            $id=$form->getValue('id');
                                            $resp=$tableEquipos->updateEquipos($data, $id);
                                            
                                            if((int)$resp>0):
                                            	$resp=$id;
                                            else:
                                            	$resp=0;
                                            endif;
                                            
                                            if($estado=='update'){
                                            	$this->eliminarFotosVideos($form);
                                            	$resp=$id;
                                                
                                                //Editamos equipos a un evento
                                                if((int)$form->getValue('eventos')>0):                                                
                                                    $dataEventosEquipo['id_eventos']=trim($form->getValue('eventos'));
                                                    $dataEventosEquipo['id_equipos']=$resp;
                                                    $dataEventosEquipo['estado']=1;
                                                    $dataEventosEquipo['usuariomodificacion']=$this->_idUsuario;
                                                    $dataEventosEquipo['fechamodificacion']=date('Y-m-d H:i:s');
                                                    
                                                    $idEveEqui=trim($form->getValue('idEveEqui'));
                                                    $getEventosEquipos=new Application_Model_DbTable_EventosEquipos();
                                                    $respUpdate=$getEventosEquipos->updateEquipos($dataEventosEquipo,$idEveEqui);
                                                endif;
                                            }
                                        }
                                        //------------------------------------------------------------------
                                        
                                        //
                                        if((int)$resp > 0):                                                        
                                                        $this->agregarFotosVideos($form,$resp);
                                                        $this->_redirect('admin/equipos/index');                                                         
                                        endif;                 
                                         
                           } catch (Zend_Db_Exception $e) {
                                    $this->getMessenger()->error('Error al Actulizar.');
                                     echo $e->getMessage();
                           } catch (Zend_Exception $e) {
                                    $this->getMessenger()->error('error desconocido');
                                     echo $e->getMessage();
                           }
    }
    
    
    public function indexAction()
    {        
        $tableEquipos=new Application_Model_DbTable_Equipos();          
        $equipos = $tableEquipos->getListaEquipos();
        
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        
        $paginator = Zend_Paginator::factory($equipos);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        
        $this->view->paginator = $paginator;
    }
    
    public function nuevoAction(){
            $urlWeb = $this->getFrontController()->getBaseUrl();
            $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/videos-fotos.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-equipo.js');

            $tableUbigeo=new Application_Model_DbTable_Ubigeo();
            $myUbigeo=new My_Ubigeo();

            $frmEquipo = new Application_Form_Equipos();

            if($this->getRequest()->isPost()):
                    $request=$this->_request->getPost();

                    $ubigeo['departamento']=$request['departamento'];
                    $ubigeo['provincia']=$request['provincia'];
                    $ubigeo['distrito']=$request['distrito'];

                    $myUbigeo->setUbigeo($frmEquipo,$tableUbigeo,$ubigeo);

                    if($frmEquipo->isValid($this->_request->getPost())):
                            $this->registrarEquipos($frmEquipo,'add');                                                      
                    endif;

            else:
                    $ubigeo['departamento']=$tableUbigeo::DEPARTAMENTO_INICIAL;
                    $ubigeo['provincia']=$tableUbigeo::PROVINCIA_INICIAL;
                    $ubigeo['distrito']=$tableUbigeo::DISTRITO_INICIAL;

                    $myUbigeo->setUbigeo($frmEquipo,$tableUbigeo,$ubigeo);
            endif;
            $this->view->frmEquipoDeportivo = $frmEquipo;
    }
    
    public function updateAction(){
            
            $urlWeb = $this->getFrontController()->getBaseUrl();
            $this->view->headScript()->appendFile($urlWeb.'/static/js/ubigeo.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/videos-fotos.js');
            $this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-equipo.js');
            
            $tableEquipos=new Application_Model_DbTable_Equipos();
            $tableUbigeo=new Application_Model_DbTable_Ubigeo();
            $myUbigeo=new My_Ubigeo();
            
            $id=$this->getParam('id',0);
            $frmEquipo = new Application_Form_Equipos($id);
            
                  
            
            if($this->getRequest()->isPost()):
                    $request=$this->_request->getPost();

                    $ubigeo['departamento']=$request['departamento'];
                    $ubigeo['provincia']=$request['provincia'];
                    $ubigeo['distrito']=$request['distrito'];

                    $myUbigeo->setUbigeo($frmEquipo,$tableUbigeo,$ubigeo);

                    if($frmEquipo->isValid($this->_request->getPost())):                           
                            $this->registrarEquipos($frmEquipo,'update');                                                      
                    endif;

            else:
                
                    if((int)$id==0){
                            $this->redirect('admin/login/index');
                    }
                    
                    $getEquipo=$tableEquipos->getEquipo($id);
                    
                    $frmEquipo->getElement('txtNombre')->setValue($getEquipo['nombre']);
                    $frmEquipo->getElement('txtDeporte')->setValue($getEquipo['deporte']);
                    $frmEquipo->getElement('sltDisciplina')->setValue($getEquipo['id_disciplina']);
                    $frmEquipo->getElement('txtDireccionpostal')->setValue($getEquipo['direccion_postal']);
                    $frmEquipo->getElement('txtTelefono')->setValue($getEquipo['telefono']);
                    $frmEquipo->getElement('txtEmail')->setValue($getEquipo['email']);
                    
                    $ubigeo['departamento']=$getEquipo['departamento'];
                    $ubigeo['provincia']=$getEquipo['provincia'];
                    $ubigeo['distrito']=$getEquipo['distrito'];
                    
                    $myUbigeo->setUbigeo($frmEquipo,$tableUbigeo,$ubigeo);
                    
                    $tableEquiposFotos=new Application_Model_DbTable_EquiposFotos();            
                    $this->view->fotos=$tableEquiposFotos->getFotos($id);

                    $tableEquiposVideos=new Application_Model_DbTable_EquiposVideos();
                    $this->view->videos=$tableEquiposVideos->getVideos($id);
                    
                    //-------------------------------------------------------------------
                    $tableEventosEquipos=new Application_Model_DbTable_EventosEquipos();
                    $getEventosEquipos=$tableEventosEquipos->getParticipa($id);
                                        
                    if(count($getEventosEquipos)==1):
                        $frmEquipo->getElement('eventos')->setValue($getEventosEquipos[0]['id_eventos']);
                        $frmEquipo->getElement('idEveEqui')->setValue($getEventosEquipos[0]['id']);
                    endif;
                    //------------------------------------------------------------------- 
                    
                    $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/equipos/update';
            endif;
            
            $this->view->frmEquipoDeportivo = $frmEquipo;
    }
    
    
    public function borrarAction(){
		
		if ($this->getRequest()->isPost()) {
			$del = $this->getRequest()->getPost('del');
			if ($del == 'Si') {
				$id = $this->getRequest()->getPost('id');
                                $tableEquipo = new Application_Model_DbTable_Equipos();
				$tableEquipo->deleteEquipo($id);
			}
			$this->_redirect('admin/equipos'); 
		}else{
			$id = $this->_getParam('id', 0);
			$tableEquipo = new Application_Model_DbTable_Equipos();
			$this->view->equipo = $tableEquipo->getDetalleEquipo($id);
		}		
	}
    
    public function destacarAction(){
            $id=$this->_request->getParam('id');
            $act=$this->_request->getParam('act');
            $tableUsuarioAdmin = new Application_Model_DbTable_Equipos();
            $tableUsuarioAdmin->destacarEquipo($id,$act);
            $this->_redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function asignarequipoAction(){
        
        $id=$this->_request->getParam('id',0);
        $existe=$this->_request->getParam('existe',0);
        
        if((int)$id==0):
            $this->_redirect('admin/login');
        endif;
        
        $frmAsignarEquipo=new Application_Form_AsignarEquipo($id);
        $this->view->frmAsignarEquipo=$frmAsignarEquipo;
        
        $tableEventosEquipos=new Application_Model_DbTable_EventosEquipos();
        $this->view->participa=$tableEventosEquipos->getParticipa($id);
        
        $this->view->existe=$existe;
    }
    
    public function addasignarequipoAction(){//redirect
        $frmAsignarEquipo=new Application_Form_AsignarEquipo();
        
         if( $this->getRequest()->isPost()):
                
                if($frmAsignarEquipo->isValid($this->_request->getPost())):
                    
                    $data['id_eventos'] = $frmAsignarEquipo->getValue('eventos');
                    $data['id_equipos'] = $frmAsignarEquipo->getValue('equipos');
                    $data['usuarioregistro'] = 1;
                    $data['fecharegistro'] = date('Y-m-d H:i:s');
                    
                    $tableEventosEquipos=new Application_Model_DbTable_EventosEquipos();
                    $data['estado'] = $tableEventosEquipos::ACTIVO;
                    
                    $respExiste=$tableEventosEquipos->existe($data['id_equipos'],$data['id_eventos']);
                    
                    $existe=$tableEventosEquipos::INACTIVO;
                    
                    if((int)$respExiste['count'] > 0):
                           $existe=$tableEventosEquipos::ACTIVO;
                           $this->_redirect('/admin/equipos/asignarequipo/id/'.$data['id_equipos'].'/existe/'.$existe);
                    else:   
                        $resp=$tableEventosEquipos->addEquipos($data);
                            
                        if((int)$resp>0):
                            $this->_redirect('/admin/equipos/asignarequipo/id/'.$data['id_equipos']);
                        endif;
                    endif;
                    
                endif;
                
         endif;
    }
    
    public function deleteasignarequiposAction(){//redirect
       
         $id=$this->_request->getParam('id',0);
         $idEquipo=$this->_request->getParam('equipo',0);
         
         if((int)$id==0):
            $this->_redirect('admin/login');
        endif;
        
        $tableEventosEquipos=new Application_Model_DbTable_EventosEquipos();
        $tableEventosEquipos->deleteEquipos($id);
        
        $this->_redirect('/admin/equipos/asignarequipo/id/'.(int)$idEquipo);
        
    }
    
}

