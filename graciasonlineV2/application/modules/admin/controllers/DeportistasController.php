<?php

class Admin_DeportistasController extends Zend_Controller_Action
{
    protected $_auth;
    
    public function init()
    {      
        $options=array('layout'=>'layout-admin');//cambiamos de layout
        Zend_Layout::startMvc($options);
        Zend_Layout::getMvcInstance()->assign('menuSelect', 6);
        
        $myAuth=new My_Auth('backend');
        $auth = Zend_Auth::getInstance();        
        $myAuth->authExiste($auth);
              
        if(!isset($auth->getIdentity()->backend)):
            $this->_redirect('admin/login');
        endif;
        
        $this->_auth= $auth->getIdentity()->backend;
    }

    public function indexAction()
    {
        
        $tableDeportista=new Application_Model_DbTable_Deportista();  
        
        $deportistas = $tableDeportista->getListaDeportistas();
              
        //$this->view->listadoAvisos=$tableAvisos->getListadoAvisos();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/items.phtml');
        //Zend_Debug::dump($post); exit();
        $paginator = Zend_Paginator::factory($deportistas);
        if($this->_hasParam('page')){
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        
        $this->view->paginator = $paginator;
        
    }
    
    public function nuevoAction(){
        $urlWeb = $this->getFrontController()->getBaseUrl();
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-deportista.js');
        
        $frmDeportista = new Application_Form_RegistroDeportista();        
        
        if( $this->getRequest()->isPost() ){    		
                if($frmDeportista->isValid($this->_request->getPost())){
                    $data['nombres'] = $frmDeportista->getValue('txtNombres');
                    $data['apellidos'] = $frmDeportista->getValue('txtApellidos');
                    $data['dni'] = $frmDeportista->getValue('txtDni');
                    $data['departamento'] = $frmDeportista->getValue('sltDepartamento');
                    $data['email'] = $frmDeportista->getValue('txtEmail');
                    $data['estado'] = 1;
                    $data['fecharegistro'] = date('Y-m-d H:i:s');
                    $data['usuarioregistro'] = (int)$this->_auth->id;
                    $data['equipo_id'] = $frmDeportista->getValue('sltEquipo');
                    
                    $tableDeportista = new Application_Model_DbTable_Deportista();
                    
                    if($tableDeportista->addDeportista($data)){
                        $this->view->msgRegistro=1;
                         $this->_redirect('admin/deportistas');
                    }else{
                        $this->view->msgRegistro=0;
                    }
                }
        }
        
        $this->view->frmDeportista = $frmDeportista;
    }
    
    public function editarAction(){
            
        $urlWeb = $this->getFrontController()->getBaseUrl();
    	$this->view->headScript()->appendFile($urlWeb.'/static/js/backend/registro-deportista.js');
         
        $this->view->title = "Editar Deportista";
        $this->view->headTitle($this->view->title, 'Actualizar datos deportista');
		
        $id=$this->_request->getParam('id');
        $frmDeportista = new Application_Form_RegistroDeportista($id);
                
		if ($this->getRequest()->isPost()) {
			if ($frmDeportista->isValid($this->getRequest()->getPost())) {
				
				$data['id'] = $frmDeportista->getValue('id');
                                
				$data['nombres'] = $frmDeportista->getValue('txtNombres');
                                $data['apellidos'] = $frmDeportista->getValue('txtApellidos');
                                $data['dni'] = $frmDeportista->getValue('txtDni');
                                $data['departamento'] = $frmDeportista->getValue('sltDepartamento');
                                $data['email'] = $frmDeportista->getValue('txtEmail');
                                $data['estado'] = 1;
                                $data['fechaactualizacion'] = date('Y-m-d H:i:s');
                                $data['usuarioregistro'] = (int)$this->_auth->id;
                                $data['equipo_id'] = $frmDeportista->getValue('sltEquipo');
				
				$tableDeportista = new Application_Model_DbTable_Deportista();
                                if($tableDeportista->updateDeportista($data)){
                                    //$this->view->respRegistro=1
                                    return $this->_redirect('admin/deportistas');
                                }else{
                                    $this->view->respRegistro=0;
                                }
				
			}
		}else{
			
                        $id = $this->_getParam('id', 0);
			if ($id > 0) {
                                $tableDeportista = new Application_Model_DbTable_Deportista();
                                $getDeportista=$tableDeportista->getDeportista($id);
                                
                                $frmDeportista->getElement('txtNombres')->setValue($getDeportista['nombres']);
                                $frmDeportista->getElement('txtApellidos')->setValue($getDeportista['apellidos']);
                                $frmDeportista->getElement('txtDni')->setValue($getDeportista['dni']);
                                $frmDeportista->getElement('sltDepartamento')->setValue($getDeportista['departamento']);
                                $frmDeportista->getElement('txtEmail')->setValue($getDeportista['email']);
                                $frmDeportista->getElement('sltEquipo')->setValue($getDeportista['equipo_id']);
                                $this->view->action=Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/deportistas/editar';
                                
                        }
		}
                
                $this->view->frmDeportista = $frmDeportista;
    }
    
    public function borrarAction(){
            if ($this->getRequest()->isPost()) {
                    $del = $this->getRequest()->getPost('del');
                    if ($del == 'Si') {
                            $id = $this->getRequest()->getPost('id');
                            $tableDeportista = new Application_Model_DbTable_Deportista();
                            $tableDeportista->deleteDeportista($id);
                    }
                    $this->_redirect('admin/deportistas'); 
            }else{
                    $id = $this->_getParam('id', 0);
                    $tableDeportista = new Application_Model_DbTable_Deportista();
                    $this->view->deportista = $tableDeportista->getDetalleDeportista($id);
            }		
    }
    /*public function borrarAction(){
        $id=$this->_request->getParam('id');
        $tableAvisos=new Application_Model_DbTable_Deportista();
        if($tableAvisos->deleteDeportista($id)){
            $this->redirect('admin/deportistas/index');            
        }
        
        $this->view->msgAvisos="No se pudo eliminar aviso del codigo=".$id;        
    }*/
    
}

