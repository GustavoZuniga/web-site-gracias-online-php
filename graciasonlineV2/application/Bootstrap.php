<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    
    protected function _initSessions() {
        //$this->bootstrap('session');
        //Zend_Session::start();
    }
    
    protected function _initDatabases(){ 
        $parametersCms = array(
                    'host'     => 'localhost',
                    'username' => 'root',
                    'password' => '',
                    'dbname'   => 'bsctg_cms'
                   ); 
        try {
            $dbCms = Zend_Db::factory('Pdo_Mysql', $parametersCms);
            $dbCms->getConnection();
			
        } catch (Zend_Db_Adapter_Exception $e) {
            echo $e->getMessage();
            die('Could not connect to database.');
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
            die('Could not connect to database.');
        }
        Zend_Registry::set('db', $dbCms);

        /*$parametersMoodle = array(
                            'host'     => 'localhost',
                            'username' => 'root',
                            'password' => '',
                            'dbname'   => 'bsctg_cms'
                           );
        try {
            $dbMoodle = Zend_Db::factory('Pdo_Mysql', $parametersMoodle);
            $dbMoodle->getConnection();
        } catch (Zend_Db_Adapter_Exception $e) {
            echo $e->getMessage();
            die('Could not connect to database.');
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
            die('Could not connect to database.');
        }
        Zend_Registry::set('dbmoodle', $dbMoodle);*/
		
    }
    
    public function init()
    {
         $this->db = Zend_Registry::get('db');
         //$this->dbmoodle = Zend_Registry::get('dbmoodle');
    }

}

