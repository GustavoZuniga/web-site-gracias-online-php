<?php

class Application_Form_Postular extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmPostular');//nombre del form
        
        if(isset($option)){
            $id = new Zend_Form_Element_Hidden('id');    	
            $id->setValue($option);
            $this->addElement($id);
        }
        
        $nombres=new Zend_Form_Element_Text('txtNombresCompletoPo');
		$nombres->setRequired(true);
		$nombres->setAttrib('placeholder','nombre completo');
        $this->addElement($nombres);
        
        $email=new Zend_Form_Element_Text('txtEmailPo');
                $email->addValidator(new Zend_Validate_EmailAddress(), true);
                $email->addValidator(new Zend_Validate_NotEmpty(), true);
		$email->setRequired(true);
		$email->setAttrib('placeholder','e-email');
        $this->addElement($email);
               
        $ciudad=new Zend_Form_Element_Text('txtCiudadPo');
		$ciudad->setRequired(true);
		$ciudad->setAttrib('placeholder','ciudad');
         $this->addElement($ciudad);       
               
        
        $pais=new Zend_Form_Element_Select('pais');        
                $pais->addFilter('StripTags');
                $pais->addFilter('StringTrim');
                $pais->setRequired(true);
        
        $pais->addMultiOption('', 'Pais');
    	$pais->addMultiOption('peru', 'Peru');
        
        $this->addElement($pais);
        
        $comentario = new Zend_Form_Element_Textarea('textComentarioPo');		
		                $comentario->addFilter('StripTags');
		                $comentario->addFilter('StringTrim');
		                $comentario->setRequired(false);
				$comentario->setAttrib('placeholder','comentario');
		        $this->addElement($comentario);
        
    	$submit=new Zend_Form_Element_Submit('btnPostular');
    	$submit->setLabel('Postular');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/login');
    }
    
}

