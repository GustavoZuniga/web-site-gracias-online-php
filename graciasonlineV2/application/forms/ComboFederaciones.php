<?php
/**
 * Description of Contactos
 *
 * @author Zuñiga
 */
class Application_Form_ComboFederaciones extends Zend_Form
{  
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmComboFederaciones');//nombre del form
        $id=0;
        if(isset($option)){
           $id = new Zend_Form_Element_Hidden('id');            
           $id->setValue($option);
        }
        
        $federaciones=new Zend_Form_Element_Select('federaciones');
        $federaciones->addFilter('StripTags');
        $federaciones->addFilter('StringTrim');
                $federaciones->setRequired(false);
                $federaciones->setAttrib('placeholder','federaciones');                
        //$this->addElement($federaciones);
        //-----------------------------------------------------------------------------------------------------------------
            $tableFederaciones=new Application_Model_DbTable_Federaciones();
            
            $getFederaciones=$tableFederaciones->getListaFederaciones();
            $federaciones->addMultiOption(0,'Elija una federación' );
            
            foreach ($getFederaciones as $row):
                $federaciones->addMultiOption($row['id'], $row['nombre']);
            endforeach;
            
            $this->addElement($federaciones);
        //-----------------------------------------------------------------------------------------------------------------
        

    			
    	$submit=new Zend_Form_Element_Submit('btnEnviarFederaciones');
    	$submit->setLabel('Guardar');    	
    	$submit->setAttrib('id', 'btnFederaciones');
        
    	$this->addElements(array($id,$submit));//agregando lo q se ha creado al form
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/federaciones/index');
    }
}


