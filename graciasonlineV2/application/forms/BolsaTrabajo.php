<?php
//autor=Dimas Gustavo <amadeusc2@gmail.com>
class Application_Form_BolsaTrabajo extends Zend_Form
{
    CONST DEPARTAMENTO_INICIAL='LIMA';
    CONST PROVINCIA_INICIAL='LIMA';
    CONST DISTRITO_INICIAL='LIMA';
    
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmBolsaTrabajo');//nombre del form
        
        if(isset($option)){
            $id = new Zend_Form_Element_Hidden('id');    	
            $id->setValue($option);
            $this->addElement($id);
        }
        
    	$titulo=new Zend_Form_Element_Text('txtTitulo');
		$titulo->setRequired(true);		
		$titulo->setAttrib('placeholder','titulo');
	$this->addElement($titulo);
        
        $institucion=new Zend_Form_Element_Text('txtInstitucion');
		$institucion->setRequired(true);
		$institucion->setAttrib('placeholder','institucion');
                $institucion->setAttrib('class', 'text');
        $this->addElement($institucion);
        
        $checkbox=new Zend_Form_Element_Checkbox("checkOcultar");    	
    	$checkbox->setChecked(false);
        
        $this->addElement($checkbox);
        
        $disciplina = new Zend_Form_Element_Select('sltDisciplina');
                $disciplina->addFilter('StripTags');
                $disciplina->addFilter('StringTrim');
		$disciplina->setRequired(true);
                $disciplina->setAttrib('class', 'text');
		$disciplina->setAttrib('placeholder','disciplina');        
                
                $tableDisciplinas=new Application_Model_DbTable_Disciplinas();
                $getDisciplinas=$tableDisciplinas->getDisciplinas();
                $disciplina->addMultiOption('seleccione', 'seleccione');
                foreach ($getDisciplinas as $row):
                    $disciplina->addMultiOption($row['id'], $row['nombre']);
                endforeach;
                $disciplina->setValue('seleccione');
        
        $this->addElement($disciplina);
        
        
        
        $descripcion=new Zend_Form_Element_Textarea('txtDescripcion');		
                $descripcion->addFilter('StripTags');
                $descripcion->addFilter('StringTrim');
                $descripcion->setOptions(array('cols' => '100', 'rows' => '10'));
                $descripcion->setRequired(true);
		$descripcion->setAttrib('placeholder','descripcion');
        $this->addElement($descripcion);
                
        $departamento=new Zend_Form_Element_Select('sltDepartamento');
                $departamento->addFilter('StripTags');
                $departamento->addFilter('StringTrim');
		$departamento->setRequired(true);
                $departamento->setAttrib('class', 'text');
		$departamento->setAttrib('placeholder','departamento');
                
                $tableUbigeo=new Application_Model_DbTable_Ubigeo();
                $getDepartamentos=$tableUbigeo->getDepartamentos();
                                
                foreach ($getDepartamentos as $rowDepa):
                        $departamento->addMultiOption($rowDepa['departamento'], $rowDepa['departamento']);
                endforeach;
                
                $departamento->setValue(self::DEPARTAMENTO_INICIAL);
                
        $this->addElement($departamento);
        
        /*
        $provincia=new Zend_Form_Element_Select('sltProvincia');
                $provincia->addFilter('StripTags');
                $provincia->addFilter('StringTrim');
		$provincia->setRequired(true);
                $provincia->setAttrib('class', 'text');
		$provincia->setAttrib('placeholder','provincia');        
                
                $getProvincias=$tableUbigeo->getProvincias(self::DEPARTAMENTO_INICIAL);
                
                foreach ($getProvincias as $rowProvincias):
                        $provincia->addMultiOption($rowProvincias['provincia'], $rowProvincias['provincia']);
                endforeach;
                
                $provincia->setValue(self::PROVINCIA_INICIAL);
                
        $this->addElement($provincia);
        
        $distrito=new Zend_Form_Element_Select('sltDistrito');
                $distrito->addFilter('StripTags');
                $distrito->addFilter('StringTrim');
		$distrito->setRequired(true);
                $provincia->setAttrib('class', 'text');
		$distrito->setAttrib('placeholder','distrito');        
                
                $getDistritos=$tableUbigeo->getDistritos(self::PROVINCIA_INICIAL);
                
                foreach ($getDistritos as $rowDistrito):
                        $distrito->addMultiOption($rowDistrito['distrito'], $rowDistrito['distrito']);
                endforeach;
                
                $distrito->setValue(self::DISTRITO_INICIAL);
                
        $this->addElement($distrito);*/
        
        $email=new Zend_Form_Element_Text('txtEmail');                
		$email->setRequired(true);
                $email->addValidator(new Zend_Validate_EmailAddress(), true);
                $email->addValidator(new Zend_Validate_NotEmpty(), true);
                $email->errMsg = "No parece ser un correo electrónico valido";
		$email->setAttrib('placeholder','email');
                $email->setAttrib('class', 'text');
                
        $this->addElement($email);
        
    	$submit=new Zend_Form_Element_Submit('btnRegistrarBolsaTrabajo');
    	$submit->setLabel('enviar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/login');
    }
    
    

}

