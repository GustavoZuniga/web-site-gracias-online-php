<?php

class Application_Form_BuscarNoticias extends Zend_Form
{
	   
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    public function __construct($option=null){
		    	parent::__construct($option);
		    	$this->setName('frmNoticiasBusqueda');//nombre del form
		        
		        $busqueda=new Zend_Form_Element_Text('txtBusqueda');
                                $busqueda->setRequired(true);		
                                $busqueda->setAttrib('placeholder','usuario');
                        $this->addElement($busqueda);
                            
		    	$submit=new Zend_Form_Element_Submit('btnBusqueda');
		    	$submit->setLabel('Buscar');
		    	$this->addElement($submit);
		    	
		    	$this->setMethod('post');//metodo post del form
		    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index');
    }
}

