<?php
//autor=Gustavo
class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmLogin');//nombre del form
    	 
    	$username=new Zend_Form_Element_Text('txtUsuario');
		$username->setRequired(true);		
		$username->setAttrib('required','required');
		$username->setAttrib('placeholder','usuario');
				
    	$password=new Zend_Form_Element_Password('txtPassword');    	
    		$password->setRequired(true);		
		$password->setAttrib('placeholder','contraseña');
    			
    	$submit=new Zend_Form_Element_Submit('btnIngresar');
                $submit->setLabel('Ingresar');    	
                $submit->setAttrib('id', 'btnIngresar');
    	
    	$this->addElements(array($username,$password,$submit));//agregando lo q se ha creado al form
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'usuarios/login/index');
    }
}

