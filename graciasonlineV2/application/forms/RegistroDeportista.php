<?php
class Application_Form_RegistroDeportista extends Zend_Form
{
    CONST DEPARTAMENTO_INICIAL='LIMA';
    CONST PROVINCIA_INICIAL='LIMA';
    CONST DISTRITO_INICIAL='LIMA';
    
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmDeportista');//nombre del form
        
        if(isset($option)){
            $id = new Zend_Form_Element_Hidden('id');    	
            $id->setValue($option);
            $this->addElement($id);
        }
        
    	$nombres = new Zend_Form_Element_Text('txtNombres');
		$nombres->setRequired(true);		
		$nombres->setAttrib('placeholder','nombres');
                $nombres->setAttrib('class', 'text');
	$this->addElement($nombres);
        
        $apellidos = new Zend_Form_Element_Text('txtApellidos');
		$apellidos->setRequired(true);
		$apellidos->setAttrib('placeholder','apellidos');
                $apellidos->setAttrib('class', 'text');
        $this->addElement($apellidos);
        
        $dni = new Zend_Form_Element_Text('txtDni');
		$dni->setRequired(true);
		$dni->setAttrib('placeholder','dni');
                //$dni->addValidator('Regex', false, array('/^[a-z0-9]{6,}$/i'));
                $dni->setAttrib('maxLength', 8);
                $dni->setAttrib('class', 'text');
        $this->addElement($dni);
        
        $departamento=new Zend_Form_Element_Select('sltDepartamento');
                $departamento->addFilter('StripTags');
                $departamento->addFilter('StringTrim');
		$departamento->setRequired(true);
                $departamento->setAttrib('class', 'text');
		$departamento->setAttrib('placeholder','departamento');
                
                $tableUbigeo=new Application_Model_DbTable_Ubigeo();
                $getDepartamentos=$tableUbigeo->getDepartamentos();
                                
                foreach ($getDepartamentos as $rowDepa):
                        $departamento->addMultiOption($rowDepa['departamento'], $rowDepa['departamento']);
                endforeach;
                
                $departamento->setValue(self::DEPARTAMENTO_INICIAL);
                
        $this->addElement($departamento);
        
        $equipo=new Zend_Form_Element_Select('sltEquipo');
                $equipo->addFilter('StripTags');
                $equipo->addFilter('StringTrim');
		$equipo->setRequired(false);
                $equipo->setAttrib('class', 'text');
		$equipo->setAttrib('placeholder','equipo');
                
                $tableEquipo = new Application_Model_DbTable_Equipos();
                $getEquipos=$tableEquipo->getComboEquipos();
                  $equipo->addMultiOption('0','--Seleccione--');              
                foreach ($getEquipos as $row):
                        
                        $equipo->addMultiOption($row['id'], $row['nombre']);
                endforeach;
        $this->addElement($equipo);
        /*
        $provincia=new Zend_Form_Element_Select('sltProvincia');
                $provincia->addFilter('StripTags');
                $provincia->addFilter('StringTrim');
		$provincia->setRequired(true);
                $provincia->setAttrib('class', 'text');
		$provincia->setAttrib('placeholder','provincia');        
                
                $getProvincias=$tableUbigeo->getProvincias(self::DEPARTAMENTO_INICIAL);
                
                foreach ($getProvincias as $rowProvincias):
                        $provincia->addMultiOption($rowProvincias['provincia'], $rowProvincias['provincia']);
                endforeach;
                
                $provincia->setValue(self::PROVINCIA_INICIAL);
                
        $this->addElement($provincia);
        
        $distrito=new Zend_Form_Element_Select('sltDistrito');
                $distrito->addFilter('StripTags');
                $distrito->addFilter('StringTrim');
		$distrito->setRequired(true);
                $distrito->setAttrib('class', 'text');
		$distrito->setAttrib('placeholder','distrito');        
                
                $getDistritos=$tableUbigeo->getDistritos(self::PROVINCIA_INICIAL);
                
                foreach ($getDistritos as $rowDistrito):
                        $distrito->addMultiOption($rowDistrito['distrito'], $rowDistrito['distrito']);
                endforeach;
                
                $distrito->setValue(self::DISTRITO_INICIAL);
                
        $this->addElement($distrito);*/
        
        $email = new Zend_Form_Element_Text('txtEmail');                
                    $email->setRequired(true);
                    $email->addValidator(new Zend_Validate_EmailAddress(), true);
                    $email->addValidator(new Zend_Validate_NotEmpty(), true);
                    $email->errMsg = "No parece ser un correo electrónico valido";
                    $email->setAttrib('placeholder','email');
                    $email->setAttrib('class', 'text');
        $this->addElement($email);
        
       
        
    	$submit=new Zend_Form_Element_Submit('btnDeportista');
    	$submit->setLabel('Registrar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/deportistas/nuevo');
    }
    
    

}

