<?php
//autor=Dimas Gustavo <amadeusc2@gmail.com>
class Application_Form_RegistroUsuario extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmRegistroUsuario');//nombre del form
        
        if(isset($option)){
            $id = new Zend_Form_Element_Hidden('id');    	
            $id->setValue($option);
            $this->addElement($id);
        }
        
    	$nombres = new Zend_Form_Element_Text('txtNombres');
                    $nombres->setRequired(true);		
                    $nombres->setAttrib('placeholder','nombres');
                    $nombres->setAttrib('class', 'text');
	$this->addElement($nombres);
        
        $apellidos = new Zend_Form_Element_Text('txtApellidos');
		$apellidos->setRequired(true);
		$apellidos->setAttrib('placeholder','apellidos');
                $apellidos->setAttrib('class', 'text');
        $this->addElement($apellidos);
        
        $username = new Zend_Form_Element_Text('txtUsername');
		$username->setRequired(true);
		$username->setAttrib('placeholder','username');
                $username->setAttrib('class', 'text');
        $this->addElement($username);
        
        $password=new Zend_Form_Element_Password('txtPassword');                
                $password->setRequired(true);                
                $password->errMsg = "No es un contraseña validad";
		$password->setAttrib('placeholder','contraseña');
                $password->setAttrib('autocomplete','off');
                
        $this->addElement($password);
               
        
        $email=new Zend_Form_Element_Text('txtEmail');                
		$email->setRequired(true);
                $email->addValidator(new Zend_Validate_EmailAddress(), true);
                $email->addValidator(new Zend_Validate_NotEmpty(), true);
                $email->errMsg = "No parece ser un correo electrónico valido";
		$email->setAttrib('placeholder','email');
                $email->setAttrib('class', 'text');
                
        $this->addElement($email);
        
    	$submit=new Zend_Form_Element_Submit('btnRegistroUsuario');
    	$submit->setLabel('enviar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/usuarios/nuevo');
    }
    
    

}

