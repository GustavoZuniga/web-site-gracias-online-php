<?php

class Application_Form_BuscarSedesDeportivas extends Zend_Form
{
	   
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    public function __construct($option=null){
		    	parent::__construct($option);
		    	$this->setName('frmBuscarSedesDeportivas');//nombre del form
		        
		        $departamento=new Zend_Form_Element_Select('departamento');
		        $departamento->addFilter('StripTags');
		        $departamento->addFilter('StringTrim');
				$departamento->setRequired(false);
				$departamento->setAttrib('placeholder','departamento');
                
		        $this->addElement($departamento);
		        
		        $provincia=new Zend_Form_Element_Select('provincia');
		        $provincia->addFilter('StripTags');
		        $provincia->addFilter('StringTrim');
				$provincia->setRequired(false);
				$provincia->setAttrib('placeholder','provincia');                
        		$this->addElement($provincia);
        
        		$distrito=new Zend_Form_Element_Select('distrito');
                        $distrito->addFilter('StripTags');
                        $distrito->addFilter('StringTrim');
				$distrito->setRequired(false);
				$distrito->setAttrib('placeholder','distrito');        
                
		        $this->addElement($distrito);
                            
		    	$submit=new Zend_Form_Element_Submit('btnBuscar');
		    	$submit->setLabel('Buscar');
		    	$this->addElement($submit);
		    	
		    	$this->setMethod('post');//metodo post del form
		    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/add');
    }
}

