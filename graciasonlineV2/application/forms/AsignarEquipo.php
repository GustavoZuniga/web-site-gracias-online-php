<?php
class Application_Form_AsignarEquipo extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmAsignarEquipo');//nombre del form
        
        $eventos = new Zend_Form_Element_Select('eventos');
        $eventos->addFilter('StripTags');
        $eventos->addFilter('StringTrim');
        $eventos->setRequired(false);
        $eventos->setAttrib('class', 'text');
        $eventos->setAttrib('placeholder','eventos');        

        $tableEventos=new Application_Model_DbTable_Eventos();
        $getEventos=$tableEventos->getEventosVigentes();
        
        $eventos->addMultiOption('','Elija un evento');
        foreach ($getEventos as $row):
            $eventos->addMultiOption($row['id'], $row['nombre_evento']);
        endforeach;
        
        $this->addElement($eventos);       
        
        $idEquipos=new Zend_Form_Element_Hidden('equipos');//id de equipo
        $idEquipos->setValue($option);
        $this->addElement($idEquipos);
        
        
    	$submit=new Zend_Form_Element_Submit('btnAgregar');
    	$submit->setLabel('Participar');        
        $submit->setAttrib('class', 'button add');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/equipos/nuevo');
    }
    
    

}

