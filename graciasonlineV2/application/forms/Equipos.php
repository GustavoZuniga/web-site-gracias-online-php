<?php
class Application_Form_Equipos extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmEquipo');//nombre del form
        
        /*
        $eventos = new Zend_Form_Element_Select('eventos');
        $eventos->addFilter('StripTags');
        $eventos->addFilter('StringTrim');
        $eventos->setRequired(false);
        $eventos->setAttrib('class', 'text');
        $eventos->setAttrib('placeholder','eventos');        

        $tableEventos=new Application_Model_DbTable_Eventos();
        $getEventos=$tableEventos->getEventosVigentes();
        
        $eventos->addMultiOption('','Elija un evento');
        foreach ($getEventos as $row):
            $eventos->addMultiOption($row['id'], $row['nombre_evento']);
        endforeach;

        $this->addElement($eventos);
        */
        
    	$nombre = new Zend_Form_Element_Text('txtNombre');
		$nombre->setRequired(true);		
		$nombre->setAttrib('placeholder','nombre');
                $nombre->setAttrib('class', 'text');
	$this->addElement($nombre);
        
        $deporte = new Zend_Form_Element_Text('txtDeporte');
		$deporte->setRequired(true);
		$deporte->setAttrib('placeholder','deporte');
                $deporte->setAttrib('class', 'text');
        $this->addElement($deporte);
        
        $disciplina = new Zend_Form_Element_Select('sltDisciplina');
                $disciplina->addFilter('StripTags');
                $disciplina->addFilter('StringTrim');
		$disciplina->setRequired(true);
                $disciplina->setAttrib('class', 'text');
		$disciplina->setAttrib('placeholder','disciplina');        
                
                $tableDisciplinas=new Application_Model_DbTable_Disciplinas();
                $getDisciplinas=$tableDisciplinas->getDisciplinas();
                
                foreach ($getDisciplinas as $row):
                    $disciplina->addMultiOption($row['id'], $row['nombre']);
                endforeach;
        
        $this->addElement($disciplina);
        
        $direccion = new Zend_Form_Element_Text('txtDireccionpostal');
		$direccion->setRequired(true);
		$direccion->setAttrib('placeholder','dirección');
                $direccion->setAttrib('class', 'text');
        $this->addElement($direccion);
        
        $departamento=new Zend_Form_Element_Select('departamento');
                $departamento->addFilter('StripTags');
                $departamento->addFilter('StringTrim');
		$departamento->setRequired(false);
                $departamento->setAttrib('class', 'text');
		$departamento->setAttrib('placeholder','departamento');
        $this->addElement($departamento);
        
        $provincia=new Zend_Form_Element_Select('provincia');
                $provincia->addFilter('StripTags');
                $provincia->addFilter('StringTrim');
		$provincia->setRequired(false);
                $provincia->setAttrib('class', 'text');
		$provincia->setAttrib('placeholder','provincia');
        $this->addElement($provincia);
        
        $distrito=new Zend_Form_Element_Select('distrito');
                $distrito->addFilter('StripTags');
                $distrito->addFilter('StringTrim');
		$distrito->setRequired(false);
                $distrito->setAttrib('class', 'text');
		$distrito->setAttrib('placeholder','distrito');        
                
        $this->addElement($distrito);
        
        $telefono = new Zend_Form_Element_Text('txtTelefono');
                    $telefono->setRequired(true);		
                    $telefono->setAttrib('placeholder','telefono');
                    $telefono->setAttrib('class', 'text');
	$this->addElement($telefono);
        
        $email = new Zend_Form_Element_Text('txtEmail');                
                    $email->setRequired(true);
                    $email->addValidator(new Zend_Validate_EmailAddress(), true);
                    $email->addValidator(new Zend_Validate_NotEmpty(), true);
                    $email->errMsg = "No parece ser un correo electrónico valido";
                    $email->setAttrib('placeholder','email');
                    $email->setAttrib('class', 'text');
        $this->addElement($email);
        
        $getFotos=new Zend_Form_Element_Hidden('getFotos');
        $this->addElement($getFotos);
        
        $getVideos=new Zend_Form_Element_Hidden('getVideos');
        $this->addElement($getVideos);
        
        //UPDATE
        if(isset($option)){
                $id = new Zend_Form_Element_Hidden('id');
                $id->setValue($option);
                $this->addElement($id);
                
                $idEventosEquipos = new Zend_Form_Element_Hidden('idEveEqui');
                $idEventosEquipos->setValue($option);
                $this->addElement($idEventosEquipos);

                $getFotosEliminados=new Zend_Form_Element_Hidden('getFotosEliminados');
                $this->addElement($getFotosEliminados);

                $fotoInicio=new Zend_Form_Element_Hidden('fotoInicio');
                $this->addElement($fotoInicio);
                
                $getVideosEliminados=new Zend_Form_Element_Hidden('getVideosEliminados');
                $this->addElement($getVideosEliminados);

                $getTraidos=new Zend_Form_Element_Hidden('getTraidos');//esto es para videos
                $this->addElement($getTraidos);
        }
        
    	$submit=new Zend_Form_Element_Submit('btnEquipo');
    	$submit->setLabel('enviar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/equipos/nuevo');
    }
    
    

}

