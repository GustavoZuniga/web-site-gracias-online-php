<?php

class Application_Form_Ayuda extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
	
	public function __construct($option=null){
	    	parent::__construct($option);
	    	$this->setName('frmAyuda');//nombre del form    	
	    	 
	    	$email=new Zend_Form_Element_Text('textAyudaEmail');		
	                $email->setRequired(true);
			$email->setAttrib('placeholder','e-mail');
		
	        $texttarea=new Zend_Form_Element_TexTarea('textAyudaMensaje');
	    	$texttarea->addFilter('StripTags');                
	                $texttarea->addFilter('StringTrim');
	                $texttarea->setAttrib('placeholder','mensaje');
	                $texttarea->setRequired(true);
	        
	    	$submit=new Zend_Form_Element_Submit('btnEnviarMensajeAyuda');
	    	$submit->setLabel('enviar');
	    	
	    	
	    	$this->addElements(array($email,$texttarea,$submit));//agregando lo q se ha creado al form
	    	$this->setMethod('post');//metodo post del form
	    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/login');
	    }
	    

}

