<?php

class Application_Form_Federaciones extends Zend_Form
{
	   
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    public function __construct($option=null){
		    	parent::__construct($option);
		    	$this->setName('frmFederaciones');//nombre del form
		        
                        $departamento=new Zend_Form_Element_Select('departamento');
		        $departamento->addFilter('StripTags');
		        $departamento->addFilter('StringTrim');
				$departamento->setRequired(false);
				$departamento->setAttrib('placeholder','departamento');
                
		        $this->addElement($departamento);
		        
		        $provincia=new Zend_Form_Element_Select('provincia');
		        $provincia->addFilter('StripTags');
		        $provincia->addFilter('StringTrim');
				$provincia->setRequired(false);
				$provincia->setAttrib('placeholder','provincia');                
        		$this->addElement($provincia);
        
        		$distrito=new Zend_Form_Element_Select('distrito');
                        $distrito->addFilter('StripTags');
                        $distrito->addFilter('StringTrim');
				$distrito->setRequired(false);
				$distrito->setAttrib('placeholder','distrito');        
                
		        $this->addElement($distrito);
                        
		    	$nombre=new Zend_Form_Element_Text('txtNombre');
				$nombre->setRequired(true);		
				$nombre->setAttrib('placeholder','nombre');
				$this->addElement($nombre);
		        
		        $direccion=new Zend_Form_Element_Text('txtDireccion');
				$direccion->setRequired(true);		
				$direccion->setAttrib('placeholder','direccion');
				$this->addElement($direccion);
		        
		        
		        
                        $telefono=new Zend_Form_Element_Text('txtTelefono');                
                        $telefono->setRequired(false);
                        $telefono->setAttrib('placeholder','telefono');
		        $this->addElement($telefono);
                        
                        $web=new Zend_Form_Element_Text('txtWeb');                
                        $web->setRequired(false);
                        $web->setAttrib('placeholder','Web');
		        $this->addElement($web);
                        
                        $email=new Zend_Form_Element_Text('txtEmail');                
                        $email->setRequired(false);
                        $email->setAttrib('placeholder','e-mail');
		        $this->addElement($email);
                        
                        $resolucion=new Zend_Form_Element_Text('txtResolucion');
                        $resolucion->setRequired(false);
                        $resolucion->setAttrib('placeholder','resolucion');
		        $this->addElement($resolucion);
                        
                        $presidente=new Zend_Form_Element_Text('txtPresidente');
                        $presidente->setRequired(false);
                        $presidente->setAttrib('placeholder','presidente');
		        $this->addElement($presidente);
                        
                        $visepresidente=new Zend_Form_Element_Text('txtVisepresidente');
                        $visepresidente->setRequired(false);
                        $visepresidente->setAttrib('placeholder','visepresidente');
		        $this->addElement($visepresidente);
                            
                        $secretario=new Zend_Form_Element_Text('txtSecretario');
                        $secretario->setRequired(false);
                        $secretario->setAttrib('placeholder','secretario');
		        $this->addElement($secretario);
                        
                        $tesorero=new Zend_Form_Element_Text('txtTesorero');
                        $tesorero->setRequired(false);
                        $tesorero->setAttrib('placeholder','tesorero');
		        $this->addElement($tesorero);
                        
                        $vocal=new Zend_Form_Element_Text('txtVocal');
                        $vocal->setRequired(false);
                        $vocal->setAttrib('placeholder','vocal');
		        $this->addElement($vocal);
                        
                        
                        $twitter=new Zend_Form_Element_Text('txtTwitter');
                        $twitter->setRequired(false);
                        $twitter->setAttrib('placeholder','twitter');
		        $this->addElement($twitter);
                        
                        $facebook=new Zend_Form_Element_Text('txtFacebook');
                        $facebook->setRequired(false);
                        $facebook->setAttrib('placeholder','facebook');
		        $this->addElement($facebook);
                        
		        $getFotos=new Zend_Form_Element_Hidden('getFotos');
			$this->addElement($getFotos);
		        
		        //UPDATE
		        if(isset($option)){
		            $id = new Zend_Form_Element_Hidden('id');    	
		            $id->setValue($option);
		            $this->addElement($id);		            
		        }
                            
		    	$submit=new Zend_Form_Element_Submit('btnRegistrarFederacion');
		    	$submit->setLabel('Registrar');
		    	$this->addElement($submit);
		    	
		    	$this->setMethod('post');//metodo post del form
		    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/add');
                        
    }
}

