<?php

class Application_Form_Contactos extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmContactos');//nombre del form
        
    	$email=new Zend_Form_Element_Text('txtEmail');
		$email->setRequired(true);		
		$email->setAttrib('placeholder','e-mail');
	$this->addElement($email);
        
        $descripcion=new Zend_Form_Element_Textarea('textDescripcion');		
                $descripcion->addFilter('StripTags');
                $descripcion->addFilter('StringTrim');
                $descripcion->setRequired(true);
		$descripcion->setAttrib('placeholder','descripcion');
        $this->addElement($descripcion);
        
    	$submit=new Zend_Form_Element_Submit('btnContactos');
    	$submit->setLabel('Enviar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/add');
    }
}

