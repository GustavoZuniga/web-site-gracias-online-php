<?php

class Application_Form_CrearCuenta extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmCrearCuentaUsuarios');//nombre del form
    	 
        $email=new Zend_Form_Element_Text('txtEmailRu');
                $email->addValidator(new Zend_Validate_EmailAddress(), true);
                $email->addValidator(new Zend_Validate_NotEmpty(), true);
		$email->setRequired(true);
		$email->setAttrib('placeholder','e-email');
        $this->addElement($email);
		
    	$password=new Zend_Form_Element_Password('txtPasswordRu');    	
    		$password->setRequired(true);		
		$password->setAttrib('placeholder','password');
        $this->addElement($password);
        
        $nombres=new Zend_Form_Element_Text('txtNombresRu');
		$nombres->setRequired(true);
		$nombres->setAttrib('placeholder','nombres');
        $this->addElement($nombres);
                
        $apellidos=new Zend_Form_Element_Text('txtApellidosRu');
		$apellidos->setRequired(true);
		$apellidos->setAttrib('placeholder','apellidos');
        $this->addElement($apellidos);
                   
        
        $rol=new Zend_Form_Element_Select('cmbRol');        
                $rol->addFilter('StripTags');
                $rol->addFilter('StringTrim');
                $rol->setRequired(true);
        
        $rol->addMultiOption('0', 'Seleccione un rol');
    	$rol->addMultiOption('1', 'Estudiante');
    	$rol->addMultiOption('2', 'Profesor');
        
        $this->addElement($rol);
        
    	$submit=new Zend_Form_Element_Submit('btnCrearCuentaUsuario');
    	$submit->setLabel('enviar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/login');
    }
    
}

