<?php

class Application_Form_Eventos extends Zend_Form
{
	   
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    public function __construct($option=null){
		    	parent::__construct($option);
		    	$this->setName('frmRegistrarEventos');//nombre del form
		        
		    	$titulo=new Zend_Form_Element_Text('txtNombreEvento');
				$titulo->setRequired(true);		
				$titulo->setAttrib('placeholder','nombre');
				$this->addElement($titulo);
		        
		        $descripcion=new Zend_Form_Element_Textarea('textDescripcion');		
		                $descripcion->addFilter('StripTags');
		                $descripcion->addFilter('StringTrim');
		                $descripcion->setRequired(true);
				$descripcion->setAttrib('placeholder','descripcion');
		        $this->addElement($descripcion);
		        
		        $lugar=new Zend_Form_Element_Text('txtLugar');
				$lugar->setRequired(true);		
				$lugar->setAttrib('placeholder','lugar');
				$this->addElement($lugar);
		        
		        $departamento=new Zend_Form_Element_Select('departamento');
		        $departamento->addFilter('StripTags');
		        $departamento->addFilter('StringTrim');
				$departamento->setRequired(false);
				$departamento->setAttrib('placeholder','departamento');
                
		        $this->addElement($departamento);
		        
		        $provincia=new Zend_Form_Element_Select('provincia');
		        $provincia->addFilter('StripTags');
		        $provincia->addFilter('StringTrim');
				$provincia->setRequired(false);
				$provincia->setAttrib('placeholder','provincia');                
        		$this->addElement($provincia);
        
        		$distrito=new Zend_Form_Element_Select('distrito');
                        $distrito->addFilter('StripTags');
                        $distrito->addFilter('StringTrim');
				$distrito->setRequired(false);
				$distrito->setAttrib('placeholder','distrito');        
                
		        $this->addElement($distrito);
		        
		        $fechaInicio=new Zend_Form_Element_Text('txtFechaInicio');                
                        $fechaInicio->setRequired(false);
                        $fechaInicio->setAttrib('placeholder','01/01/1990');
		        $this->addElement($fechaInicio);
                        
                        $fechaFin=new Zend_Form_Element_Text('txtFechaFin');                
                        $fechaFin->setRequired(false);
                        $fechaFin->setAttrib('placeholder','01/01/1990');
		        $this->addElement($fechaFin);
		        
		        $hora=new Zend_Form_Element_Text('txtHora');                
				$hora->setRequired(false);
				$hora->setAttrib('placeholder','13:01');
		        $this->addElement($hora);
		        
                        $email=new Zend_Form_Element_Text('txtEmail');                
                        $email->setRequired(false);
                        $email->setAttrib('placeholder','e-mail');
		        $this->addElement($email);
                        
                        $telefono=new Zend_Form_Element_Text('txtTelefono');                
                        $telefono->setRequired(false);
                        $telefono->setAttrib('placeholder','telefono');
		        $this->addElement($telefono);
                        
                        $twitter=new Zend_Form_Element_Text('txtTwitter');
                        $twitter->setRequired(false);
                        $twitter->setAttrib('placeholder','twitter');
		        $this->addElement($twitter);
                        
                        $facebook=new Zend_Form_Element_Text('txtFacebook');
                        $facebook->setRequired(false);
                        $facebook->setAttrib('placeholder','facebook');
		        $this->addElement($facebook);
                        
		        $getFotos=new Zend_Form_Element_Hidden('getFotos');
				$this->addElement($getFotos);
		        
		        $getVideos=new Zend_Form_Element_Hidden('getVideos');
				$this->addElement($getVideos);
		        
		        //UPDATE
		        if(isset($option)){
		            $id = new Zend_Form_Element_Hidden('id');    	
		            $id->setValue($option);
		            $this->addElement($id);
		            
		            $getFotosEliminados=new Zend_Form_Element_Hidden('getFotosEliminados');
		            $this->addElement($getFotosEliminados);
		
		            $getVideosEliminados=new Zend_Form_Element_Hidden('getVideosEliminados');
		            $this->addElement($getVideosEliminados);
		
		            $getTraidos=new Zend_Form_Element_Hidden('getTraidos');//esto es para videos
		            $this->addElement($getTraidos);
		        }
		        
                        $mapaLatitud = new Zend_Form_Element_Hidden('mapaLatitud');		            
		        $this->addElement($mapaLatitud);
                        
                        $mapaLongitud = new Zend_Form_Element_Hidden('mapaLongitud');		            
		        $this->addElement($mapaLongitud);
                            
		    	$submit=new Zend_Form_Element_Submit('btnRegistrarEvento');
		    	$submit->setLabel('Registrar');
		    	$this->addElement($submit);
		    	
		    	$this->setMethod('post');//metodo post del form
		    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/add');
    }
}

