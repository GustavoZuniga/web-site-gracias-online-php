<?php
class Application_Form_SedeDeportiva extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmSedeDeportiva');//nombre del form
        
    	$nombre=new Zend_Form_Element_Text('txtNombre');
		$nombre->setRequired(true);		
		$nombre->setAttrib('placeholder','nombre');
		$this->addElement($nombre);
        
        $ubicacion=new Zend_Form_Element_Text('txtUbicacion');
		$ubicacion->setRequired(true);
		$ubicacion->setAttrib('placeholder','ubicacion');
                $ubicacion->setAttrib('class', 'text');
        $this->addElement($ubicacion);
        
        $departamento=new Zend_Form_Element_Select('sltDepartamento');
                $departamento->addFilter('StripTags');
                $departamento->addFilter('StringTrim');
		$departamento->setRequired(true);
                $departamento->setAttrib('class', 'text');
		$departamento->setAttrib('placeholder','departamento');
                
                $this->addElement($departamento);
        
        $provincia=new Zend_Form_Element_Select('sltProvincia');
                $provincia->addFilter('StripTags');
                $provincia->addFilter('StringTrim');
		$provincia->setRequired(true);
                $provincia->setAttrib('class', 'text');
		$provincia->setAttrib('placeholder','provincia');
                
                $this->addElement($provincia);
        
        $distrito=new Zend_Form_Element_Select('sltDistrito');
                $distrito->addFilter('StripTags');
                $distrito->addFilter('StringTrim');
		$distrito->setRequired(true);
                $distrito->setAttrib('class', 'text');
		$distrito->setAttrib('placeholder','distrito');
                
                $this->addElement($distrito);
        
        $equipamiento = new Zend_Form_Element_Textarea('txtEquipamiento');		
                $equipamiento->addFilter('StripTags');
                $equipamiento->addFilter('StringTrim');
                $equipamiento->setOptions(array('cols' => '100', 'rows' => '10'));
                $equipamiento->setRequired(false);
		$equipamiento->setAttrib('placeholder','equipamiento');
        $this->addElement($equipamiento);
                
        
        $telefono = new Zend_Form_Element_Text('txtTelefono');
                    $telefono->setRequired(true);		
                    $telefono->setAttrib('placeholder','telefono');
                    $telefono->setAttrib('class', 'text');
	$this->addElement($telefono);
        
        $email = new Zend_Form_Element_Text('txtEmail');                
                    $email->setRequired(true);
                    $email->addValidator(new Zend_Validate_EmailAddress(), true);
                    $email->addValidator(new Zend_Validate_NotEmpty(), true);
                    $email->errMsg = "No parece ser un correo electrónico valido";
                    $email->setAttrib('placeholder','email');
                    $email->setAttrib('class', 'text');
        $this->addElement($email);
        
        $atencion = new Zend_Form_Element_Text('txtHoraatencion');
                    $atencion->setRequired(true);		
                    $atencion->setAttrib('placeholder','horario atencion');
                    $atencion->setAttrib('class', 'text');
	$this->addElement($atencion);
        
        $costo = new Zend_Form_Element_Text('txtCostohora');
                    $costo->setRequired(true);		
                    $costo->setAttrib('placeholder','costo por hora');
                    $costo->setAttrib('class', 'text');
	$this->addElement($costo);
        
        $responsable = new Zend_Form_Element_Text('txtResponsable');
                    $responsable->setRequired(true);		
                    $responsable->setAttrib('placeholder','responsable');
                    $responsable->setAttrib('class', 'text');
	$this->addElement($responsable);
        
        $getFotos=new Zend_Form_Element_Hidden('getFotos');
	$this->addElement($getFotos);

		//UPDATE
		if(isset($option)){
			$id = new Zend_Form_Element_Hidden('id');
			$id->setValue($option);
			$this->addElement($id);
		
			$getFotosEliminados=new Zend_Form_Element_Hidden('getFotosEliminados');
			$this->addElement($getFotosEliminados);
			
			$fotoInicio=new Zend_Form_Element_Hidden('fotoInicio');
			$this->addElement($fotoInicio);
		}
		
    	$submit=new Zend_Form_Element_Submit('btnRegistrarSede');
    	$submit->setLabel('enviar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/admin/sede-deportiva/nuevo');
    }

}

