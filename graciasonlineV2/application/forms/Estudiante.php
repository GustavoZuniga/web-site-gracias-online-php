<?php
/**
 * Description of Contactos
 *
 * @author Zuñiga
 */
class Application_Form_Estudiante extends Zend_Form
{  
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmEstudiante');//nombre del form
        $id=0;
        if(isset($option)){
           $id = new Zend_Form_Element_Hidden('hdnId');
           $id->setValue($option);
        }
        
    	$firstname = new Zend_Form_Element_Text('txtFirstname');
                    $firstname->setRequired(true);		
                    $firstname->setAttrib('required','required');
                    $firstname->setAttrib('placeholder','Nombres');
                    
	
                    
        $lastanme = new Zend_Form_Element_Text('txtLastanme'); 
                   
                    $lastanme->setRequired(true);
					$lastanme->setAttrib('required','required');					
                    $lastanme->setAttrib('placeholder','Apellidos');
					
		$email = new Zend_Form_Element_Text('txtEmail'); 
                    $email->setRequired(true);		
                    $email->setAttrib('placeholder','Correo');
					
		

    	$submit=new Zend_Form_Element_Submit('btnEnviar');
    	$submit->setLabel('Guardar');    	
    	$submit->setAttrib('id', 'btn');
        
    	$this->addElements(array($id,$firstname,$lastanme,$email,$submit));//agregando lo q se ha creado al form
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/estudiantes/index');
    }
}


