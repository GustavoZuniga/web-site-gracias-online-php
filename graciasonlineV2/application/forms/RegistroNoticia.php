<?php
/**
 * Description of Contactos
 *
 * @author Zuñiga
 */
class Application_Form_RegistroNoticia extends Zend_Form
{  
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmRegistrarNoticia');//nombre del form
        $id=0;
        if(isset($option)){
           $id = new Zend_Form_Element_Hidden('id');            
           $id->setValue($option);
        }
        
    	$titulo = new Zend_Form_Element_Text('txtTitulo');
                    $titulo->setRequired(true);		
                    $titulo->setAttrib('required','required');
                    $titulo->setAttrib('placeholder','titulo');
                    
	
                    
        $resumen = new Zend_Form_Element_Text('txtResumen'); 
                   
                    $resumen->setRequired(true);		
                    $resumen->setAttrib('placeholder','resumen');
        
                    
    	$descripcion = new Zend_Form_Element_Textarea('txtDescripcion'); 
                    $descripcion->setOptions(array('cols' => '100', 'rows' => '10'));
                    $descripcion->setRequired(true);		
                    $descripcion->setAttrib('placeholder','descripcion');
                
        $lugar = new Zend_Form_Element_Text('txtLugar');
                    $lugar->setRequired(true);		
                    $lugar->setAttrib('required','required');
                    $lugar->setAttrib('placeholder','lugar');
                
        /*$picture = new Zend_Form_Element_File('uplimage');
                $picture->addValidator('Extension', false, 'jpg,png,gif,jpeg');
                $picture->addValidator('Size',false,'10024000');
                $picture->setDestination(APPLICATION_PATH . '/../public/imagenes/noticias/');
                $picture->setValueDisabled(true);*/
                
        /*$picture = new Zend_Form_Element_File('picture', array(
            'label' => 'Picture',
            'required' => true,
            'MaxFileSize' => 2097152, // 2097152 bytes = 2 megabytes
            'validators' => array(
                array('Count', false, 1),
                array('Size', false, 2097152),
                array('Extension', false, 'gif,jpg,png'),
                array('ImageSize', false, array('minwidth' => 100,
                                                'minheight' => 100,
                                                'maxwidth' => 1000,
                                                'maxheight' => 1000))
            )
        ));
 
        $picture->setValueDisabled(true);*/

    			
    	$submit=new Zend_Form_Element_Submit('btnEnviarNoticia');
    	$submit->setLabel('Guardar');    	
    	$submit->setAttrib('id', 'btnNoticia');
        
    	$this->addElements(array($id,$titulo,$resumen,$descripcion,$lugar,$submit));//agregando lo q se ha creado al form
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/noticias/index');
    }
}


