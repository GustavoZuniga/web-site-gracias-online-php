<?php
class Application_Form_EquiposBuscar extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmEquipoBuscar');//nombre del form
        
        $disciplina = new Zend_Form_Element_Select('disciplina');
                $disciplina->addFilter('StripTags');
                $disciplina->addFilter('StringTrim');
		$disciplina->setRequired(true);
                $disciplina->setAttrib('class', 'text');
		$disciplina->setAttrib('placeholder','disciplina');        
                
                $tableDisciplinas=new Application_Model_DbTable_Disciplinas();
                $getDisciplinas=$tableDisciplinas->getDisciplinas();
                
                foreach ($getDisciplinas as $row):
                    $disciplina->addMultiOption($row['id'], $row['nombre']);
                endforeach;
        
        $this->addElement($disciplina);
        
    	$submit=new Zend_Form_Element_Submit('btnBuscar');
    	$submit->setLabel('enviar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/usuarios/equipos/index');
    }
    
    

}

