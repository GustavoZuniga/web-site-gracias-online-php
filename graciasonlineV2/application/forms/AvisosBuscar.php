<?php
class Application_Form_AvisosBuscar extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmBolsaTrabajoBusqueda');//nombre del form
        
        $disciplina = new Zend_Form_Element_Select('sltDisciplina');
                $disciplina->addFilter('StripTags');
                $disciplina->addFilter('StringTrim');
		$disciplina->setRequired(true);
                $disciplina->setAttrib('class', 'select-margin-right');
		$disciplina->setAttrib('placeholder','disciplina');        
                
                $tableDisciplinas=new Application_Model_DbTable_Disciplinas();
                $getDisciplinas=$tableDisciplinas->getDisciplinas();
                $disciplina->addMultiOption('cualquiera','Cualquiera');
                foreach ($getDisciplinas as $row):
                    $disciplina->addMultiOption($row['id'], $row['nombre']);
                endforeach;
                $disciplina->setValue('cualquiera');
        
        $this->addElement($disciplina);
        
        $departamento=new Zend_Form_Element_Select('sltDepartamento');
                $departamento->addFilter('StripTags');
                $departamento->addFilter('StringTrim');
		$departamento->setRequired(true);
                $departamento->setAttrib('class', 'select-margin-right');
		$departamento->setAttrib('placeholder','departamento');
                
                $tableUbigeo=new Application_Model_DbTable_Ubigeo();
                $getDepartamentos=$tableUbigeo->getDepartamentos();
                $departamento->addMultiOption('cualquiera','Cualquiera');                
                foreach ($getDepartamentos as $rowDepa):
                        $departamento->addMultiOption($rowDepa['departamento'], $rowDepa['departamento']);
                endforeach;
                
                $departamento->setValue('cualquiera');
                
        $this->addElement($departamento);
        
        $fecha = new Zend_Form_Element_Select('sltFecha');
                $fecha->addFilter('StripTags');
                $fecha->addFilter('StringTrim');
		$fecha->setRequired(true);
                $fecha->setAttrib('class', 'select-margin-right');
		$fecha->setAttrib('placeholder','fecha');
                $fecha->addMultiOption('cualquiera','Cualquiera');  
                $fecha->addMultiOption('2','Hoy y ayer');
                $fecha->addMultiOption('3','Ultimos 3 días');
                $fecha->addMultiOption('7','Ultimos 7 días');
                $fecha->addMultiOption('14','Ultimos 14 días');
                $fecha->addMultiOption('21','Ultimos 21 días');
                $fecha->addMultiOption('31','Ultimos 31 días');
                $fecha->setValue('cualquiera');
                
        $this->addElement($fecha);
        
    	$submit=new Zend_Form_Element_Submit('btnBolsaTrabajoBusqueda');
    	$submit->setLabel('enviar');
    	$this->addElement($submit);
    	
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/usuarios/equipos/index');
    }
    
    

}

