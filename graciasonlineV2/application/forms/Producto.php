<?php
/**
 * Description of Contactos
 *
 * @author Zuñiga
 */
class Application_Form_Producto extends Zend_Form
{  
    public function __construct($option=null){
    	parent::__construct($option);
    	$this->setName('frmProducto');//nombre del form
        $id=0;
        if(isset($option)){
           $id = new Zend_Form_Element_Hidden('hdnId');
           $id->setValue($option);
        }
        
    	$titulo = new Zend_Form_Element_Text('txtNombre');
                    $titulo->setRequired(true);		
                    $titulo->setAttrib('required','required');
                    $titulo->setAttrib('placeholder','titulo');
                    
	
                    
        $precio = new Zend_Form_Element_Text('txtPrecio'); 
                   
                    $precio->setRequired(true);		
                    $precio->setAttrib('placeholder','00.00');

    	$submit=new Zend_Form_Element_Submit('btnEnviar');
    	$submit->setLabel('Guardar');    	
    	$submit->setAttrib('id', 'btnProducto');
        
    	$this->addElements(array($id,$titulo,$precio,$submit));//agregando lo q se ha creado al form
    	$this->setMethod('post');//metodo post del form
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/productos/index');
    }
}


