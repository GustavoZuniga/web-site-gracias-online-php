<?php

class Application_Model_DbTable_NoticiaImagen extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_noticia_imagen';
    
    /**
     *
     * @param array $data
     * @return  int $resp (Confirmacion de haber grabado)
     * @todo graba los datos de registro de noticia
     */
    public function addRegistro($data){ 
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
     public function updateNoticia($data){
         
        $id = $data['noticia_id'];     
        return $this->update($data, 'noticia_id = '. (int)$id);
    }
                
}

