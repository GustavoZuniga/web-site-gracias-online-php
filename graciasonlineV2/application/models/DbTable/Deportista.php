<?php

class Application_Model_DbTable_Deportista extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_deportista';
    
     /**
     *     
     * @return  fetchAll (trae el listado en array)
     * @todo trae el listado de deportistas
     */
    
    public function getListaDeportistas(){ 

        $data=array('d.id','d.nombres','d.apellidos','d.dni','d.departamento'
                    ,'d.email','d.estado','d.equipo_id'
                    );
        $dataEquipo = array('e.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->from(array("d" => $this->_name),$data);
        $select->joinLeft(array("e" => 'equipos'),'e.id = d.equipo_id',$dataEquipo);
        $select->order("d.id desc");
        
        return $this->fetchAll($select)->toArray();
        
    }
    
    
    public function getDeportista($id){
       
        $data=array('d.id','d.nombres','d.apellidos','d.dni','d.departamento',
                    'd.email','d.equipo_id'
                    );
        $select = $this->select();
        $select->from(array("d" => $this->_name),$data);
        $select->where("d.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
    
    /**
     * @param int $id (id de equipo)     
     * @return  fetchAll (trae el listado en array)
     * @todo trae el listado de deportistas del equipo X
     */
    
    public function getDeportistasEquipo($id){
        $data = array('a.id','a.nombres');
        
        $select = $this->select();
        $select->from(array("a" => $this->_name),$data);
        $select->where("a.equipo_id =".(int)$id);
        
        return $this->fetchAll($select)->toArray();        
    }
    
    /**
     *
     * @param $id
     * @return  int 
     * @todo Obtiene el detalle del deportista
     */
    public function getDetalleDeportista($id){
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row){
                throw new Exception("Count not find row $id");
        }
        return $row->toArray();
    }
    
    /**
     *
     * @param array $data
     * @return  int $resp (Confirmacion de haber grabado)
     * @todo graba los datos de registro de un deportista
     */
    public function addDeportista($data){
       
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
    
    
    public function updateDeportista($data){
        
            $id = $data['id'];     
            return $this->update($data, 'id = '. (int)$id);
    }
    

    public function deleteDeportista($id){
            $this->delete('id =' . (int)$id);
    }
    
    
    
    
                
}

