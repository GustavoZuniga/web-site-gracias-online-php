<?php

class Application_Model_DbTable_ImagenEntidad extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_imagen_entidad';
    
    /**
     *
     * @param array $data
     * @return  int $resp (Confirmacion de haber grabado)
     * @todo graba los datos de registro de noticia
     */
    public function insertImagen($data){ 
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
     public function updateImagen($data){
     
        $id = $data['img_entidad_id'];     
        return $this->update($data, 'img_entidad_id = '. (int)$id);
    }
                
}

