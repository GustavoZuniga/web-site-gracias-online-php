<?php

class Application_Model_DbTable_Noticia extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_noticia';
    
    /**
     *
     * @param array $data
     * @return  int $resp (Confirmacion de haber grabado)
     * @todo graba los datos de registro de noticia
     */
    public function addRegistro($data){ 
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
    
    /**
     *
     * @param $id
     * @return  int 
     * @todo Obtiene el detalle de noticia
     */
    public function getNoticia($id){
      
        $data=array('n.id','n.titulo','n.resumen','n.lugar','n.descripcion','n.fecha_registro'
                    );
        $dataImagen = array('i.noticia_id','i.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->from(array("n" => $this->_name),$data);
        $select->joinLeft(array("i" => 'cms_noticia_imagen'),'i.noticia_id = n.id',$dataImagen);
        $select->where("n.id =".(int)$id);
		
        return $this->fetchRow($select)->toArray();
		
    }
    
    public function addNoticia($titulo, $descripcion){
		$data = array('titulo' => $titulo, 'descripcion' => $descripcion,);
		$this->insert($data);
    }

    public function updateNoticia($data){
            $id = $data['id'];     
            return $this->update($data, 'id = '. (int)$id);
    }

    public function deleteNoticia($id){
            $this->delete('id =' . (int)$id);
    }
    
    public function getListaNoticias(){    	
        $data=array('n.id','n.titulo','n.resumen','n.descripcion'
                    ,'n.fecha_registro'
                    ,'n.estado'
                    );
        
        $dataImagen = array('i.noticia_id','i.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("id desc");
        $select->from(array("n" => $this->_name),$data);
        $select->joinLeft(array("i" => 'cms_noticia_imagen'),'i.noticia_id = n.id',$dataImagen);
        
        return $this->fetchAll($select)->toArray();
    }
    
    /**
     *     
     * @return  fetchAll (trae el listado en array)
     * @todo trae el listado de noticias seccion aside
     */
    
     public function getWigetNoticias(){ 

         $data=array('n.id','n.titulo','n.resumen','n.descripcion'
                    ,'n.fecha_registro');
        
        $dataImagen = array('i.noticia_id','i.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("id desc");
        $select->limit(3);
        $select->from(array("n" => $this->_name),$data);
        $select->joinLeft(array("i" => 'cms_noticia_imagen'),'i.noticia_id = n.id',$dataImagen);
        
        return $this->fetchAll($select)->toArray();
    }
    
    public function activarNoticia($id,$act){    	
        $data['estado'] = $act;
        return $this->update($data, 'id = '. (int)$id);
    }
    
    public function buscar($consulta){
        
        $data=array('n.id','n.titulo','n.resumen','n.descripcion','n.fecha_registro'
                    );
        
        $dataImagen = array('i.noticia_id','i.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("id desc");
        $select->from(array("n" => $this->_name),$data);
        $select->join(array("i" => 'cms_noticia_imagen'),'i.noticia_id = n.id',$dataImagen);
        
        $select->where('titulo LIKE "%'.$consulta.'%"');
        
        return $this->fetchAll($select)->toArray();
    }
    
    /**
     * @author Dimas Gustavo  
     * @param int $id (id de Federacion)   
     * @return  fetchAll (trae el listado en array)
     * @todo Traemos todas las noticias de una federacion federaciones
     */
    public function getNoticasDeFederacion($id){    	
        $data=array('n.id','n.titulo','n.resumen','n.descripcion'
                    ,'n.fecha_registro'
                    ,'n.estado'
                    );
        
        $dataImagen = array('i.noticia_id','i.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("id desc");
        $select->from(array("n" => $this->_name),$data);
        $select->joinLeft(array("i" => 'cms_noticia_imagen'),'i.noticia_id = n.id',$dataImagen);
        $select->where('n.id_federaciones='.(int)$id);
        
        return $this->fetchAll($select)->toArray();
    }
                
}

