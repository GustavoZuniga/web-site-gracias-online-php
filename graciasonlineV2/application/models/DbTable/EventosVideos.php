<?php

class Application_Model_DbTable_EventosVideos extends Zend_Db_Table_Abstract
{

    protected $_name = 'eventos_videos';
    
    /**
     *
     *@param array $data
     *@return row (1 o 0)
     *@todo grabar un video del evento
     */
    
    public function addVideos($data){        
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
    /**
     *
     *@param array $ids
     *@return int delete
     *@todo elimina todos los videos traidos
     */
    public function deleteVideos($ids){    	
        $where = $this->getAdapter()->quoteInto('id IN (?)', $ids);
        return $this->delete($where);
    }
    
    /**
     *
     *@param int $idEvento
     *@return fetchAll (listado de videos)
     *@todo traemos los videos del eventos
     */
    
    public function getVideos($idEvento){        
    	$select = $this->select();    	
    	$data=array('a.id','a.nombre');
        
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("a.id_eventos =".$idEvento);
        
    	return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param int $id
     *@return fetchRow (un solo registro)
     *@todo traemos un solo video
     */
    
    public function getVideo($id){        
    	$select = $this->select();    	
    	$data=array('a.id','a.nombre');
        
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("a.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
}

