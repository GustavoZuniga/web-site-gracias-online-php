<?php

class Application_Model_DbTable_Disciplinas extends Zend_Db_Table_Abstract
{
    CONST ESTADO_ACTIVO=1;
    
    protected $_name = 'disciplinas';

    /**
     *
     *@return fetchAll(listado de deportes)
     *@todo traemos los los deportes
     */    
    public function getDisciplinas(){
        $select = $this->select();    	
    	$data=array('a.id','a.nombre');
        
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("a.estado = ?", self::ESTADO_ACTIVO);
        
    	return $this->fetchAll($select)->toArray();
    }

}

