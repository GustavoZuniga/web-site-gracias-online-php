<?php
/**
 * @autor=Dimas Gustavo 	 
 */
class Application_Model_DbTable_EventosParticipar extends Zend_Db_Table_Abstract
{

    protected $_name = 'eventos_participar';

    /**
     *
     *@param array $data
     *@return row (1 o 0)
     *@todo grabar el participante
     */
    public function add($data){
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
    /**
     *
     *@param int $idusuario,int $idevento
     *@return execute
     *@todo eliminamos al paticipante
     */
    public function deleteParticipar($idusuario,$idevento){
        $sql="delete from ".$this->_name." where id_usuario=".$idusuario." and id_evento=".$idevento;
        $resp=$this->getAdapter()->query($sql);
        $resp->execute();
    }
    
    /**
     *
     *@param int $idusuario,int $idevento
     *@return fetchRow (un registro)
     *@todo verificamos si el usuario esta participando
     */
    public function existe($idusuario,$idevento){        
        $select = $this->select();
        
    	$select->from(array("a" => $this->_name),array('count(a.id) as count'));
    	$select->where("a.id_usuario =".$idusuario);
        $select->where("a.id_evento =".$idevento);
        
    	return $this->fetchRow($select)->toArray();
    }
    
    
}

