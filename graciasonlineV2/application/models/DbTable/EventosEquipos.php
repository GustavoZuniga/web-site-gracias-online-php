<?php

class Application_Model_DbTable_EventosEquipos extends Zend_Db_Table_Abstract
{

    protected $_name = 'eventos_equipos';
    CONST ACTIVO=1;
    CONST INACTIVO=0;
    
    /**
     *
     *@param array $data
     *@return row (1 o 0)
     *@todo graba el equipo que esta participando en el evento
     */
    public function addEquipos($data){        
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
    /**
     *
     * @param array $data
     * @return  update (Confirmacion de haber grabado)
     * @todo modificar los equipos del evento
     */
    public function updateEquipos($data,$id){    	
    	return $this->update($data, 'id = '. (int)$id);
    }
    
    /**
     *
     * @param array $data
     * @return  update (Confirmacion de haber grabado)
     * @todo elimina el equipo del evento
     */
    public function deleteEquipos($id){        
    	return $this->delete('id = '. (int)$id);
    }
    
    /**
     *
     *@param int $id (id del equipo)
     *@return fetchAll (listado de eventos)
     *@todo lista de ventos donde participa el equipo x
     */
    
    public function getParticipa($id){
        $dataEventosEquipos=array('a.id','a.id_eventos',"a.id_equipos");
        $dataEventos=array('b.nombre_evento','date_format(b.fecha,"%d-%m-%Y") as fecha','date_format(b.hora,"%H:%i") as hora');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("a" => $this->_name),$dataEventosEquipos);    	
    	$select->joinLeft(array('b' => 'eventos'), 'b.id = a.id_eventos', $dataEventos);
        $select->where("a.id_equipos =".(int)$id);
        
        return $this->fetchAll($select)->toArray();
    }
    
    
    
    /**
     *
     *@param int $id (id del evento)
     *@return fetchAll (listado de equipos)
     *@todo listar equipos que participan en eventos
     */
    
    public function getEquipos($id){
        $dataEventosEquipos=array('a.id_eventos');
        $dataEquipos=array('b.id','b.nombre');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("a" => $this->_name),$dataEventosEquipos);    	
    	$select->joinLeft(array('b' => 'equipos'), 'b.id = a.id_equipos', $dataEquipos);
        $select->where("a.id_eventos =".(int)$id);
        
        return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param int $idEquipo,$idEvento
     *@return fetchRow (listado de equipos)
     *@todo verifica si el equipo ya esta en el evento
     */
    
    public function existe($idEquipo,$idEvento){        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("a" => $this->_name),array('count'=>'count(a.id)'));    	
    	
        $select->where("a.id_equipos =".(int)$idEquipo);
        $select->where("a.id_eventos =".(int)$idEvento);
        
        return $this->fetchRow($select)->toArray();
    }
    
   
    

}

