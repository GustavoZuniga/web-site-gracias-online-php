<?php

class Application_Model_DbTable_Postulante extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_postulante';
   
    /**
     *
     * @param array $data
     * @return  int row (Confirmacion de haber grabado)
     * @todo inserta un nuevo postulante
     */
    public function addPostulante($data){ 
      
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
    
}

