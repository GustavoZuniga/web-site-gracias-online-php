<?php

class Application_Model_DbTable_Equipos extends Zend_Db_Table_Abstract
{
    CONST ACTIVO=1;
    CONST INACTIVO=0;
    CONST ELIMINADO=2;
    
    protected $_name = 'equipos';
    
    
     /**
     * @param int $id (id de usuario)    
     * @return  fetchAll (trae el listado en array)
     * @todo lista de equipos($id >0  -->equipos de usuario,$id==0  -->todos los equipo)
     */
    
    public function getListaEquipos($id=0){
        
        $dataEquipos=array('e.id','e.nombre','e.deporte','e.direccion_postal'
                            ,'e.ubigeo','e.telefono','e.email','e.estado','e.foto_inicio','e.destacado'
                            );
        
        $dataDisciplinas=array('b.nombre as disciplina');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("e" => $this->_name), $dataEquipos);
    	$select->joinLeft(array('b' => 'disciplinas'), 'b.id = e.id_disciplinas', $dataDisciplinas);
        $select->where('e.estado='.self::ACTIVO);
        if((int)$id>0):
            $select->where('e.usuarioregistro='.(int)$id);
        endif;
        $select->order("id desc");
        
        return $this->fetchAll($select)->toArray();
    }
    
    /**     
     * @param int $id (id de disciplina)
     * @return  fetchAll (trae el listado en array)
     * @todo buscamos equipos por disciplina
     */
    
    public function getBuscarEquipos($id){
    	$dataEquipos=array('e.id','e.nombre','e.deporte','e.direccion_postal'
                            ,'e.ubigeo','e.telefono','e.email','e.estado','e.foto_inicio'
                            );
        
        $dataDisciplinas=array('b.nombre as disciplina');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("e" => $this->_name), $dataEquipos);
    	$select->joinLeft(array('b' => 'disciplinas'), 'b.id = e.id_disciplinas', $dataDisciplinas);
        $select->where('e.id_disciplinas='.(int)$id);
        
        return $this->fetchAll($select)->toArray();
    }
    
    /**
     * @author Dimas Gustavo
     * @param int $id (id de equipo)
     * @return  fetchRow (registro)
     * @todo traemos el registro del equipo
     */
    
    public function getEquipo($id){
    	$dataEquipos=array('e.id','e.nombre','e.deporte','e.direccion_postal'
                            ,'e.ubigeo','e.telefono','e.email'
                            );
        
        $dataDisciplinas=array('b.id as id_disciplina','b.nombre as disciplina');
        
        $dataUbigeo=array('c.departamento','c.provincia','c.distrito');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("e" => $this->_name), $dataEquipos);
    	$select->joinLeft(array('b' => 'disciplinas'), 'b.id = e.id_disciplinas', $dataDisciplinas);
        $select->joinLeft(array('c' => 'ubigeo'), 'c.id = e.ubigeo', $dataUbigeo);
        $select->where("e.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
    
    /**
     * @author Dimas Gustavo
     * @param int $id (id de equipo)
     * @return  fetchRow (registro)
     * @todo traemos el registro del equipo (para el nombre de "asignarEquipoAction()")
     */
    
    public function getEquipoSolo($id){
    	$dataEquipos=array('e.id','e.nombre');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("e" => $this->_name), $dataEquipos);    	
        $select->where("e.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
    
    /**
     *
     * @param array $data
     * @return  int save (1,0)
     * @todo graba los datos del equipo
     */
    public function addEquipo($data){                
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
    
    /**
     *
     * @param array $data
     * @return  update (Confirmacion de haber grabado)
     * @todo modificar un equipo
     */
    public function updateEquipos($data,$id){    	
    	return $this->update($data, 'id = '. (int)$id);
    }
    
    /**
     *
     * @param array $data
     * @return  update (1,0)
     * @todo eliminamos el equipo (cambiamos al estado 2)
     */
    
    public function deleteEquipo($data,$id){
        return $this->update($data, 'id = '. (int)$id);
    }
    
    /**
     *     
     * @return  fetchAll (trae el listado en array)
     * @todo trae el listado de equipos
     */
    
    public function getComboEquipos(){
        
        $dataEquipos=array('e.id','e.nombre','e.deporte','e.direccion_postal'
                            ,'e.ubigeo','e.telefono','e.email','e.estado'
                            );
        
        $dataDisciplinas=array('b.nombre as disciplina');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("e" => $this->_name), $dataEquipos);
    	$select->joinLeft(array('b' => 'disciplinas'), 'b.id = e.id_disciplinas', $dataDisciplinas);
        
        return $this->fetchAll($select)->toArray();
    }
    
    
    public function destacarEquipo($id,$act){        
        $data['destacado'] = $act;
        return $this->update($data, 'id = '. (int)$id);
    }
    
    
    public function getEquiposDestacados(){  
        
        $select = $this->select();        
        $dataEquipos=array('e.id','e.nombre','e.deporte','e.direccion_postal'
                            ,'e.ubigeo','e.telefono','e.email','e.estado'
                            ,'e.foto_inicio','e.destacado'
                            );
        $select->where("destacado=1");
        $select->order("id desc");
        $select->from(array("e" => $this->_name),$dataEquipos);
        $select->limit(3);
        
        return $this->fetchAll($select)->toArray();
    
    }
    
    
                
}

