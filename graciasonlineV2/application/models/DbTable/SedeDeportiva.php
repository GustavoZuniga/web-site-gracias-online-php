<?php

class Application_Model_DbTable_SedeDeportiva extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_sede_deportiva';
    
     /**
     * @author Dimas Gustavo    
     * @return  fetchAll (trae el listado en array)
     * @todo traemos el listado de Sedes Deportivas
     */
    
    public function getListaSedeDeportiva(){
        $dataSedeDeportivas=array('s.id','s.nombre','s.ubicacion','s.telefono'
                            ,'s.email','s.equipamiento','s.horario_atencion'
                            ,'s.costo_hora','s.responsable','s.fecharegistro'
        					,'s.foto_inicio'
                            );
        
        $dataSedeDeportivasFotos=array('b.departamento','b.provincia','b.distrito');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("s" => 'cms_sede_deportiva'), $dataSedeDeportivas);
    	$select->join(array('b' => 'ubigeo'), 'b.id = s.ubigeo', $dataSedeDeportivasFotos);
        
        return $this->fetchAll($select)->toArray();
    }
    
    /**
     * @author Dimas Gustavo
     * @param int $id (id de ubigeo)
     * @return  fetchAll (trae el listado en array)
     * @todo buscamos las sedes deportivas por criterio de busqueda
     */
    
    public function getBuscarSedes($id){
    	$dataSedeDeportivas=array('s.id','s.nombre','s.ubicacion','s.telefono'
    			,'s.email','s.equipamiento','s.horario_atencion'
    			,'s.costo_hora','s.responsable','s.fecharegistro'
    			,'s.foto_inicio','s.ubigeo'
    	);
    
    	$select = $this->select();
    	$select->from(array("s" => 'cms_sede_deportiva'), $dataSedeDeportivas);
    	$select->where('s.ubigeo='.$id);
    
    	return $this->fetchAll($select)->toArray();
    }
    
    
    /**
     * @author Dimas Gustavo
     * @param int $id (id de Sedes Deportivas)
     * @return  fetchRow (registro)
     * @todo traemos el registro de sedes deportivas
     */
    
    public function getSedeDeportiva($id){
    	$select = $this->select();
    	
    	$data=array('s.id','s.nombre','s.ubicacion','s.telefono'
    			,'s.email','s.equipamiento',"s.horario_atencion"
    			,'s.costo_hora','s.responsable','s.fecharegistro'
    			,'s.ubigeo','s.foto_inicio'
    	);
    	
    	$select->from(array("s" => $this->_name),$data);
    	$select->where("s.id =".(int)$id);
    	
    	return $this->fetchRow($select)->toArray();
    }
        
    /**
     *
     * @param array $data
     * @return  int $resp (Confirmacion de haber grabado)
     * @todo graba los datos de registro de sede deportiva
     */
    public function addSedeDeportiva($data){
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
    
    /**
     *
     * @param int $id,array $data
     * @return  return (1,0)
     * @todo modifica los datos de Sedes Deportivas
     */
    public function updateSedeDeportiva($data,$id){            
            return $this->update($data, 'id = '.(int)$id);
    }
	
    public function deleteSedeDeportiva($id){//evaluar si se elimara directamente o es un cambio de estado, si se elimina difinitivamente (que se elimine tambien el fisico)
            $this->delete('id =' . (int)$id);
    }
    
    public function buscarSedeDeportiva($nombre){
    	
    }
    
                
}

