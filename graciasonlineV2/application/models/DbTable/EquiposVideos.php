<?php

class Application_Model_DbTable_EquiposVideos extends Zend_Db_Table_Abstract
{

    protected $_name = 'equipos_videos';
	
    /**
     *
     *@param array $data
     *@return row (1 o 0)
     *@todo grabar un video 
     */
    
    public function addVideos($data){
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
    /**
     *
     *@param array $ids
     *@return int delete
     *@todo elimina todos los videos traidos
     */
    public function deleteVideos($ids){
    	$where = $this->getAdapter()->quoteInto('id IN (?)', $ids);
    	return $this->delete($where);
    }
            
    /**
     *
     *@param int $id (id de equipo)
     *@return fetchAll (listado de fotos)
     *@todo traemos los videos 
     */
    
    public function getVideos($id){        
    	$select = $this->select();    	
    	$data=array('a.id','a.nombre',"a.ruta");
        
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("a.id_equipos =".(int)$id);
        
    	return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param int $id
     *@return fetchRow (un solo registro)
     *@todo traemos un solo video
     */
    
    public function getVideo($id){        
    	$select = $this->select();    	
    	$data=array('a.id','a.nombre');
        
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("a.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
}

