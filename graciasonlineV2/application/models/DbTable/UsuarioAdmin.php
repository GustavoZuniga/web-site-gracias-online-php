<?php

class Application_Model_DbTable_UsuarioAdmin extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_admin';
   
    public function getListaUsuariosAdmin(){    	
        $select = $this->select();    

        $data=array('u.id','u.username','u.password','u.nombres','u.apellidos','u.email','u.estado'
                    );
        $select->order("id desc");
        $select->from(array("u" => $this->_name),$data);
        //$select->where('u.estado = 1');
        
        return $this->fetchAll($select)->toArray();
    }
    
    public function addUsuario($data){        
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
    
    public function getUsuariosAdmin($id){    	
        $select = $this->select();        
        $data=array('u.id','u.username','u.password','u.nombres','u.apellidos','u.email','u.estado'
                    );
        $select->from(array("u" => $this->_name),$data);
        $select->where("u.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
    
    public function updateUsuarioAdmin($data){
            $id = $data['id'];     
            return $this->update($data, 'id = '. (int)$id);
    }
    
    public function activarUsuarioAdmin($id,$act){        
        $data['estado'] = $act;
        return $this->update($data, 'id = '. (int)$id);
    }
    
    public function deleteUsuarioAdmin($id){
            $this->delete('id =' . (int)$id);
    }
}

