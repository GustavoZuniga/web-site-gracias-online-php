<?php

class Application_Model_DbTable_EventosFotos extends Zend_Db_Table_Abstract
{

    protected $_name = 'eventos_fotos';

    /**
     *
     *@param array $data
     *@return row (1 o 0)
     *@todo grabar una foto del evento
     */
    public function addFotos($data){        
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
    /**
     *
     *@param array $ids
     *@return int delete
     *@todo elimina todas las fotos
     */
    public function deleteFotos($ids){
        $where = $this->getAdapter()->quoteInto('id IN (?)', $ids);
        return $this->delete($where);
    }
    
    /**
     *
     *@param int $idEvento
     *@return fetchAll (listado de fotos)
     *@todo traemos las fotos de un eventos
     */
    
    public function getFotos($id){        
    	$select = $this->select();    	
    	$data=array('a.id','a.nombre',"a.ruta");
        
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("a.id_eventos =".$id);
        
    	return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param array $ids
     *@return fetchAll (listado de fotos)
     *@todo traemos las fotos solo de los $ids ejem->(1,3,5) para borrar las fotos fisicamente
     */
    
    public function getGrupoFotos($ids){
    	$select = $this->select();
    	$data=array('a.id','a.nombre',"a.ruta");
    
    	$select->from(array("a" => $this->_name),$data);
    	$select->where('a.id IN (?)', $ids);
    
    	return $this->fetchAll($select)->toArray();
    }
    
}

