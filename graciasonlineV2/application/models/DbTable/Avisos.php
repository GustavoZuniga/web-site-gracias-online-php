<?php

class Application_Model_DbTable_Avisos extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_aviso';
    
    /**
     *
     * @param array $data
     * @return  int row (Confirmacion de haber grabado)
     * @todo inserta un nuevo aviso
     */
    public function addAvisos($data){        
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
    
    /**
     *
     * @param array $data
     * @return  update (Confirmacion de haber grabado)
     * @todo modifica un aviso
     */
    public function updateAvisos($data,$id){    	
    	return $this->update($data, 'id = '. (int)$id);
    }
    
    /**
     *
     * @param int $id
     * @return  fetchRow (confirmacion de que existe)
     * @todo trae un aviso
     */
    public function getAvisos($id){    	
        $select = $this->select();        
        $data=array('a.id','a.titulo','a.descripcion','a.institucion','a.ocultar','a.departamento',
                    'a.provincia','a.distrito','a.email','a.fecharegistro','a.id_disciplinas'
                    );
        $select->from(array("a" => $this->_name),$data);
        $select->where("a.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
    
    /**
     *     
     * @return  fetchAll (trae el listado en array)
     * @todo trae el listado de avisos
     */
    public function getListadoAvisos(){ 
        $data=array('a.id','a.titulo','a.descripcion','a.institucion','a.ocultar','a.departamento',
                    'a.provincia','a.distrito','a.email','a.fecharegistro','a.estado','a.id_disciplinas'
                            );
        
        $dataDisciplinas=array('b.nombre as disciplina');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("a" => $this->_name), $data);
    	$select->joinLeft(array('b' => 'disciplinas'), 'b.id = a.id_disciplinas', $dataDisciplinas);
        $select->order("a.id desc");
        
        return $this->fetchAll($select)->toArray();
       
       
    }
    
    /**
     * @param int $id
     * @return update (indica si elimino el aviso)
     * @todo eliminamos el aviso(colocamos en estado a 2 )
     */
    
    public function deleteAvisos($data,$id){        
        return $this->update($data, 'id = '. (int)$id);
    }
    
    public function borrarAvisos($data,$id){
        $this->delete('id =' . (int)$id);
    }
    
    public function activarAviso($id,$act){    	
        $data['estado'] = $act;
        return $this->update($data, 'id = '. (int)$id);
    }
    
    /**
     * @param array $consulta (sltDisciplina,sltDepartamento,sltFecha)    
     * @return  fetchAll (trae el listado en array)
     * @todo Listamos los avisos por criterio de busqueda
     */
    public function buscar($consulta){
        
        $dataAvisos=array('a.id','a.titulo','a.descripcion','a.institucion','a.ocultar','a.departamento',
                    'a.provincia','a.distrito','a.email','a.fecharegistro','a.estado','a.id_disciplinas'
                    );
        
        $dataDisciplinas=array('b.nombre as disciplina');
        
        $select = $this->select()->setIntegrityCheck(false);
    	$select->from(array("a" => $this->_name), $dataAvisos);
    	$select->joinLeft(array('b' => 'disciplinas'), 'b.id = a.id_disciplinas', $dataDisciplinas);
                
        if($consulta['sltDisciplina']!='cualquiera'){
            $select->where('a.id_disciplinas='.(int)$consulta['sltDisciplina']);
        }
        
        if($consulta['sltDepartamento']!='cualquiera'){
            $select->where('a.departamento LIKE "%'.$consulta['sltDepartamento'].'%"');
        }
        
        if($consulta['sltFecha']!='cualquiera'){
            //fecha n dias atras hasta la fecha actual
            $select->where('a.fecharegistro BETWEEN DATE(DATE_ADD(NOW(), INTERVAL -'.(int)$consulta['sltFecha'].' DAY)) AND CURDATE()');
        }
        
        $select->order("a.fecharegistro desc");
        
        return $this->fetchAll($select)->toArray();
    }

    
}

