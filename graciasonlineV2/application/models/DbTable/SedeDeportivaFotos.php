<?php
/**
 * @autor=Dimas Gustavo 
 *
 */
class Application_Model_DbTable_SedeDeportivaFotos extends Zend_Db_Table_Abstract
{

    protected $_name = 'sede_deportiva_fotos';
    
    /**
     *
     *@param array $data
     *@return row (1 o 0)
     *@todo grabar una foto del evento
     */
    public function addFotos($data){        
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
    /**
     *
     *@param array $ids
     *@return int delete
     *@todo elimina todas las fotos
     */
    public function deleteFotos($ids){
        $where = $this->getAdapter()->quoteInto('id IN (?)', $ids);
        return $this->delete($where);
    }

    /**
     *
     *@param array $ids
     *@return fetchAll (listado de fotos)
     *@todo traemos las fotos solo de los $ids ejem->(1,3,5) para borrar las fotos fisicamente
     */
    
    public function getGrupoFotos($ids){
    	$select = $this->select();
    	$data=array('a.id','a.nombre',"a.ruta");
    
    	$select->from(array("a" => $this->_name),$data);
    	$select->where('a.id IN (?)', $ids);
    
    	return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param int $id
     *@return fetchAll (listado de fotos)
     *@todo traemos las fotos de sedes deportivas
     */
    public function getFotos($id){
    	$select=$this->select();
    	$data=array('a.id','a.nombre',"a.ruta");
    	
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("a.id_sededeportiva =".(int)$id);
    	
    	return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param int $id,text $name
     *@return fetchRow (1,0)
     *@todo si todabia existe la foto de inicio 
     */
    public function existeFoto($name,$id){
    	$select=$this->select();
    	
    	$select->from(array("a" => $this->_name),array("count(a.id) as count"));
    	$select->where("a.id_sededeportiva =".(int)$id);
    	$select->where("a.nombre ='".$name."'");
    	
    	return $this->fetchRow($select)->toArray();
    }
}

