<?php
/**
 * @autor=Dimas Gustavo
 * @todo Solo es el ubigeo de Peru
 *
 */
class Application_Model_DbTable_Ubigeo extends Zend_Db_Table_Abstract
{
	CONST DEPARTAMENTO_INICIAL='LIMA';
	CONST PROVINCIA_INICIAL='LIMA';
	CONST DISTRITO_INICIAL='LIMA';
	
    protected $_name = 'ubigeo';
    
    /**
     *     
     *@return array fetchAll(listado de registros)
     *@todo listado de departamentos del peru
     */
   public function getDepartamentos(){    	
    	$select = $this->select();
    	$select->from(array("a" => $this->_name),array('a.departamento'));
    	$select->group('a.departamento');
    	$select->order("a.departamento ASC");        
    	return $this->fetchAll($select)->toArray();
    }

    /**
     *
     * @param string $departamento
     * @return  fetchAll (varios registros)
     * @todo listado de provincias de un departamento x
     */
    public function getProvincias($departamento){  		
            $select = $this->select();  		
            $select->from(array("a" => $this->_name),array('a.provincia'));
            $select->where("a.departamento ='".$departamento."'");
            $select->group('a.provincia');
            $select->order("a.provincia ASC");
            
            return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     * @param string $provincia
     * @return  fetchAll (varios registros)
     * @todo listado de distritos  de una provincia x
     */
    public function getDistritos($provincia){  		
            $select = $this->select();  		
            $select->from(array("a" => $this->_name),array('a.distrito'));
            $select->where("a.provincia ='".$provincia."'");
            $select->group('a.distrito');
            $select->order("a.distrito ASC");                
            return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     * @param string $departamento,$provincia,$distrito
     * @return  fetchRow (un solo registro)
     * @todo obtenemos el id de la tabla ubigeo, tras hacer un post en registro evento
     */
    public function getUbicasion($departamento,$provincia,$distrito){
        $select = $this->select();  		
        $select->from(array("a" => $this->_name),array('a.id'));
        $select->where("a.departamento ='".$departamento."'");
        $select->where("a.provincia ='".$provincia."'");
        $select->where("a.distrito ='".$distrito."'");
        return $this->fetchRow($select)->toArray();        
    }
    
    /**
     *
     * @param int $id
     * @return  fetchRow (un solo registro)
     * @todo obtenermos el registro del ubigeo por id
     */
    public function getUbigeo($id){
        $select = $this->select();  		
        $select->from(array("a" => $this->_name),array('a.departamento','a.provincia','a.distrito'));
        $select->where("a.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }

}

