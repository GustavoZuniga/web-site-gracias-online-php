<?php

class Application_Model_DbTable_Estudiante extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_usuario';
    
	/**
     *
     * @param ninguno
     * @return  lisado de estudiantes
     * @
     */
	public function getAll(){    	
        $data=array('p.id','p.username','p.password','p.email','p.estado','p.firstname',
					'p.lastname','p.rol');
		
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("id desc");
        $select->from(array("p" => $this->_name),$data);
		$select->where("p.rol = 1");// ROL DEL ESTUDIANTE = 1
        
        return $this->fetchAll($select)->toArray();
    }
	
	/**
     *
     * @param id del productos
	 * @param id estado 1 o 0
     * @return 
     * @
     */
	public function cambiarEstado($data){  	
        $id = $data['id'];  	
        return $this->update($data, 'id = '. (int)$id);
    }
	
	
	
	
	
	
	
	
	
	
    /**
     *
     * @param array $data
     * @return  int $resp (Confirmacion de haber grabado)
     * @todo graba los datos de registro de noticia
     */
    public function addRegistro($data){ 
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
	
	/**
     *
     * @param ninguno
     * @return  listado de productos en estado ACTIVO
     * @
     */
	public function getProductosActivos(){
	
		$data=array('p.pro_id','p.pro_nombre','p.pro_precio','p.pro_estado');
 
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("pro_id asc");
        $select->from(array("p" => $this->_name),$data);
		$select->where("p.pro_estado = 1");
        
        return $this->fetchAll($select)->toArray();
	}

    
    /**
     *
     * @param $id
     * @return  int 
     * @todo Obtiene el detalle de un producto por id
     */
    public function getProductoById($id){
      
        $data = array('p.pro_id','p.pro_nombre','p.pro_precio','p.pro_estado');
		
        $dataImagen = array('i.img_nombre','i.img_extension','i.img_entidad_id');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->from(array("p" => $this->_name),$data);
        $select->joinLeft(array("i" => 'cms_imagen_entidad'),'i.img_entidad_id = p.pro_id',$dataImagen);
        $select->where("p.pro_id =".(int)$id);
		
        return $this->fetchRow($select)->toArray();
		
    }
	/**
     *
     * @param array $data
     * @return  int $resp (Confirmacion de haber grabado)
     * @todo actualiza el registro por id
     */
	public function updateRegistro($data){
            $id = $data['pro_id'];    
            return $this->update($data, 'pro_id = '. (int)$id);
    }
	
	
	
	
    
    public function addNoticia($titulo, $descripcion){
		$data = array('titulo' => $titulo, 'descripcion' => $descripcion,);
		$this->insert($data);
    }

    public function updateNoticia($data){
            $id = $data['id'];     
            return $this->update($data, 'id = '. (int)$id);
    }

    public function deleteNoticia($id){
            $this->delete('id =' . (int)$id);
    }
 
    
    /**
     *     
     * @return  fetchAll (trae el listado en array)
     * @todo trae el listado de noticias seccion aside
     */
    
     public function getWigetNoticias(){ 

        $data=array('n.id','n.titulo','n.resumen','n.descripcion'
                    ,'n.fecha_registro');
        
        $dataImagen = array('i.noticia_id','i.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("id desc");
        $select->limit(3);
        $select->from(array("n" => $this->_name),$data);
        $select->joinLeft(array("i" => 'cms_noticia_imagen'),'i.noticia_id = n.id',$dataImagen);
        
        return $this->fetchAll($select)->toArray();
    }
    
    public function activarNoticia($id,$act){    	
        $data['estado'] = $act;
        return $this->update($data, 'id = '. (int)$id);
    }
    
    public function buscar($consulta){
        
        $data=array('n.id','n.titulo','n.resumen','n.descripcion','n.fecha_registro'
                    );
        
        $dataImagen = array('i.noticia_id','i.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("id desc");
        $select->from(array("n" => $this->_name),$data);
        $select->join(array("i" => 'cms_noticia_imagen'),'i.noticia_id = n.id',$dataImagen);
        
        $select->where('titulo LIKE "%'.$consulta.'%"');
        
        return $this->fetchAll($select)->toArray();
    }
    
    /**
     * @author Dimas Gustavo  
     * @param int $id (id de Federacion)   
     * @return  fetchAll (trae el listado en array)
     * @todo Traemos todas las noticias de una federacion federaciones
     */
    public function getNoticasDeFederacion($id){    	
        $data=array('n.id','n.titulo','n.resumen','n.descripcion'
                    ,'n.fecha_registro'
                    ,'n.estado'
                    );
        
        $dataImagen = array('i.noticia_id','i.nombre');
        
        $select = $this->select()->setIntegrityCheck(false); 
        $select->order("id desc");
        $select->from(array("n" => $this->_name),$data);
        $select->joinLeft(array("i" => 'cms_noticia_imagen'),'i.noticia_id = n.id',$dataImagen);
        $select->where('n.id_federaciones='.(int)$id);
        
        return $this->fetchAll($select)->toArray();
    }
                
}

