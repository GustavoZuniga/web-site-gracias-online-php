<?php

class Application_Model_DbTable_Federaciones extends Zend_Db_Table_Abstract
{

    protected $_name = 'federaciones';
    CONST ACTIVO=1;
    CONST INACTIVO=0;
    CONST ELIMINADO=0;
    
    /**
     * @author Dimas Gustavo     
     * @return  fetchAll (trae el listado en array)
     * @todo Traemos todas las federaciones
     */ 
    
    public function getListaFederaciones($id=0){
        $dataFederaciones=array('a.id','a.ubigeo','a.nombre','a.direccion'
                                ,'a.telefono','a.web','a.email','a.resolucion'
                                ,'a.presidente','a.vicepresidente','a.secretario'
                                ,'a.tesorero','a.vocal','a.logo');
        
        $select = $this->select();
        $select->from(array('a'=>$this->_name),$dataFederaciones);
        $select->where('a.estado='.self::ACTIVO);
        return $this->fetchAll($select)->toArray();
    }
    
    /**
     * @author Dimas Gustavo
     * @param int $id (id de Federacion)
     * @return  fetchRow (registro)
     * @todo traemos el registro de la Federacion
     */
    
    public function getFederacion($id){
    	$select = $this->select();
    	
    	$data=array('a.id','a.ubigeo','a.nombre','a.direccion'
                    ,'a.telefono','a.web','a.email','a.resolucion'
                    ,'a.presidente','a.vicepresidente','a.secretario'
                    ,'a.tesorero','a.vocal','a.logo'
                    ,'a.facebook','a.twitter');
    	
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("a.id =".(int)$id);
    	
    	return $this->fetchRow($select)->toArray();
    }
    
    /**
     * @autor Dimas Gustavo
     * @param type $data (array)
     * @todo actualizamos los datos de la tabla
     */
    public function updateFederacion($data,$id){                
            return $this->update($data, 'id = '. (int)$id);
    }
    
    /**
     *
     * @param array $data
     * @return  int save (1,0)
     * @todo graba los datos de la federacion
     */
    public function addFederaciones($data){
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
    
    /**
     *
     *@param int $id (id del evento)
     *@return update (respuesta de cambio)
     *@todo eliminamos una federacion (es un cambio de estado a valor 2)
     */
    public function deleteFederacion($data,$id){        
        return $this->update($data, 'id = '. (int)$id);
    }
    
}

