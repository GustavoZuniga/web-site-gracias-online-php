<?php

class Application_Model_DbTable_Eventos extends Zend_Db_Table_Abstract
{
    CONST ESTADO_ACTIVO=1;
    CONST ESTADO_ELIMINADO=2;
    CONST CREO_USUARIO='usuario';
    CONST CREO_ADMINISTRADOR='administrador';
    
    protected $_name = 'eventos';

    /**
     *
     *@param string $mesActual
     *@return array toArray(listado de registros)
     *@todo listado de eventos del mes
     */
    public function getEventosMes($mesActual){//modificasion(tavo)   
		
    	$select = $this->select();    	
    	$data=array('a.id','a.nombre_evento','a.descripcion'
                    ,'a.quien_creo'
                    ,"date_format(a.fecha_inicio, '%Y-%m-%d') as fecha_inicio"
                    ,"date_format(a.fecha_fin, '%Y-%m-%d') as fecha_fin");
        
        
    	$select->from(array("a" => $this->_name),$data);
    	$select->where("date_format(a.fecha_inicio,'%m/%Y') ='".$mesActual."'");
    	$select->where("a.estado = ?", self::ESTADO_ACTIVO);
    	$select->order("a.fecha_inicio ASC");
        
        
        /*$select->from(array("a" => $this->_name),$data);
    	$select->where("'".$mesActual."' BETWEEN date_format(a.fecha_inicio, '%m/%Y') AND date_format(a.fecha_fin, '%m/%Y')");
    	$select->where("a.estado = ?", self::ESTADO_ACTIVO);
    	$select->order("a.fecha_inicio ASC");*/
        var_dump($data);exit();
    	return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param array $data
     *@return row (1 o 0)
     *@todo grabar un evento
     */
    public function addEventos($data){        
    	$row=$this->createRow();
    	$row->setFromArray($data);
    	return $row->save();
    }
    
    /**
     *
     * @param array $data
     * @return  update (Confirmacion de haber grabado)
     * @todo modificar un evento
     */
    public function updateEventos($data,$id){    	
    	return $this->update($data, 'id = '. (int)$id);
    }
    
    /**
     *
     *@param int $id (id del usuario)
     *@return fetchAll (registros de eventos)
     *@todo lista de eventos creados del usuario(es utilizado en el backend por "")
     */
    public function listadoEventos($id,$tipo='frontend'){        
    	$select = $this->select();    	
    	$data=array('a.id','a.nombre_evento',"date_format(a.hora, '%H:%i:%s') as hora"
                    ,"date_format(a.fecha_inicio, '%d-%m-%Y') as fecha_inicio"
                    ,"date_format(a.fecha_fin, '%d-%m-%Y') as fecha_fin");
        
    	$select->from(array("a" => $this->_name),$data);
        
        //if($tipo=='frontend'):
            $select->where("a.usuarioregistro =".$id);
        //endif;    	
        
    	$select->where("a.estado = ?", self::ESTADO_ACTIVO);
    	$select->order("a.fecha_inicio DESC");
        
    	return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param int $id (id del evento)
     *@return update (respuesta de cambio)
     *@todo eliminamos un evento (es un cambio de estado a valor 2)
     */
    public function deleteEvento($data,$id){        
        return $this->update($data, 'id = '. (int)$id);
    }
    
    /**
     *
     *@param int $id (id del evento)
     *@return fetchRow (respuesta de cambio)
     *@todo traemos el evento por id
     */
    public function getEvento($id){        
        $select = $this->select();
        
        $data=array('a.id','a.nombre_evento','a.descripcion'
                    ,'a.lugar','a.ubigeo',"date_format(a.fecha_inicio, '%d/%m/%Y') as fecha_inicio"
                    ,"date_format(a.fecha_fin, '%d/%m/%Y') as fecha_fin"
                    ,"date_format(a.hora, '%H:%i:%s') as hora",'a.mapa_latitud','a.mapa_longitud'
                    ,'a.email','a.telefono','a.twitter','a.facebook'
                   );
        
        $select->from(array("a" => $this->_name),$data);
        $select->where("a.id =".(int)$id);  		
        
        return $this->fetchRow($select)->toArray();
    }
    
    /**
     *
     *@param datetime $fecha 
     *@return array fetchAll 
     *@todo traemos los eventos del dia
     */
    public function getEventoDia($fecha){        
        $select = $this->select();
        
        $data=array('a.id','a.nombre_evento','a.descripcion'
                    ,'a.lugar','a.ubigeo'
                    ,"date_format(a.fecha_inicio, '%d-%m-%Y') as fecha_inicio"
                    ,"date_format(a.fecha_fin, '%d-%m-%Y') as fecha_fin"
                    ,"date_format(a.hora, '%H:%i:%s') as hora");
        
        $select->from(array("a" => $this->_name),$data);        
        $select->where("'".$fecha."' BETWEEN date_format(a.fecha_inicio, '%m/%Y') AND date_format(a.fecha_fin, '%m/%Y')");
    	$select->where("a.estado = ?", self::ESTADO_ACTIVO);
        $select->order("a.hora ASC");
        
        return $this->fetchAll($select)->toArray();
    }
    
    /**
     *
     *@param datetime $fecha,datetime $hora 
     *@return array fetchAll 
     *@todo traemos los eventos bigentes desde la hora actual hacia adelante 
     */
    public function getEventosVigentes(){        
        $select = $this->select();
        
        $data=array('a.id','a.nombre_evento');
        
        $select->from(array("a" => $this->_name),$data);
        $select->where("date_format(a.fecha_inicio, '%Y-%m-%d') >=CURDATE()");
        $select->order("a.fecha_inicio ASC");
        $select->order("a.hora ASC");
        return $this->fetchAll($select)->toArray();
    }
    
}

