<?php

class Application_Model_DbTable_Usuario extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_usuario';
   
    public function addUsuario($data){
        
    	$row=$this->createRow();
    	$row->setFromArray($data);        
    	return $row->save();
    }
	
	public function getInfoUsuario($id){  
 	
		$data=array('u.id','u.username','u.firstname',
					'u.lastname','u.email','u.celular');
	
        $select = $this->select();      
        $select->from(array("u" => $this->_name),$data);
        $select->where("u.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
    
    public function getUsuarios($id){  
 	
	$data=array('u.id','u.username','u.firstname',
					'u.lastname','u.email','u.celular');
	
        $select = $this->select();        
        
					
        $select->from(array("u" => $this->_name),$data);
        $select->where("u.id =".(int)$id);
        
        return $this->fetchRow($select)->toArray();
    }
    
    public function updateUsuario($data){
            $id = $data['id'];     
            return $this->update($data, 'id = '. (int)$id);
    }
    
    public function deleteUsuario($id){
            $this->delete('id =' . (int)$id);
    }
}

