//ACTUALMENTE YA NO TRABAJA HA SIDO REEMPLAZADO POR PURO CODIGO PHP(pero esto servira cuando se trabaje con ajax)
$(document).on('ready',function() {//asi es ahora con html5
                
        $('#frmBuscarSedesDeportivas').keypress(function(e){    
            if(e == 13){ return false; } 
        });        
        
        //UBIGEO
        $('#departamento').on('change',function(){
                    departamento($(this),$('#provincia'),$('#distrito'));
        });

        $('#provincia').on('change',function(){
                    provincia($(this),$('#distrito'));                    
        });
        
        $('.pagination-anterior').on('click',function(){        	
        	
        	var ubigeo=$.trim($('#frmBuscarSedesDeportivas').attr('ubigeo'));
        	
        	if(ubigeo){
        		var page=parseInt($('#frmBuscarSedesDeportivas').attr('page'))-1;
        		var action=$.trim($('#frmBuscarSedesDeportivas').attr('action'));
        		$('#frmBuscarSedesDeportivas').attr('action',action+'index/ubigeo/'+ubigeo+'/page/'+page);
        		$('#btnBuscar').click();
        		return false;
        	}
        	        
        });
        
        $('.pagination-siguiente').on('click',function(){        	
        	
        	var ubigeo=$.trim($('#frmBuscarSedesDeportivas').attr('ubigeo'));
        	
        	if(ubigeo){
        		var page=parseInt($('#frmBuscarSedesDeportivas').attr('page'));
        		if(page==0){
        			page=page+2
        		}else{
        			page=page+1
        		}
        		
        		var action=$.trim($('#frmBuscarSedesDeportivas').attr('action'));
        		$('#frmBuscarSedesDeportivas').attr('action',action+'index/ubigeo/'+ubigeo+'/page/'+page);
        		$('#btnBuscar').click();
        		return false;
        	}
        	        
        });
        
        $('#pagination > div > a').on('click',function(){        	
        	
        	var ubigeo=$.trim($('#frmBuscarSedesDeportivas').attr('ubigeo'));
        	
        	if(ubigeo){
        		var page=$.trim($(this).text());        		
        		var action=$.trim($('#frmBuscarSedesDeportivas').attr('action'));
        		$('#frmBuscarSedesDeportivas').attr('action',action+'index/ubigeo/'+ubigeo+'/page/'+page);
        		$('#btnBuscar').click();
        		return false;
        	}
        	        
        });
        
});

