$(document).on('ready',function() {//asi es ahora con html5
        
        function validaCamposFecha(fechaini,fechafin){//valida campos de fecha
           
                var validaFecha = false;

                if (fechaini == '' || fechafin == '') {
                    validaFecha = false;
                } else {

                    var fechaIniVal = fechaini;
                    var fechaFinVal = fechafin;

                    var inicio = fechaIniVal.split("/");
                    var fin = fechaFinVal.split("/");

                    if (fin[2] >= inicio[2]) {
                        if(fin[1] >= inicio[1]){
                            if(fin[0] < inicio[0]){
                               validaFecha = false;
                            }else {
                               validaFecha = true;
                            }    
                        }else{
                           validaFecha = false;
                        }
                    }else{
                        validaFecha = false;
                    }         
                }
                
                return validaFecha;
        }
    
    
        $('#frmRegistrarEventos').keypress(function(e){    
            if(e == 13){ return false; } 
        });
 				
        $('#btnRegistrarEvento').on("click",function(e){

                           var nombre =$.trim($("#txtNombreEvento").val());
                           if(nombre == "" || nombre == "nombre"){				     	
                                $('#error-nombre').addClass('mostrar');
                                $("#txtNombreEvento").focus();				     	
                                return false;
                            }

                            var descripcion = $.trim($("#textDescripcion").val());				    
                                if(descripcion == "" || descripcion == "descripcion"){				     	
                                $('#error-descripcion').addClass('mostrar');
                                $("#textDescripcion").focus();				     	
                                return false;
                            }

                            var lugar = $.trim($("#txtLugar").val());
                            if(lugar == "" || lugar == "lugar" ){				     	
                                $('#error-lugar').addClass('mostrar');
                                $("#txtLugar").focus();				     	
                                return false;
                            }

                            var departamento = $.trim($("#departamento").val());
                            if(departamento == "" || departamento== "departamento"){				     	
                                $('#error-departamento').addClass('mostrar');
                                $("#departamento").focus();				     	
                                return false;
                            }


                            var fechaInicio = $.trim($("#txtFechaInicio").val());

                            if(fechaInicio == "" || fechaInicio == "fecha" || fechaInicio == "01/01/1990"){				     	
                                $('#error-fecha-inicio').addClass('mostrar');
                                $("#txtFechaInicio").focus();				     	
                                return false;
                            }
                            
                            var fechaFin = $.trim($("#txtFechaFin").val());

                            if(fechaFin == "" || fechaFin == "fecha" || fechaFin == "01/01/1990"){				     	
                                $('#error-fecha-fin').addClass('mostrar');
                                $("#txtFechaFin").focus();				     	
                                return false;
                            }
                            
                            //validamos si la segunda fecha es >= ala primera fecha
                            if(validaCamposFecha(fechaInicio,fechaFin)==false){
                                $('#error-fecha-fin').addClass('mostrar');
                                $('#error-fecha-fin').text('Tiene que ser ">=" a la Fecha Inicio');
                                $("#txtFechaFin").focus();
                                return false;                            
                            }
                            
                            
                            deleteFileVacios();

        });
		
	//SUBIR FOTOS	
        $('#subir-fotos').on('click',function(e){
                subirFotos('#content-sf');
                return false;
        });
        
        //ELIMINAMOS FOTOS
        $('#content-sf p a').on('click',function(e){                        
                var id=$(this).parent().attr('id').split('-');                        
                deleteFotos(id[1]);
                return false;
        });
        
        //ELIMINAMOS VIDEOS
        $('#content-sv p a').on('click',function(e){
                var id=$(this).parent().attr('id').split('-');                        
                deleteVideos(id[1]);
                return false;
        });
        
        $('#subir-videos').on('click',function(e){
        	subirVideos('#content-sv');
            return false;
        });
        
        //UBIGEO
        $('#departamento').on('change',function(){
                    departamento($(this),$('#provincia'),$('#distrito'));
        });

        $('#provincia').on('change',function(){
                    provincia($(this),$('#distrito'));                    
        });     
                
                
});

