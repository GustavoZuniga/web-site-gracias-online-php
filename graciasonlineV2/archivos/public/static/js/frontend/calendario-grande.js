$(document).on('ready',function() {//asi es ahora con html5
		function clickDiaGrande(obj){
				//return false;//temporalmente hasta definir bien las cosas()
                                
				if(!obj.attr('tit')){
					$('.content-aviso-calendario-grande').css('display','none');
					return false;
				}
				
				$('.content-aviso-calendario-grande').css('display','inherit');								
								
				var left=obj.position().left+110;
				var top=obj.position().top-30;
				
				$('.content-aviso-calendario-grande').css({'left':left+'px','top':top+'px'});
				
				var titulo=obj.attr('tit').split('/////');
				var id=obj.attr('id').split('-');
                                    //-------------------------------------------------------------------                               
                                    var eventos="";
                                    var urlDetalle=$('#nav-calendario-grande').attr('actionDetalle')+'/id/';

                                     $.each(id, function(i,row){
                                         eventos+='<li><a href="'+(urlDetalle+id[i])+'">'+titulo[i]+'</a></li>';
                                     });

                                     $('.content-aviso-calendario-grande ul').html(eventos);

                                     if(parseInt(eventos.length)>0){
                                         var year=$('#nav-calendario-grande').attr('year');
                                         var mes=$('#nav-calendario-grande').attr('month');
                                         var dia=$.trim(obj.children('span').text());


                                         if(parseInt(dia)<10){
                                             dia='0'+parseInt(dia);
                                         }

                                         if(parseInt(mes)<10){
                                             mes='0'+parseInt(mes);
                                         }


                                         var urlListado=$('#nav-calendario-grande').attr('actionEvento')+'/fecha/'+dia+'-'+mes+'-'+year;    
                                         var objA='<a href="'+urlListado+'">Ver todos...</a>';

                                         $('.content-aviso-calendario-grande > .cbody > a:first').remove();
                                         $('.content-aviso-calendario-grande ul').after(objA);
                                     }
                                    //-------------------------------------------------------------------
		}
		
		
		
		$('#nav-calendario-grande > dl >dd').on("click", function(event){
				clickDiaGrande($(this)); 
				return false;
		});
                
                $('#nav-calendario-grande > dl >dd >p>a').on("click", function(event){
                    var url=$(this).attr('href');
                    $(location).attr('href',url);
                    return false;
                });
		
		$('.btnCalendarioGrandeIzquierda').on('click',function(event){
			var year=$('#nav-calendario-grande').attr('year');
			var month=$('#nav-calendario-grande').attr('month');				
			
			month=parseInt(month)-1;
			
			if(month > 0 && month<10){
				month='0'+month;
			}
			
			if(month < 1){
				year=parseInt(year)-1;
				month='12';					
			}			
			
			getEventosGrandes(year,month);
			
			return false;
		});
		
		$('.btnCalendarioGrandeDerecha').on('click',function(event){			
				var year=$('#nav-calendario-grande').attr('year');
				var month=$('#nav-calendario-grande').attr('month');				
				
				month=parseInt(month)+1;
				
				if(month<10){
					month='0'+month;
				}
				
				if(month >12){
					year=parseInt(year)+1;
					month='01';					
				}			
				
				getEventosGrandes(year,month);
			return false;
		});
		
		function getEventosGrandes(year,month){//Funcion que trae eventos
				var action=$('#nav-calendario-grande').attr('data-source');
				var dataString='year='+year+'&month='+month;
				
				$.ajax({
                                        async: true,  
                                        dataType: 'json',  
                                        type: 'POST',
                                        contentType: 'application/x-www-form-urlencoded',  
                                        url: action,  
                                        data: dataString,  
                                        beforeSend: function(data){  

                                        },
                                        success: function(requestData){
                                                        $('.content-aviso-calendario-grande').css('display','none');

                                                        $('#nav-calendario-grande > dl').remove();

                                                        var calendario="";
                                                        var num=1;
                                                        var dia=1;

                                                        var diasMes=requestData['diasMes'];
                                                        var diaSemana=requestData['diaSemana'];
                                                        var nuevoCss="";

                                                        if((parseInt(diaSemana)==7 && diasMes>=30 ) || (parseInt(diaSemana)==6 && diasMes==31) ){nuevoCss=$('#nav-calendario-grande').attr('css');}

                                                        $.each(requestData['eventos'], function(i,row){	                				
                                                                        //guardo sus datos en un texto	                				
                                                                        if(num==1){
                                                                                calendario+='<dl>';
                                                                        }

                                                                         for(var s=0;dia<diaSemana;s++){		                				 	
                                                                                        calendario+='<dd class="'+nuevoCss+'"></dd>';		                				
                                                                                        num=num+1;
                                                                                        dia=dia+1;
                                                                         }


                                                                         if(num <=6){	
                                                                                         if(row['existe']==1){ 			                					 	
                                                                                                 calendario=calendarioBloque(row,'normal',nuevoCss,calendario)
                                                                                         }else{
                                                                                                calendario+='<dd class="'+nuevoCss+'">';
                                                                                                               calendario+='<span>'+row['dia']+'</span>';
                                                                                                calendario+='</dd>';
                                                                                         }
                                                                         }



                                                                         if(num==7){
                                                                                         if(row['existe']==1){
                                                                                                 calendario=calendarioBloque(row,'domingo',nuevoCss,calendario)
                                                                                         }else{
                                                                                                         calendario+='<dd class="domingo '+nuevoCss+'">';                                                                                                                        
                                                                                                                        calendario+='<span>'+row['dia']+'</span>';
                                                                                                         calendario+='</dd>';
                                                                                         }
                                                                }

                                                                        if(num==7 || diasMes==num){
                                                                                calendario+='</dl>';
                                                                        num=0;
                                                                }

                                                                        num=num+1;                                                        		
                                                                        dia=dia+1;

                                                            });

                                                        $('#mesActualGrande').text(requestData['nombreMes']);

                                                        $('#nav-calendario-grande > .content-aviso-calendario-grande').after(calendario);
                                                            
                                                        $('#nav-calendario-grande > dl > dd').on({
                                                                        'click':function(){
                                                                                clickDiaGrande($(this));
                                                                                return false;
                                                                        }
                                                        });
                                                        
                                                        $('#nav-calendario-grande > dl > dd > p >a').on({
                                                                        'click':function(){
                                                                                var url=$(this).attr('href');
                                                                                $(location).attr('href',url);
                                                                                return false;
                                                                        }
                                                        });
                                                        

                                                        //actualizamos los atributos
                                                        $('#nav-calendario-grande').attr('year',year);
                                                        $('#nav-calendario-grande').attr('month',month);
                                                        $('#nav-calendario-grande').attr('diasMes',diasMes);
                                                        $('#nav-calendario-grande').attr('diaSemana',diaSemana);

                                        },  
                                        error: function(requestData, strError, strTipoError){  
                                                alert('Error ' + strTipoError +': ' + strError);  
                                        },  
                                        complete: function(requestData, exito){

                                        }  
                                    });
		}
		
		//sabemos que esto es lo mejor
		function calendarioBloque(row,diaActual,nuevoCss,calendario){
				    var countRow=0;
				    
				    $.each(row, function(i,registro){
                                         countRow++;   
                                    });
                                    //-------------------------------------------------------------------------
                                    //codigo temporal hasta hacer el poput de calendario grande
                                    var actionEvento=$('#nav-calendario-grande').attr('actionEvento');
                                    var year=$('#nav-calendario-grande').attr('year');
                                    var mes=$('#nav-calendario-grande').attr('month');
                                    //alert(mes);
                                    var dia=row['dia'];
                                    
                                    if(parseInt(dia)<10){
                                        dia='0'+dia;
                                    }
                                    
                                    //rebajamos el mes temporalmente en (-1) para que funcione correctamente
                                    if(parseInt(mes)===1){
                                        mes=13;
                                        year=year-1;
                                    }
                                    var newMes=parseInt(mes)-1;
                                    
                                    if(newMes<10){
                                        newMes='0'+newMes;
                                    }
                                    
                                    var urlEvento=actionEvento+'/fecha/'+dia+'-'+newMes+'-'+year;
                                    //-------------------------------------------------------------------------
                                    
                                    var cssQuienCreo="";
                                    
				    if(diaActual=='domingo'){
				    	nuevoCss=nuevoCss+" domingo";
				    }
                                    
					if((countRow - 2)==1){
                                                    if(row[0]['quien_creo']=='usuario'){
                                                        cssQuienCreo=' evento-usuario';
                                                    }else{
                                                        cssQuienCreo=' evento-admin';
                                                    }
                                                    
                                                    calendario+='<dd id="'+row[0]['id']+'" tit="'+row[0]['nombre_evento']+'" des="'+row[0]['descripcion']+'" class="'+nuevoCss+' '+cssQuienCreo+' evento">';                                                                    
                                                                    calendario+='<span>'+row['dia']+'</span>';
                                                                    calendario+='<p>';
                                                                        calendario+='<a href="'+urlEvento+'">'+row[0]['nombre_evento']+'</a>';
                                                                        calendario+='<a href="'+urlEvento+'">Ver más…</a>';
                                                                    calendario+='</p>';
                                                    calendario+='</dd>';
					}else{
					            
			                    var ids=0;
			                    var titulos="";
			                    var descripciones="";

			                    for(i=0;i<(countRow-2);i++){
                                                            if(row[i]['quien_creo']=='usuario'){
                                                                cssQuienCreo=' evento-usuario';
                                                            }else{
                                                                cssQuienCreo=' evento-admin';
                                                            }
                                                            
			                                    if(ids==0){
			                                        ids=row[i]['id'];
			                                    }else{
			                                        ids=ids+'-'+row[i]['id'];
			                                    }

			                                    if(titulos==""){
			                                    	titulos=row[i]['nombre_evento'];
			                                    }else{
			                                        titulos=titulos+'/////'+row[i]['nombre_evento'];
			                                    }

			                                    if(descripciones==""){
			                                        descripciones=row[i]['descripcion'];
			                                    }else{
			                                        descripciones=descripciones+'/////'+row[i]['descripcion'];
			                                    }
			                    }
					           
					           calendario+='<dd id="'+ids+'" tit="'+titulos+'" des="'+descripciones+'" class="'+nuevoCss+' '+cssQuienCreo+' evento">';					           			
                                                                        calendario+='<span>'+row['dia']+'</span>';
                                                                        calendario+='<p>';
                                                                            calendario+='<a href="'+urlEvento+'">'+row[0]['nombre_evento']+'</a>';
                                                                            calendario+='<a href="'+urlEvento+'">Ver más…</a>';
                                                                        calendario+='</p>';
					           calendario+='</dd>';
		
					}
					
					return calendario;
			
		}
});

