 $(document).on('ready',function() {//asi es ahora con html5	
 		
 		$('#frmContactos').keypress(function(e){    
		    if(e == 13){ return false; } 
		});
 		
                $("#btnContactos").on("click",function(e) { 
                    
				    var email = $.trim($("#txtEmail").val());
					if(email == "" || email == "e-mail"){
				     	$('#error-email').text('Falta colocar el e-mail.');
				     	$('#error-email').addClass('mostrar');
				     	$("#txtEmail").focus();				     	
				     	return false;
				    }
				    
				    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					if(!emailReg.test(email)){						
						$('#error-email').text('e-mail incorrecto');
						$('#error-email').addClass('mostrar');
                                                $("#txtEmail").focus();											
						return false;
					}
				    
				    var mensaje = $.trim($("#textDescripcion").val());
					if(mensaje == "" || mensaje == "mensaje"){
                                            //$('#error-descripcion').text('Falta ingresar el mensaje.');
                                            $('#error-descripcion').addClass('mostrar');
                                            $("#textDescripcion").focus();				     	
                                            return false;
                                        } 
                                    
                                    e.preventDefault();
				    var action=$('#frmContactos').attr('data-source');                                    
                                    var dataString=$('#frmContactos').serialize();
                                    
                                    $.ajax({
                                            async: true,  
                                            dataType: 'json',  
                                            type: 'POST',
                                            contentType: 'application/x-www-form-urlencoded',  
                                            url: action,  
                                            data: dataString,  
                                            beforeSend: function(data){  
                                                 
                                            },
                                            success: function(requestData){                                                
                                                    $('#msg-form').removeClass('note-green').removeClass('note-blue').removeClass('note-red').addClass('note-yellow');
                                                    $('#msg-form').html('<b>Nota</b>: Tu mensaje está siendo procesado, espere un momento.');
                                                if(requestData.resp==1){                                                    
                                                    $("#textDescripcion").attr('value','');
                                                    $("#txtEmail").attr('value','');
                                                    $("#txtEmail").focus();
                                                    $('#msg-form').removeClass('note-blue').removeClass('note-yellow').removeClass('note-red').addClass('note-green');
                                                    $('#msg-form').html('<b>Nota</b>: Tu mensaje ha sido enviado, estaremos respondiendo en la brevedad posible.');
                                                    $('#error-email').removeClass('mostrar');
                                                    $('#error-descripcion').removeClass('mostrar');
                                                }else{
                                                    $('#msg-form').removeClass('note-green').removeClass('note-blue').removeClass('note-yellow').addClass('note-red');
                                                    $('#msg-form').html('<b>Nota</b>: No se pudo enviar tu mensaje, inténtelo nuevamente más tarde.');
                                                }
                                                
                                            },  
                                            error: function(requestData, strError, strTipoError){  
                                                    alert('Error ' + strTipoError +': ' + strError);  
                                            },  
                                            complete: function(requestData, exito){

                                            }  
                                        });
                                        
                                        //return false;
		});
});

