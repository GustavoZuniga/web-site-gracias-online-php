$(document).on('ready',function() {	
 		
                $('#frmPostular').keypress(function(e){    
		    if(e == 13){ return false; } 
		});
                
                $('#txtNombresCompletoPo').keypress(function(e){    
		    $('#error-nombre').removeClass('mostrar');
		});
                
               
                $('#txtEmailPo').keypress(function(e){    
		    $('#error-email').removeClass('mostrar');
		});
                
                $('#txtCiudadPo').keypress(function(e){    
		   $('#error-ciudad').removeClass('mostrar');
		});
                
                $('#pais').keypress(function(e){    
		   $('#error-pais').removeClass('mostrar');
		});

		$("#btnPostular").on("click", function(e){
                                var nombre =$.trim($("#txtNombresCompletoPo").val());
					if(nombre == "" || nombre == "nombre completo"){				     	
				     	$('#error-nombre').addClass('mostrar');
				     	$("#txtNombresCompletoPo").focus();				     	
				     	return false;
				    }
				    
				    var email = $.trim($("#txtEmailPo").val());
					if(email == "" || email == "e-mail"){				     	
				     	$('#error-email').addClass('mostrar');
				     	$("#txtEmailPo").focus();				     	
				     	return false;
				    }
				    
				    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					if(!emailReg.test(email)){
						$('#error-email').addClass('mostrar');
						$('#error-email').text('e-mail incorrecto');
					    $("#txtEmailPo").focus();											
						return false;
					}

				    
				    var ciudad = $.trim($("#txtCiudadPo").val());
					if(ciudad == "" || ciudad == "ciudad"){				     	
				     	$('#error-ciudad').addClass('mostrar');
				     	$("#txtCiudadPo").focus();				     	
				     	return false;
				    }
                                        
					var pais= $.trim($("#pais").val());
					if(pais== ""){				     	
				     	$('#error-pais').addClass('mostrar');
				     	$("#pais").focus();				     	
				     	return false;
				    }
		});
				
});

