$(document).on('ready',function() {//asi es ahora con html5
		function clickDia(obj){
				return false;//temporalmente hasta definir bien las cosas()
                                
				if(!obj.attr('tit')){
					$('.content-aviso-calendario-grande').css('display','none');
					return false;
				}
				
				$('.content-aviso-calendario-grande').css('display','inherit');								
								
				var left=obj.position().left-100;
				var top=obj.position().top-80;
				
				$('.content-aviso-calendario-grande').css({'left':left+'px','top':top+'px'});
				
				var numero=$.trim(obj.text());								
				$('#num-msg-calendario').text(numero);
				
				var titulo=obj.attr('tit').split('/////');				
				var descripcion=obj.attr('des').split('/////');
				var id=obj.attr('id').split('-');
				
				//pendiente por desarrollar (si en el mismo dia tenemos mas de un evento,colocar el titulo del evento y en el siguiente titulo (mas(5))-->el titulo con el numero de eventos que falta mostrar, para que esto te dirig a una vista en particular)
				//-----------------------------------------------
				/*
				var nombreTitulo=titulo;				
				if(id.length>1){
					
				}
				*/
				//-----------------------------------------------
				
				$('.content-aviso-calendario-grande > .right > a').text(titulo[0]);
				var url=$('#nav-calendario-grande').attr('actionEvento')+'/id/'+id[0];
				$('.content-aviso-calendario-grande > .right > a').attr('href',url);
				$('.content-aviso-calendario-grande > .right > p').text(descripcion[0]);				
		}
		
		
		
		$('#nav-calendario-grande > dl >dd').on("click", function(event){                                
				//clickDia($(this)); //desabilitado temporalmente hasta hacer el poput calendario
				//return false;//desabilitado temporalmente hasta hacer el poput calendario
		});
		
		$('.btnCalendarioGrandeIzquierda').on('click',function(event){
			var year=$('#nav-calendario-grande').attr('year');
			var month=$('#nav-calendario-grande').attr('month');				
			
			month=parseInt(month)-1;
			
			if(month > 0 && month<10){
				month='0'+month;
			}
			
			if(month < 1){
				year=parseInt(year)-1;
				month='12';					
			}			
			
			getEventos(year,month);
			
			return false;
		});
		
		$('.btnCalendarioGrandeDerecha').on('click',function(event){			
				var year=$('#nav-calendario-grande').attr('year');
				var month=$('#nav-calendario-grande').attr('month');				
				
				month=parseInt(month)+1;
				
				if(month<10){
					month='0'+month;
				}
				
				if(month >12){
					year=parseInt(year)+1;
					month='01';					
				}			
				
				getEventos(year,month);
			return false;
		});
		
		function getEventos(year,month){//Funcion que trae eventos
				var action=$('#nav-calendario-grande').attr('data-source');
				var dataString='year='+year+'&month='+month;
				
				$.ajax({
                                        async: true,  
                                        dataType: 'json',  
                                        type: 'POST',
                                        contentType: 'application/x-www-form-urlencoded',  
                                        url: action,  
                                        data: dataString,  
                                        beforeSend: function(data){  

                                        },
                                        success: function(requestData){
                                                        $('.content-aviso-calendario-grande').css('display','none');

                                                        $('#nav-calendario-grande > dl').remove();

                                                        var calendario="";
                                                        var num=1;
                                                        var dia=1;

                                                        var diasMes=requestData['diasMes'];
                                                        var diaSemana=requestData['diaSemana'];
                                                        var nuevoCss="";

                                                        if((parseInt(diaSemana)==7 && diasMes>=30 ) || (parseInt(diaSemana)==6 && diasMes==31) ){nuevoCss=$('#nav-calendario-grande').attr('css');}

                                                        $.each(requestData['eventos'], function(i,row){	                				
                                                                        //guardo sus datos en un texto	                				
                                                                        if(num==1){
                                                                                calendario+='<dl>';
                                                                        }

                                                                         for(var s=0;dia<diaSemana;s++){		                				 	
                                                                                        calendario+='<dd class="'+nuevoCss+'"></dd>';		                				
                                                                                        num=num+1;
                                                                                        dia=dia+1;
                                                                         }


                                                                         if(num <=6){	
                                                                                         if(row['existe']==1){ 			                					 	
                                                                                                 calendario=calendarioBloque(row,'normal',nuevoCss,calendario)
                                                                                         }else{
                                                                                                calendario+='<dd class="'+nuevoCss+'">';
                                                                                                               calendario+='<span>'+row['dia']+'</span>';
                                                                                                calendario+='</dd>';
                                                                                         }
                                                                         }



                                                                         if(num==7){
                                                                                         if(row['existe']==1){
                                                                                                 calendario=calendarioBloque(row,'domingo',nuevoCss,calendario)
                                                                                         }else{
                                                                                                         calendario+='<dd class="domingo '+nuevoCss+'">';                                                                                                                        
                                                                                                                        calendario+='<span>'+row['dia']+'</span>';
                                                                                                         calendario+='</dd>';
                                                                                         }
                                                                }

                                                                        if(num==7 || diasMes==num){
                                                                                calendario+='</dl>';
                                                                        num=0;
                                                                }

                                                                        num=num+1;                                                        		
                                                                        dia=dia+1;

                                                            });

                                                        $('#mesActualGrande').text(requestData['nombreMes']);

                                                        $('#nav-calendario-grande > .content-aviso-calendario-grande').after(calendario);
                                                            
                                                        $('#nav-calendario-grande > dl > dd').on({
                                                                        'click':function(){
                                                                                //clickDia($(this));//desabilitado temporalmente hasta que se cree el poput calendario grande
                                                                                //return false;//desabilitado temporalmente hasta que se cree el poput calendario grande
                                                                        }
                                                        });


                                                        //actualizamos los atributos
                                                        $('#nav-calendario-grande').attr('year',year);
                                                        $('#nav-calendario-grande').attr('month',month);
                                                        $('#nav-calendario-grande').attr('diasMes',diasMes);
                                                        $('#nav-calendario-grande').attr('diaSemana',diaSemana);

                                        },  
                                        error: function(requestData, strError, strTipoError){  
                                                alert('Error ' + strTipoError +': ' + strError);  
                                        },  
                                        complete: function(requestData, exito){

                                        }  
                                    });
		}
		
		//sabemos que esto es lo mejor
		function calendarioBloque(row,diaActual,nuevoCss,calendario){
				    var countRow=0;
				    
				    $.each(row, function(i,registro){
                                         countRow++;   
                                    });
                                    //-------------------------------------------------------------------------
                                    //codigo temporal hasta hacer el poput de calendario grande
                                    var actionEvento=$('#nav-calendario-grande').attr('actionEvento');
                                    var year=$('#nav-calendario-grande').attr('year');
                                    var mes=$('#nav-calendario-grande').attr('month');
                                    //alert(mes);
                                    var dia=row['dia'];
                                    
                                    if(parseInt(dia)<10){
                                        dia='0'+dia;
                                    }
                                    
                                    //rebajamos el mes temporalmente en (-1) para que funcione correctamente
                                    if(parseInt(mes)===1){
                                        mes=13;
                                        year=year-1;
                                    }
                                    var newMes=parseInt(mes)-1;
                                    
                                    if(newMes<10){
                                        newMes='0'+newMes;
                                    }
                                    
                                    var urlEvento=actionEvento+'/fecha/'+dia+'-'+newMes+'-'+year;
                                    //-------------------------------------------------------------------------
				    if(diaActual=='domingo'){
				    	nuevoCss=nuevoCss+" domingo";
				    }
		
					if((countRow - 2)==1){
                                                            
                                                    calendario+='<dd id="'+row[0]['id']+'" tit="'+row[0]['nombre_evento']+'" des="'+row[0]['descripcion']+'" class="'+nuevoCss+' evento">';                                                                    
                                                                    calendario+='<span>'+row['dia']+'</span>';
                                                                    calendario+='<p>';
                                                                        calendario+='<a href="'+urlEvento+'">'+row[0]['nombre_evento']+'</a>';
                                                                        calendario+='<a href="'+urlEvento+'">Ver más…</a>';
                                                                    calendario+='</p>';
                                                    calendario+='</dd>';
					}else{
					            
			                    var ids=0;
			                    var titulos="";
			                    var descripciones="";

			                    for(i=0;i<(countRow-2);i++){                                                                                                                    
			                                    if(ids==0){
			                                        ids=row[i]['id'];
			                                    }else{
			                                        ids=ids+'-'+row[i]['id'];
			                                    }

			                                    if(titulos==""){
			                                    	titulos=row[i]['nombre_evento'];
			                                    }else{
			                                        titulos=titulos+'/////'+row[i]['nombre_evento'];
			                                    }

			                                    if(descripciones==""){
			                                        descripciones=row[i]['descripcion'];
			                                    }else{
			                                        descripciones=descripciones+'/////'+row[i]['descripcion'];
			                                    }
			                    }
					           
					           calendario+='<dd id="'+ids+'" tit="'+titulos+'" des="'+descripciones+'" class="'+nuevoCss+' evento">';					           			
                                                                        calendario+='<span>'+row['dia']+'</span>';
                                                                        calendario+='<p>';
                                                                            calendario+='<a href="'+urlEvento+'">'+row[0]['nombre_evento']+'</a>';
                                                                            calendario+='<a href="'+urlEvento+'">Ver más…</a>';
                                                                        calendario+='</p>';
					           calendario+='</dd>';
		
					}
					
					return calendario;
			
		}
});

