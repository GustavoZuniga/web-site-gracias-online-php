$(document).on('ready',function() {//asi es ahora con html5 
                               
                $('#frmRegistrarUsuario').keypress(function(e){    
		    if(e == 13){ return false; } 
		});
                
                $('#txtNombres').keyup(function() {
                    $('#error-nombres').removeClass('mostrar-respuesta');
                });
                
                $('#txtApellidos').keyup(function() {
                    $('#error-apellidos').removeClass('mostrar-respuesta');
                });
                
                $('#txtUsername').keyup(function() {
                    $('#error-username').removeClass('mostrar-respuesta');
                });
                
                $('#txtPassword').keyup(function() {
                    $('#error-password').removeClass('mostrar-respuesta');
                });
                
                 $('#txtEmail').keyup(function() {
                    $('#error-email').removeClass('mostrar-respuesta');
                });
                
		$("#btnRegistroUsuario").click(function(e) {						
                    var nombres =$.trim($("#txtNombres").val());
                         if(nombres == "" || nombres =="nombres"){
                             //alert('mostrar titulo');
                         $('#error-nombres').addClass('mostrar-respuesta');
                         $("#txtNombres").focus();				     	
                         return false;
                     }
				    
                    var apellidos = $.trim($("#txtApellidos").val());				    
                        if(apellidos == "" || apellidos == "apellidos"){
                        $('#error-apellidos').addClass('mostrar-respuesta');
                        $("#txtApellidos").focus();				     	
                        return false;
                    }
                    
                    var username =$.trim($("#txtUsername").val());
                         if(username == "" || username =="username"){
                             //alert('mostrar titulo');
                         $('#error-username').addClass('mostrar-respuesta');
                         $("#txtUsername").focus();				     	
                         return false;
                     }
                     
                      var password =$.trim($("#txtPassword").val());
                         if(password == "" || password =="contraseña"){
                             //alert('mostrar titulo');
                         $('#error-password').addClass('mostrar-respuesta');
                         $("#txtPassword").focus();				     	
                         return false;
                     }
                     
                     var email =$.trim($("#txtEmail").val());
                        if(email == "" || email =='email'){	
                            $('#error-email').addClass('mostrar-respuesta');
                            $('#error-email').text('Tiene que ingresar un e-mail.');
                            $("#txtemail").focus();		     	
                            return false;
                        }
                     
                     var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if(!emailReg.test(email)){
                                $('#error-email').addClass('mostrar-respuesta');
                                $('#error-email').text('No es un e-mail correcto, vuelva a escribir.');
                                $("#txtEmail").focus();											
                                return false;
                        }
				    
				    
				    				
		});
                
});