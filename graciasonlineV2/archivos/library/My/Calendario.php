<?php

class My_Calendario  extends Zend_Controller_Plugin_Abstract
{

    function calendario($data,$Year,$Month,$diasMes){
        $newData=$this->newData($data,$Year,$Month,$diasMes);
        
        $myFunciones=new My_Funciones();
        
    	$arrayFinal=array();
    	
    	for($i=0;$i< $diasMes;$i++){
	    		$dia=(int)$i+1;
	    		if($dia<10){$dia='0'.$dia;}
	    		
	    		$fecha=$Year."-".$Month."-".$dia;
	    		
	    		$arrayPost=array_values(array_filter($newData, function($arrayValue) use($fecha) {
	    			return $arrayValue['fecha'] == $fecha;
	    		} ));
	    		
                        $countArrayPost=count($arrayPost);
                        
	    		if($countArrayPost>0){
	    			$arrayFinal[((int)$i+1)]=array_merge($arrayPost,array('dia'=>((int)$i+1),'existe'=>1));
	    			$descripcion=$arrayFinal[((int)$i+1)][0]['descripcion'];
	    			$arrayFinal[((int)$i+1)][0]['descripcion']=$myFunciones->newString(20, $descripcion,"...");
	    			$titulo=$arrayFinal[((int)$i+1)][0]['nombre_evento'];
	    			$arrayFinal[((int)$i+1)][0]['nombre_evento']=$myFunciones->newString(25, $titulo);
	    		}else{
	    			$arrayFinal[((int)$i+1)]=array_merge($arrayPost,array('dia'=>((int)$i+1),'existe'=>0));
	    		}
    	}
    	
    	return $arrayFinal;    	
    }
    
    
    private function newData($data,$Year,$Month,$diasMes){//en proceso de desarollo        
        $newData=array();
                
        for($i=1;$i<=$diasMes;$i++):            
            $dia=(int)$i;
	    if($dia<10){$dia='0'.$dia;}
            
            $fecha=$Year."-".$Month."-".$dia;
            
            foreach ($data as $key):                
                if((strtotime($key['fecha_inicio'])<= strtotime($fecha) 
                   && strtotime($key['fecha_fin']) >=  strtotime($fecha)) 
                   ):
                        $key['fecha']=$fecha;
                        $newData[]=$key;
                endif;
            endforeach;
        endfor;
        return $newData;
    }
    
    public function newDataListaDia($data,$fecha){        
        $dia=$fecha[0];
        $Month=$fecha[1];
        $Year=$fecha[2];        
        $diasMes=$this->getMonthDays($Year, $Month);
        
        $newData=$this->newData($data,$Year,$Month,$diasMes);        
        $newFecha=$Year."-".$Month."-".$dia;
        
        $arrayPost=array_values(array_filter($newData, function($arrayValue) use($newFecha) {
                return $arrayValue['fecha'] == $newFecha;
        } ));
        
        return $arrayPost;
    }
    
    function getMonthDays($Year,$Month){//numero de dias del mes    	
    	if( is_callable("cal_days_in_month"))
    	{
    		return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
    	}else{
    		//Lo hacemos a mi manera.
    		return date("d",mktime(0,0,0,$Month+1,0,$Year));
    	}
    }
    
    function diaSemana($Year,$Month){
    	return 	date("N",strtotime(trim($Year)."-".trim($Month)."-01"));//dia de la semana
    }
    
    function meses($Month){    	
    	$meses[1]='ENERO';
    	$meses[2]='FEBRERO';
    	$meses[3]='MARZO';
    	$meses[4]='ABRIL';
    	$meses[5]='MAYO';
    	$meses[6]='JUNIO';
    	$meses[7]='JULIO';
    	$meses[8]='AGOSTO';
    	$meses[9]='SEPTIEMBRE';
    	$meses[10]='OCTUBRE';
    	$meses[11]='NOVIEMBRE';
    	$meses[12]='DICIEMBRE';    	
    	return $meses[(int)$Month];
    }

}

