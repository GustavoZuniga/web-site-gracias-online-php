function subirFotos(content){
            //eliminamos los inputs files vacios ocultos
	    deleteFileVacios();
            //--------------------------------------------------------
            //limitamos la cantidad de fotos subidas a (3)
            var count=$(content+' p').length;
                            
            if(count>=3){
                $('#error-fotos').addClass('mostrar');
                $('#error-fotos').text('Solo 3 fotos como máximo.');
                return false;
            }
            //--------------------------------------------------------
            var numFile=$(content+' p').length;
            
            
            var input='<input type="file"  name="archivos[]" class="ocultar"/>';
            
            if($(content+' input[type=file]').length>0){
            	$(content+' input[type=file]:last').after(input);
            }else{            	
            	$(content+' input:last').after(input);
            }
            var countFile=$(content+' input[type=file]').length-1;
            
            $(content+' input[type=file]').eq(countFile).on({            
                        'change':function(){
                        		    //--------------------------------------------------------
                                            var nombreImagen=$.trim($(this).val());//file
                                            
                                            //filtramos la ruta larga del archivo en google crom e iternet 8
                                            var nombreImagen = nombreImagen.substr(nombreImagen.lastIndexOf("\\") + 1, nombreImagen.length);
                                            
                                            //que solo se archivos de fotos(evaluamos la extension)
                                            var extension = nombreImagen.substr(nombreImagen.lastIndexOf(".")+1, nombreImagen.length);
                                            extension=extension.toUpperCase();
                                            
                                            var arrayExtensiones =new Array('JPG','JPEG','PNG','BMP');
                                            if($.inArray(extension,arrayExtensiones)<0){
                                                $('#error-fotos').addClass('mostrar');
                                                $('#error-fotos').text('No aceptamos este archivo ('+extension+').');
                                                $(content+' input[type=file]:last').remove();
                                                return false;
                                            }
                                            //--------------------------------------------------------
                                            var string='<p>';
                                                string+='<span>'+nombreImagen+'</span>';
                                                string+='<a href="#">eliminar</a>';
                                                string+='</p>';
                                              //--------------------------------------------------------
                                                
                                                if(numFile>0){
                                                	$('#content-sf p:last').after(string);//se agrega al final del (p)
                                                }else{
                                                	$('#content-sf input:first').before(string);
                                                }
                                                
                                                //------------------------------------------------------------------
                                                var index=$('#content-sf p').length-1;//restamos 1 porque el eq() --> empieza en (0)                                                
                                                svUno(content,'#getFotos',nombreImagen,index);//si el video existe
                                                
                                                //--------------------------------------------------------
                                                //eliminamos imagen existente                                                
                                                $(content+' p').eq(index).children('a').one({//si no eres exacto con el "index" funcionara mal,como si le dieras click 2 veces seguidas	
                                                        'click':function(){
                                                        				deleteFileVacios();
                                                        				
			                                                        	var textoVideo=$(this).parent().children('span').text();			                                                        				                                                        	
			                                                        	var posicion=$(this).parent().index();			                                                        	
			                                                        	var numP=$(content+' p[id]').length;
			                                                        	
			                                                        	if(numP>0){//si existe traidos de BD			                                                        		
			                                                        		posicion=posicion-numP;
			                                                        	}
			                                                        				                                                        	
			                                        	        		$(content+' input[type=file]').eq(posicion).remove();
			                                        	        		
			                                                            $(this).parent().remove();
			                                                                                                                    
			                                                            svDos(textoVideo,'#getFotos');
			                                                            	
			                                                            return false;
                                                       }
                                                });
                                                
                             }
                });				

                $(content+' input[type=file]').eq(countFile).click();
}

function deleteFileVacios(){//limpiamos los input[type=file]-->vacios
	$(':input[type=file]','form').each(function() {										
        if(!$(this).val()){
                $(this).remove();
        }																			
	});
}

function subirVideos(content){
            //--------------------------------------------------------
            //limitamos la cantidad de videos youtube a (3)
            var count=$(content+' p').length;
                            
            if(count>=3){
                $('#error-videos').addClass('mostrar');
                $('#error-videos').text('Solo 3 videos como máximo.');
                return false;
            }
            //--------------------------------------------------------
	    var video=$.trim($('#txtVideos').val());
	
	    if(video== "" || video == "https://www.youtube.com/"){				     	
	        $('#error-videos').addClass('mostrar');
	        $('#error-videos').text('Falta colocar la url de youtube.');			     	
	        $("#txtVideos").focus();				     	
	        return false;
	    }
	    
	    var string='<p>';
	        string+='<span>'+video+'</span>';
	        string+='<a href="#">eliminar</a>';
	        string+='</p>'; 
	   
	    var numFile=$(content+' p').length;
	    
	    if(numFile>0){
	    	$(content+' p:last').after(string);//se agrega al final del (p)
	    }else{                        	
	    	$(content+' input:first').before(string);
	    }
	    
	    var index=$(content+' p').length-1;//restamos 1 porque el eq() --> empieza en (0)                        
	    svUno(content,'#getVideos',video,index);//si el video existe
	    
	    //eliminamos imagen existente
	    $(content+' p').eq(index).children('a').one({//si no eres exacto con el "index" funcionara mal,como si le dieras click 2 veces seguidas
	                    'click':function(){                                        				
	                    				var textoVideo=$(this).parent().children('span').text();                                                        
	                    				$(this).parent().remove();                                                                                                                
	                                    svDos(textoVideo,'#getVideos');                                                        
	                                    return false;
	                    }
	    });				
	
	    $('#txtVideos').attr('value','');
	    $('#txtVideos').focus();
	    
	    return false;
}

function deleteFotos(numFile){
        var evento=$('#content-sf #p-'+numFile+' a').attr('evento');

        if(evento){//si existe, es porque es un update
                var eventoAnterior=$.trim($('#getFotosEliminados').val());
                if(eventoAnterior.length==0){
                        $('#getFotosEliminados').attr('value',evento);//agrego el id del evento a eliminar
                }

                if(eventoAnterior.length>0){                                    
                        $('#getFotosEliminados').attr('value',eventoAnterior+'/////'+evento);
                }                
        }

        $('#content-sf #p-'+numFile).remove();                   
        $('#content-sf #file-'+numFile).remove();                    
}

function deleteVideos(numFile){
        var evento=$('#content-sv #p-'+numFile+' a').attr('evento');

        if(evento){//si existe, es porque es un update
                var eventoAnterior=$.trim($('#getVideosEliminados').val());
                if(eventoAnterior.length==0){
                        $('#getVideosEliminados').attr('value',evento);//agrego el id del evento a eliminar
                }

                if(eventoAnterior.length>0){                                    
                        $('#getVideosEliminados').attr('value',eventoAnterior+'/////'+evento);
                }
                
                var textoVideo=$('#content-sv #p-'+numFile+' span').text();
                svDos(textoVideo,'#getTraidos');                
        }        
        
        $('#content-sv #p-'+numFile).remove();                   
        $('#content-sv #file-'+numFile).remove();                
}

//VIDEOS

function svUno(content,getFiles,file,index){
	//alert(content+'--'+getFiles+'--'+file+'--'+index);
	
	var getgetFile=$.trim($(getFiles).val());
	
    if(parseInt(getgetFile.length)==0){//primera vez    		
    		if($(content+' #getTraidos').length>0){//si es mayor a "0" es porque existe y si existe es update    			
				var getObjTraidos=$(content+' #getTraidos').val();
        		
    	    	if(fileExiste(content,getObjTraidos,index,file)==1){
    				return false;
    			}        	   
    		}
	    	
            $(getFiles).attr('value',file);
    }
    
    if(parseInt(getgetFile.length)>0){//varias veces
	    	if($(content+' #getTraidos').length>0){//si es mayor a "0" es porque existe y si existe es update
	    		var getObjTraidos=$(content+' #getTraidos').val();
				 
		    	if(fileExiste(content,getObjTraidos,index,file)==1){
					return false;
				}
	    	}	    	
			    		
    		if(fileExiste(content,getgetFile,index,file)==1){
    			return false;
    		}
    		
            $(getFiles).attr('value',getgetFile+'/////'+file);            
    }
    
}

function fileExiste(content,getObjFiles,index,file){
	//alert(content+'--'+getObjFiles+'--'+file+'--'+index);
	var arrayFiles=getObjFiles.split('/////');	
	var existe=0;//si el video ya se agrego
	
	$(arrayFiles).each(function(i,valor) {//si ya existe el video no			
	        if(file==valor){
	        		if($(content+' input[type=file]').length>0){//si existe se trata de fotos
	        			var posicion=index;
	        			var numP=$(content+' p[id]').length;
	        			
                    	if(numP>0){//si existe traidos de BD			                                                        		
                    		posicion=index-numP;
                    	}
                    	
	        			$(content+' input[type=file]').eq(posicion).remove();//eliminamos el file que es
	        		}
	        		
	        		$(content+' > p').eq(index).remove();
	        		
	                existe=1;
	                return false;//rompemos para que no busque mas					  		
	        }
	        
	});
	
	if(existe==1){
		var tipo=$(content).attr('tipo');
		var string="";
		if(tipo=='fotos'){string='las '+tipo;}
		if(tipo=='videos'){string='los '+tipo;}
		
		$('#error-'+tipo).addClass('mostrar');
        $('#error-'+tipo).text('No debe repetirse '+string+'.');
        //svDos(video,'#getTraidos');
	}
	
	return existe;
}

function svDos(textoVideo,objFiles){//es seguro que existe el archivo y por eso quitamos(cabiar nombre a fileEliminar())
	
    var files=$.trim($(objFiles).val());								
    var arrayFiles=files.split('/////');
    
   arrayFiles = $.grep(arrayFiles, function(value) {
                    return value != textoVideo;
                  });
    
    var countFiles=arrayFiles.length;
    
    if(countFiles>0){    		
            if(countFiles==1){            		
                $(objFiles).attr('value',arrayFiles[0]);
            }else{//countFiles>1
            	$(objFiles).attr('value',arrayFiles.join('/////'));//unimos el array
            }          									
    }else{//countFiles==0
    	$(objFiles).attr('value','');
    }
}