
$(document).on('ready',function() {//asi es ahora con html5
        $('#frmFederaciones').keypress(function(e){
            if(e == 13){ return false; } 
        });
        
        //UBIGEO
        $('#departamento').on('change',function(){
                    departamento($(this),$('#provincia'),$('#distrito'));
        });

        $('#provincia').on('change',function(){
                    provincia($(this),$('#distrito'));                    
        });
        
        var nombre =$.trim($("#txtNombre").val());
        if(nombre == "" || nombre == "nombre"){				     	
             $('#error-nombre').addClass('mostrar');
             $("#txtNombre").focus();				     	
             return false;
         }
         
         var direccion = $.trim($("#textDireccion").val());				    
            if(direccion == "" || direccion == "direccion"){				     	
            $('#error-direccion').addClass('mostrar');
            $("#textDireccion").focus();				     	
            return false;
        }
        
});

