$(document).on('ready',function() {//asi es ahora con html5 
                                
        $('#frmDeportista').keypress(function(e){    
		    if(e == 13){ return false; } 
		});
                
                $('#txtNombres').keyup(function() {
                    $('#error-nombres').removeClass('mostrar-respuesta');
                });
                
                $('#txtApellidos').keyup(function() {
                    $('#error-apellidos').removeClass('mostrar-respuesta');
                });
                
                $('#txtDni').keyup(function() {
                    $('#error-dni').removeClass('mostrar-respuesta');
                });
		        
		$("#btnDeportista").click(function(e) {
                    
                        var nombres =$.trim($("#txtNombres").val());
                             if(nombres == "" || nombres == "nombres"){				     	
                             $('#error-nombres').addClass('mostrar-respuesta');
                             $("#txtNombres").focus();				     	
                             return false;
                         }
                        var apellidos = $.trim($("#txtApellidos").val());
                            if(apellidos == "" || apellidos == "apellidos" ){				     	
                            $('#error-apellidos').addClass('mostrar-respuesta');
                            $("#txtApellidos").focus();				     	
                            return false;
                        }
                        
                        var dni = $.trim($("#txtDni").val());
                            if(dni == "" || dni== "dni"){				     	
                            $('#error-dni').addClass('mostrar-respuesta');
                            $("#txtDni").focus();				     	
                            return false;
                        }
				    
                        var departamento = $.trim($("#sltDepartamento").val());
                            if(departamento == "" || departamento== "departamento"){				     	
                            $('#error-departamento').addClass('mostrar-respuesta');
                            $("#sltDepartamento").focus();				     	
                            return false;
                        }
				                
                       
	               var email= $.trim($("#txtEmail").val());
                            if(email== "" || email== "email"){				     	
	                        $('#error-email').addClass('mostrar-respuesta');
	                        $("#txtEmail").focus();				     	
	                        return false;
	                    }
                            
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if(!emailReg.test(email)){
                                $('#error-email').addClass('mostrar-respuesta');
                                $('#error-email').text('No es un e-mail correcto, vuelva a escribir.');
                                $("#txtEmail").focus();											
                                return false;
                        }
	                                    
				   				    				
		});
});

