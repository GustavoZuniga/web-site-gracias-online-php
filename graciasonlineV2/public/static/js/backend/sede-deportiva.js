$(document).on('ready',function() {//asi es ahora con html5 
                                
        $('#frmSedeDeportiva').keypress(function(e){    
            if(e == 13){ return false; } 
        });
        
        $('#txtNombre').keyup(function() {
            $('#error-nombre').removeClass('mostrar-respuesta');
        });
        
        $('#txtUbicacion').keyup(function() {
            $('#error-ubicacion').removeClass('mostrar-respuesta');
        });
        
        $('#txtEmail').keyup(function() {
            $('#error-email').removeClass('mostrar-respuesta');
        });
		        
		$("#btnRegistrarSede").click(function(e) {
                    
                            var nombre =$.trim($("#txtNombre").val());
                                 if(nombre == "" || nombre == "nombre"){				     	
                                 $('#error-nombre').addClass('mostrar-respuesta');
                                 $("#txtNombre").focus();				     	
                                 return false;
                             }
                            var ubicacion = $.trim($("#txtUbicacion").val());
                                if(ubicacion == "" || ubicacion == "ubicacion" ){				     	
                                $('#error-ubicacion').addClass('mostrar-respuesta');
                                $("#txtUbicacion").focus();				     	
                                return false;
                            }


                            var departamento = $.trim($("#sltDepartamento").val());
                                if(departamento == "" || departamento== "departamento"){				     	
                                $('#error-departamento').addClass('mostrar-respuesta');
                                $("#sltDepartamento").focus();				     	
                                return false;
                            }

                            var provincia = $.trim($("#sltProvincia").val());
                                if(provincia == "" || provincia == "provincia"){				     	
                                $('#error-provincia').addClass('mostrar-respuesta');
                                $("#sltProvincia").focus();				     	
                                return false;
                            }

                            var distrito= $.trim($("#sltDistrito").val());
                                if(distrito== "" ){				     	
                                    $('#error-distrito').addClass('mostrar-respuesta');
                                    $("#sltDistrito").focus();				     	
                                    return false;
                                }

                           var email= $.trim($("#txtEmail").val());
                            if(email== "" || email== "email"){				     	
                                $('#error-email').addClass('mostrar-respuesta');
                                $("#txtEmail").focus();				     	
                                return false;
                            }
                                
                           var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                            if(!emailReg.test(email)){						
                                    $('#error-email').text('e-mail incorrecto');
                                    $('#error-email').addClass('mostrar');
                                    $("#txtEmail").focus();											
                                    return false;
                            }               
				   				    				
		});
                
                
                //UBIGEO
                $('#sltDepartamento').on('change',function(){
                            departamento($(this),$('#sltProvincia'),$('#sltDistrito'));
                });

                $('#sltProvincia').on('change',function(){
                            provincia($(this),$('#sltDistrito'));                    
                });
                
                $('#subir-fotos').on('click',function(e){
                        subirFotos('#content-sf');
                        return false;
                });
                
              //ELIMINAMOS FOTOS
                $('#content-sf p a').on('click',function(e){                        
                        var id=$(this).parent().attr('id').split('-');                        
                        deleteFotos(id[1]);
                        return false;
                });
});

