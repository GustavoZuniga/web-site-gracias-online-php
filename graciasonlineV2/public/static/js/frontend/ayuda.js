 $(document).on('ready',function() {		
 		
 		$('#frmAyuda').keypress(function(e){    
		    if(e == 13){ return false; } 
		});
 		
		$("#btnEnviarMensajeAyuda").click(function(e) {				    
				    var email = $.trim($("#textAyudaEmail").val());
					if(email == "" || email == "e-mail"){
				     	$('#resp-ayuda').text('Falta colocar el e-mail.');
				     	$("#textAyudaEmail").focus();				     	
				     	return false;
				    }
				    
				    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					if(!emailReg.test(email)){						
						$('#resp-ayuda').text('e-mail incorrecto');
					    $("#textAyudaEmail").focus();											
						return false;
					}
				    
				    var mensaje = $.trim($("#textAyudaMensaje").val());
					if(mensaje == "" || mensaje == "mensaje"){
				     	$('#resp-ayuda').text('Falta ingresar el mensaje.');
				     	$("#textAyudaMensaje").focus();				     	
				     	return false;
				    }	
                                    
                                    var url=$('#frmAyuda').attr('url');
      
                                   $.ajax({
                                       type: 'POST',
                                       url: url,
                                       async: false,
                                       data: 'textAyudaEmail=' + email + '&textAyudaMensaje=' + mensaje,
                                       success: function(responseText) {
                                           
                                           console.log(responseText);
                                            $("#textAyudaEmail").val("");
                                             $("#textAyudaMensaje").val("");
                                             $('#resp-ayuda').show();
                                             $('#resp-ayuda').fadeOut(15000);
                                           $("#resp-ayuda").css({ padding:"10px",color: "#190707", background: "#01DF01" });
                                           $('#resp-ayuda').text('Se envio su mensaje.');
                                          // console.log(responseText);exit();
                                       }

                                   });$('#resp-ayuda').text('Su mensaje ha sido enviado.');
		});
});

