$(document).on('ready',function() {//asi es ahora con html5
			
		/*
		function videos(num){
			
			var obj='<div id="container-'+num+'"></div>';
			$('#sgag-'+num).html(obj);
			$('#container-'+num).oembed("http://www.youtube.com/watch?v=CeacjOkLjZ0",{maxHeight: 200, maxWidth:300});
			
			//$('#video-'+num).oembed("http://www.youtube.com/watch?v=CeacjOkLjZ0",{maxHeight: 200, maxWidth:300});
		}
		*/
		
		function formatearMenu(){
			$('.m-fotos').removeClass('me-activo-fotos');
			$('.m-videos').removeClass('me-activo-videos');
			$('.m-equipos').removeClass('me-activo-equipos');
			$('.m-mapa').removeClass('me-activo-mapa');
			$('.m-deportistas').removeClass('me-activo-mapa');
		}
		
		function fotos(valor,vista){
			$("#sgag-"+valor).fadeIn();
			$("#sgag-"+vista).fadeOut();
			
			$('.section-galeria').attr('vista',valor);
			
			$('#sgagm-'+vista+' div').removeClass('fondo-rojo').addClass('fondo-blanco');
			$('#sgagm-'+valor+' div').removeClass('fondo-blanco').addClass('fondo-rojo');
		}
		
		function videos(valor,vista){
			$("#sgav-"+valor).fadeIn();
			$("#sgav-"+vista).fadeOut();
			
			$('.section-galeria').attr('vista',valor);
			
			$('#sgavm-'+vista+' div').removeClass('fondo-rojo').addClass('fondo-blanco');
			$('#sgavm-'+valor+' div').removeClass('fondo-blanco').addClass('fondo-rojo');
		}
			
		$('.content-menu-galeria-fotos > article > div').on("click",function(event){				
				
				var tipo=$('.content-galeria-evento').attr('tipo');								
				var valor=$(this).parent().attr('valor');				
				var vista=$('.section-galeria').attr('vista');
				
				if(valor==vista){
					return false;
				}
				
				$('.content-galeria-fotos > article').removeClass('mostrar');
								
				fotos(valor,vista);
		});
		
		$('.content-menu-galeria-videos > article > div').on("click",function(event){				
				
				var tipo=$('.content-galeria-evento').attr('tipo');								
				var valor=$(this).parent().attr('valor');				
				var vista=$('.section-galeria').attr('vista');
				
				if(valor==vista){
					return false;
				}
				
				$('.content-galeria-videos > article').removeClass('mostrar');
				videos(valor,vista);				
		});

		
		//MENU
		//videos
		$('.m-videos').on("click",function(event){			
			$('.section-galeria > section').addClass('ocultar');
			$('.content-galeria-videos').removeClass('ocultar');			
			$('.content-galeria-videos > article').removeAttr('style');
			$('.content-galeria-videos > article').removeClass('mostrar');
			$('.content-galeria-evento').attr('tipo','videos');
			
			formatearMenu();
			
			$('.m-videos').addClass('me-activo-videos');//activo
			$('.section-galeria').addClass('sg-video');//solo para video
			$('.section-galeria').attr('vista',1);
			
			//cuadro grande
			$('.content-galeria-videos > article').removeClass('mostrar');
			$('.content-galeria-videos > #sgav-1').addClass('mostrar');
			
			//menu galeria			
			$('.content-menu-galeria > article').removeClass('mostrar');
			
			$('.content-menu-galeria > article > div').removeClass('fondo-rojo').removeClass('fondo-blanco');
			$('.content-menu-galeria > article > div').addClass('fondo-blanco');
			
			$('#sgavm-1').addClass('mostrar');
			$('#sgavm-1 > div').addClass('fondo-rojo');
			$('#sgavm-1').css('margin-top','0px');
			$('#sgavm-2').addClass('mostrar');
			$('#sgavm-3').addClass('mostrar');
			
			return false;
		});
		
		//fotos
		$('.m-fotos').on("click",function(event){					
			$('.section-galeria > section').addClass('ocultar');
			$('.content-galeria-fotos').removeClass('ocultar');

			$('.content-galeria-fotos > article').removeAttr('style');
			$('.content-galeria-fotos > article').removeClass('mostrar');
			$('.content-galeria-evento').attr('tipo','fotos');
			
			formatearMenu();
			
			$('.m-fotos').addClass('me-activo-fotos');//activo
			$('.section-galeria').removeClass('sg-video');//solo para video
			$('.section-galeria').attr('vista',1);
			
			//cuadro grande
			$('.content-galeria-fotos > article').removeClass('mostrar');
			$('.content-galeria-fotos > #sgag-1').addClass('mostrar');
			
			//menu galeria			
			$('.content-menu-galeria > article').removeClass('mostrar');
			$('.content-menu-galeria > article > div').removeClass('fondo-rojo').removeClass('fondo-blanco');
			$('.content-menu-galeria > article > div').addClass('fondo-blanco');
			
			$('#sgagm-1').addClass('mostrar');
			$('#sgagm-1 > div').addClass('fondo-rojo');
			$('#sgagm-1').css('margin-top','0px');
			$('#sgagm-2').addClass('mostrar');
			$('#sgagm-3').addClass('mostrar');
			
			return false;
		});
		
		//equipos
		$('.m-equipos').on("click",function(event){
				$('.section-galeria > section').addClass('ocultar');
				$('.content-galeria-equipos').removeClass('ocultar');
				
				formatearMenu();
				
				$('.m-equipos').addClass('me-activo-equipos');//activo								
				return false;
		});
		
		
		//mapa
		$('.m-mapa').on("click",function(event){				
				$('.section-galeria > section').addClass('ocultar');
				$('.content-galeria-mapa').removeClass('ocultar');
				formatearMenu();
				$('.m-mapa').addClass('me-activo-mapa');//activo
				return false;
		});
		
		//deportistas
		$('.m-deportistas').on("click",function(event){				
				$('.section-galeria > section').addClass('ocultar');
				$('.content-galeria-deportistas').removeClass('ocultar');
				formatearMenu();
				$('.m-deportistas').addClass('me-activo-mapa');//activo
				return false;
		});
});

