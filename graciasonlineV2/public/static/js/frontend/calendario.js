$(document).on('ready',function() {//asi es ahora con html5
		function clickDia(obj){
				
				if(!obj.attr('tit')){										
					$('.content-aviso-calendario').removeClass('mostrar');
					return false;
				}
				
				$('.content-aviso-calendario').addClass('mostrar');
								
				var left=obj.position().left-100;
				var top=obj.position().top-80;
				
				$('.content-aviso-calendario').css({'left':left+'px','top':top+'px'});
				
				var numero=$.trim(obj.text());								
				$('#num-msg-calendario').text(numero);
				
				var titulo=obj.attr('tit').split('/////');				
				var descripcion=obj.attr('des').split('/////');
				//var id=obj.attr('id').split('-');
				
				//pendiente por desarrollar (si en el mismo dia tenemos mas de un evento,colocar el titulo del evento y en el siguiente titulo (mas(5))-->el titulo con el numero de eventos que falta mostrar, para que esto te dirig a una vista en particular)
				//-----------------------------------------------
				/*
				var nombreTitulo=titulo;				
				if(id.length>1){
					
				}
				*/
				//-----------------------------------------------
				
				$('.content-aviso-calendario > .right > a').text(titulo[0]);
                                var month=$('#nav-calendario').attr('month');
                                var year=$('#nav-calendario').attr('year');
                                var dia=numero;
                                if(parseInt(numero)<=9){
                                    dia='0'+numero;
                                }
                                
				var url=$('#nav-calendario').attr('actionEvento')+'/fecha/'+dia+'-'+month+'-'+year;
				$('.content-aviso-calendario > .right > a').attr('href',url);
				$('.content-aviso-calendario > .right > p').html(descripcion[0]+'<a href="'+url+'">Ver más…</a>');
				
		}
		
		
		
		$('#nav-calendario > p >a').on("click", function(event){
				clickDia($(this));				
				return false;
		});
		
		/*
		$('#nav-calendario > p >a').on("mouseover", function(){
				if($(this).attr('tit')){
					clickDia($(this));
					return false;
				}
				
		}).mouseout(function(){				
				$('.content-aviso-calendario').css('display','none');
				return false;	
		});
		*/
		
		$('.btnCalendarioIzquierda').on('click',function(event){
			var year=$('#nav-calendario').attr('year');
			var month=$('#nav-calendario').attr('month');				
			
			month=parseInt(month)-1;
			
			if(month > 0 && month<10){
				month='0'+month;
			}
			
			if(month < 1){
				year=parseInt(year)-1;
				month='12';					
			}			
			
			getEventos(year,month);
			
			return false;
		});
		
		$('.btnCalendarioDerecha').on('click',function(event){			
				var year=$('#nav-calendario').attr('year');
				var month=$('#nav-calendario').attr('month');				
				
				month=parseInt(month)+1;
				
				if(month<10){
					month='0'+month;
				}
				
				if(month >12){
					year=parseInt(year)+1;
					month='01';					
				}			
				
				getEventos(year,month);
			return false;
		});
		
		function getEventos(year,month){//Funcion que trae eventos
				var action=$('#nav-calendario').attr('data-source');
				var dataString='year='+year+'&month='+month;
				
				$.ajax({
	                async: true,  
	                dataType: 'json',  
	                type: 'POST',
	                contentType: 'application/x-www-form-urlencoded',  
	                url: action,  
	                data: dataString,  
	                beforeSend: function(data){  
	
	                },
	                success: function(requestData){
	                		$('.content-aviso-calendario').removeClass('mostrar');                                        
	                		
	                		$('#nav-calendario > p').remove();
	                		
	                		var calendario="";
	                		var num=1;
	                		var dia=1;
	                		
	                		var diasMes=requestData['diasMes'];
	                		var diaSemana=requestData['diaSemana'];
	                		var nuevoCss="";
	                		
	                		if((parseInt(diaSemana)==7 && diasMes>=30 ) || (parseInt(diaSemana)==6 && diasMes==31) ){nuevoCss=$('#nav-calendario').attr('css');}
	                			
	                		$.each(requestData['eventos'], function(i,row){	                				
					                //guardo sus datos en un texto	                				
		                			if(num==1){
		                				calendario+='<p>';
		                			}
		                			
		                			 for(var s=0;dia<diaSemana;s++){		                				 	
		                				 	calendario+='<a  href="#" class="'+nuevoCss+'"></a>';		                				
			                				num=num+1;
			                				dia=dia+1;
			                		 }
		                					                			 
		                			 
		                			 if(num <=6){	
			                				 if(row['existe']==1){ 			                					 	
			                					 calendario=calendarioBloque(row,'normal',nuevoCss,calendario)
			                				 }else{
                                                                                calendario+='<a  href="#" class="'+nuevoCss+'">';
                                                                                               calendario+=row['dia'];
                                                                                calendario+='</a>';
			                				 }
		                			 }
		                			 
		                			 
		                			 
		                			 if(num==7){
			                				 if(row['existe']==1){
			                					 calendario=calendarioBloque(row,'domingo',nuevoCss,calendario)
			                				 }else{
				                					 calendario+='<a  href="#" class="domingo '+nuevoCss+'">';
				                					 		calendario+=row['dia'];
				                					 calendario+='</a>';
			                				 }
		                     		}
		                			 
		                			if(num==7 || diasMes==num){
		                				calendario+='</p>';
		                     			num=0;
		                     		}
		                			
		                			num=num+1;                                                        		
                                                        dia=dia+1;
	                			 
				            });
	                		
	                		$('#mesActual').text(requestData['nombreMes']);
	                		
	                		$('#nav-calendario > .content-aviso-calendario').after(calendario);
	                		
	                		$('#nav-calendario > p > a').on({
		        					'click':function(){
		        						clickDia($(this));
		        						return false;
		        					}
		        			});
	                		
	                		
	                		//actualizamos los atributos
	                		$('#nav-calendario').attr('year',year);
	                		$('#nav-calendario').attr('month',month);
	                		$('#nav-calendario').attr('diasMes',diasMes);
	                		$('#nav-calendario').attr('diaSemana',diaSemana);
	                		
	                },  
	                error: function(requestData, strError, strTipoError){  
	                        alert('Error ' + strTipoError +': ' + strError);  
	                },  
	                complete: function(requestData, exito){
	                    
	                }  
	            });
		}
		
		//sabemos que esto es lo mejor
		function calendarioBloque(row,diaActual,nuevoCss,calendario){
				    var countRow=0;
				    
				    $.each(row, function(i,registro){
                                         countRow++;   
                                    });
                                    
                                    var cssQuienCreo="";
                                    
				    if(diaActual=='domingo'){
				    	nuevoCss=nuevoCss+" domingo";
				    }
		
					if((countRow - 2)==1){
                                                    
                                                    if(row[0]['quien_creo']=='usuario'){
                                                        cssQuienCreo=' evento-usuario';
                                                    }else{
                                                        cssQuienCreo=' evento-admin';
                                                    }
                                                    
                                                    calendario+='<a  href="#" id="'+row[0]['id']+'" tit="'+row[0]['nombre_evento']+'" des="'+row[0]['descripcion']+'" class="'+nuevoCss+' '+cssQuienCreo+' evento">';
                                                                    calendario+=row['dia'];
                                                    calendario+='</a>';
					}else{
					            
			                    var ids=0;
			                    var titulos="";
			                    var descripciones="";

			                    for(i=0;i<(countRow-2);i++){
                                                            if(row[i]['quien_creo']=='usuario'){
                                                                cssQuienCreo=' evento-usuario';
                                                            }else{
                                                                cssQuienCreo=' evento-admin';
                                                            }
                                                            
			                                    if(ids==0){
			                                        ids=row[i]['id'];
			                                    }else{
			                                        ids=ids+'-'+row[i]['id'];
			                                    }

			                                    if(titulos==""){
			                                    	titulos=row[i]['nombre_evento'];
			                                    }else{
			                                        titulos=titulos+'/////'+row[i]['nombre_evento'];
			                                    }

			                                    if(descripciones==""){
			                                        descripciones=row[i]['descripcion'];
			                                    }else{
			                                        descripciones=descripciones+'/////'+row[i]['descripcion'];
			                                    }
			                    }
					           
					           calendario+='<a  href="#" id="'+ids+'" tit="'+titulos+'" des="'+descripciones+'" class="'+nuevoCss+' '+cssQuienCreo+' evento">';
					           			calendario+=row['dia'];
					           calendario+='</a>';
		
					}
					
					return calendario;
			
		}
});

