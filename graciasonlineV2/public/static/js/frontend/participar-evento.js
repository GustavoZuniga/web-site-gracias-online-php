$(document).on('ready',function() {
		
		$('#participar-evento').on("click", function(event){
				var action=$(this).attr('href');
				var idEvento=$(this).attr('evento');
				var dataString='id='+idEvento;
                                
				$.ajax({
                                    async: true,  
                                    dataType: 'json',  
                                    type: 'POST',
                                    contentType: 'application/x-www-form-urlencoded',  
                                    url: action,  
                                    data: dataString,  
                                    beforeSend: function(data){  

                                    },
                                    success: function(requestData){                                                    
                                                    if(requestData.resp==1){
                                                        $('#participar-evento').addClass('ocultar');
                                                        $('#quitar-participacion').removeClass('ocultar');
                                                    }
                                    },  
                                    error: function(requestData, strError, strTipoError){  
                                            alert('Error ' + strTipoError +': ' + strError);  
                                    },  
                                    complete: function(requestData, exito){

                                    }  
                                });
				
				return false;
		});
                
                $('#quitar-participacion').on("click", function(event){                                
				var action=$(this).attr('href');
				var idEvento=$(this).attr('evento');
				var dataString='id='+idEvento;
                                
				$.ajax({
                                    async: true,  
                                    dataType: 'json',  
                                    type: 'POST',
                                    contentType: 'application/x-www-form-urlencoded',  
                                    url: action,  
                                    data: dataString,  
                                    beforeSend: function(data){  

                                    },
                                    success: function(requestData){
                                                    if(requestData.resp==1){
                                                        $('#quitar-participacion').addClass('ocultar');
                                                        $('#participar-evento').removeClass('ocultar');
                                                    }
                                    },  
                                    error: function(requestData, strError, strTipoError){  
                                            alert('Error ' + strTipoError +': ' + strError);  
                                    },  
                                    complete: function(requestData, exito){

                                    }  
                                });
				
				return false;
		});
		
});

