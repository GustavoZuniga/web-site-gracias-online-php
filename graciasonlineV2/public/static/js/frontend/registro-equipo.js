$(document).on('ready',function() {//asi es ahora con html5 
                                
                $('#frmEquipo').keypress(function(e){    
                    if(e == 13){ return false; } 
                });
                               
                $('#btnEquipo').on("click",function(e){
                        
                        var nombre =$.trim($("#txtNombre").val());
                             if(nombre == "" || nombre == "nombre"){				     	
	                             $('#error-nombre').addClass('mostrar');
	                             $("#txtNombre").focus();				     	
                             return false;
                         }
                         
                         
                        var deporte = $.trim($("#txtDeporte").val());
                        
                        if(deporte == "" || deporte == "deporte" ){				     	
	                        $('#error-deporte').addClass('mostrar');
	                        $("#txtDeporte").focus();				     	
                            return false;
                        }
                            
                        var disciplina=$.trim($("#sltDisciplina").val());
                        
                        if(disciplina == ""){				     	
	                        $('#error-disciplina').addClass('mostrar');
	                        $("#sltDisciplina").focus();				     	
                            return false;
                        }
                        
                        var telefono = $.trim($("#txtTelefono").val());
                        
                        if(telefono == "" || deporte == "telefono" ){				     	
	                        $('#error-telefono').addClass('mostrar');
	                        $("#txtTelefono").focus();				     	
                            return false;
                        }
                        
                       
	                 
                        var email= $.trim($("#txtEmail").val());
                        if(email== "" || email== "email"){				     	
	                        $('#error-email').addClass('mostrar');
	                        $("#txtEmail").focus();				     	
	                        return false;
	                    }
                            
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if(!emailReg.test(email)){
                                $('#error-email').addClass('mostrar');
                                $('#error-email').text('E-mail incorrecto, vuelva a escribir.');
                                $("#txtEmail").focus();											
                                return false;
                        }
                   
                });
                
                //UBIGEO
                $('#departamento').on('change',function(){
                            departamento($(this),$('#provincia'),$('#distrito'));
                });

                $('#provincia').on('change',function(){
                            provincia($(this),$('#distrito'));                    
                });
                
                //SUBIR FOTOS
                $('#subir-fotos').on('click',function(e){                            
	                    subirFotos('#content-sf');
	                    return false;
	            });
	            
	          //ELIMINAMOS FOTOS
	            $('#content-sf p a').on('click',function(e){                        
	                    var id=$(this).parent().attr('id').split('-');                        
	                    deleteFotos(id[1]);
	                    return false;
	            });
                    
                    //ELIMINAMOS VIDEOS
                    $('#content-sv p a').on('click',function(e){
                            var id=$(this).parent().attr('id').split('-');                        
                            deleteVideos(id[1]);
                            return false;
                    });
                    
                    //SUBIR VIDEOS
                    $('#subir-videos').on('click',function(e){
                            subirVideos('#content-sv');
                        return false;
                    });
});

