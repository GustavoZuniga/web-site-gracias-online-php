//$(document).ready(function(){
$(document).on('ready',function() {//asi es ahora con html5    
		//validacion de crear cuenta		
 		
                $('#frmCrearCuentaUsuarios').keypress(function(e){    
		    if(e == 13){ return false; } 
		});
                
                $('#txtNombreUsuarioRu').keypress(function(e){    
		    $('#error-usuario').removeClass('mostrar');
		});
                
                $('#txtPasswordRu').keypress(function(e){    
		    $('#error-password').removeClass('mostrar');
		});
                $('#txtNombresRu').keypress(function(e){    
		    $('#error-nombres').removeClass('mostrar');
		});
                $('#txtApellidosRu').keypress(function(e){    
		    $('#error-apellidos').removeClass('mostrar');
		});
                $('#txtEmailRu').keypress(function(e){    
		    $('#error-email').removeClass('mostrar');
		});
                $('#txtCiudadRu').keypress(function(e){    
		   $('#error-ciudad').removeClass('mostrar');
		});

		$("#btnCrearCuentaUsuario").on("click", function(e){
                                var usuario =$.trim($("#txtNombreUsuarioRu").val());
					if(usuario == "" || usuario == "usuario"){				     	
				     	$('#error-usuario').addClass('mostrar');
				     	$("#txtNombreUsuario").focus();				     	
				     	return false;
				    }
				    
				    var password = $.trim($("#txtPasswordRu").val());
                                    
					if(password == "" || password == "password"){				     	
				     	$('#error-password').addClass('mostrar');
				     	$("#txtPassword").focus();	
                                        			     	
				     	return false;
				    }
				    
				    var nombres = $.trim($("#txtNombresRu").val());
					if(nombres == "" || nombres == "nombres" ){				     	
				     	$('#error-nombres').addClass('mostrar');
				     	$("#txtNombres").focus();				     	
				     	return false;
				    }
				   
				    var apellidos = $.trim($("#txtApellidosRu").val());
					if(apellidos == "" || apellidos== "apellidos"){				     	
				     	$('#error-apellidos').addClass('mostrar');
				     	$("#txtApellidos").focus();				     	
				     	return false;
				    }
				    
				    var email = $.trim($("#txtEmailRu").val());
					if(email == "" || email == "e-mail"){				     	
				     	$('#error-email').addClass('mostrar');
				     	$("#txtEmail").focus();				     	
				     	return false;
				    }
				    
				    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					if(!emailReg.test(email)){
						$('#error-email').addClass('mostrar');
						$('#error-email').text('e-mail incorrecto');
					    $("#txtemail").focus();											
						return false;
					}

				    
				    var ciudad = $.trim($("#txtCiudadRu").val());
					if(ciudad == "" || ciudad == "ciudad"){				     	
				     	$('#error-ciudad').addClass('mostrar');
				     	$("#txtCiudad").focus();				     	
				     	return false;
				    }
                                        
					var pais= $.trim($("#pais").val());
					if(pais== ""){				     	
				     	$('#error-pais').addClass('mostrar');
				     	$("#pais").focus();				     	
				     	return false;
				    }
		});
				
});

