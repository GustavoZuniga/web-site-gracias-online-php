
                
        function departamento(departamento,provincia,distrito){
                    var action=departamento.attr("data-source");
                    var dataString='departamento='+ $.trim(departamento.val());

                    $.ajax({
                            async: true,  
                            dataType: 'json',  
                            type: 'POST',
                            contentType: 'application/x-www-form-urlencoded',  
                            url: action,  
                            data: dataString,  
                            beforeSend: function(data){  

                            },
                            success: function(requestData){

                                    if(parseInt(requestData.resp)>0){                                                
                                        provincia.find("option").remove();

                                        $(requestData.provincias).each(function(i,valor) {
                                             provincia.append('<option value="'+valor['provincia']+'">'+valor['provincia']+'</option>');
                                        });
                                        
                                        provincia.children('option:first').attr('selected','selected');
                                        
                                        distrito.find("option").remove();

                                        $(requestData.distritos).each(function(i,valor) {
                                             distrito.append('<option value="'+valor['distrito']+'">'+valor['distrito']+'</option>');
                                        });
                                        
                                        distrito.children('option:first').attr('selected','selected');                                        
                                    }

                            },  
                            error: function(requestData, strError, strTipoError){  
                                    //alert('Error ' + strTipoError +': ' + strError);  
                            },  
                            complete: function(requestData, exito){

                            }  
                        });

                        return false;
        }

        function provincia(provincia,distrito){
                    var action=provincia.attr("data-source");
                    var dataString='provincia='+ $.trim(provincia.val());

                    $.ajax({
                            async: true,  
                            dataType: 'json',  
                            type: 'POST',
                            contentType: 'application/x-www-form-urlencoded',  
                            url: action,  
                            data: dataString,  
                            beforeSend: function(data){  

                            },
                            success: function(requestData){

                                    if(parseInt(requestData.resp)>0){
                                        distrito.find("option").remove();

                                        $(requestData.distritos).each(function(i,valor) { 
                                             distrito.append('<option value="'+valor['distrito']+'">'+valor['distrito']+'</option>');
                                        });
                                    }

                            },  
                            error: function(requestData, strError, strTipoError){  
                                    //alert('Error ' + strTipoError +': ' + strError);  
                            },  
                            complete: function(requestData, exito){

                            }  
                        });

                        return false;
        }
		


