function updateBackground() {
	  screenWidth = $(window).width();
	  screenHeight = $(window).height();
	  //----------------------------------------
	  $('body').append('<img src="/ipdportal/public/static/images/fondo.jpg" alt="Fondo" id="bg" />');
	  //----------------------------------------
	  var bg = $("#bg");
	  
	  // Proporcion horizontal/vertical
	  ratio = 1;
	  
	  if (screenWidth/screenHeight > ratio) {
	    $(bg).height("auto");
	    $(bg).width("100%");
	  } else {
	    $(bg).width("auto");
	    $(bg).height("100%");
	  }
	
	  if ($(bg).width() > 0) {
	    $(bg).css('left', (screenWidth - $(bg).width()) / 2);
	  }  
}

$(document).on('ready',function() {//asi es ahora con html5	
	//solo funciona para internet explorer versiones <=8
	  if (jQuery.browser.msie && jQuery.browser.version <= 8) { 
			updateBackground();
			$(window).bind("resize", function() {
				updateBackground();
			});
	    }	  
});
			